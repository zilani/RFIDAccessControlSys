#include <SoftwareSerial.h>
#include "rdm6300.h"
#include "Arduino.h"


// Initialise an UART Instance of the Library
RDM6300::RDM6300(int rxPin, int txPin) {
    _rfidIO = new SoftwareSerial(rxPin, txPin);
    _rfidIO->begin(9600);

    // Reset the message and other values
    _tag.mfr = 0;
    _tag.id = 0;
    _tag.chk = 0;
    _tag.valid = false;

    _idAvailable = false;
    _bytesRead = 0;
}


// Returns the ID as a struct and sets the _idAvailable to "false"
RFIDTag RDM6300::readId() {
	_idAvailable = false;
#ifdef DEBUG
	Serial.println("readId()");
	Serial.println(_tag.raw);
	Serial.print("  MFR:\t");
	Serial.println(_tag.mfr,HEX);
	Serial.print("  ID:\t");
	Serial.println(_tag.id,HEX);
	Serial.print("  CHK:\t");
	Serial.println(_tag.chk,HEX);
#endif
	return _tag;
}

void RDM6300::restart()
{
    //_rfidIO->end();
    delay(2);
    _rfidIO->begin(9600);
    _rfidIO->flush();

    // Reset the message and other values
    _tag.mfr = 0;
    _tag.id = 0;
    _tag.chk = 0;
    _tag.valid = false;
    _idAvailable = false;
    _bytesRead = 0;
    for (size_t i = 0; i < sizeof(_tag.raw); ++i)  _tag.raw[i] = 0;

}


// Read data, check whether a complete ID has been 
// read and return true if the ID can be read out
boolean RDM6300::isIdAvailable() { 
	/**
	 * Method outline
	 *  1) read until a header byte is reached
	 *  2) whenever the method is called, put any 
	 *     new characters into the temporary store
	 *  3) whenever the final byte is reached, finalize 
	 *     the ID and return true, otherwise, return false
	 */
	char val;
	
if(_rfidIO->available() > 0) {
     if((val = _rfidIO->read()) == 0x02) { 
	    // start reading input
	   _bytesRead = 0; 
	
		_tag.mfr = 0;
		_tag.id  = 0;
		_tag.chk = 0;
		_tag.valid = false;
		_idAvailable = false;
#ifdef DEBUG
		Serial.println("**********************START**********************");
#endif		
	  } 
	  
	  else if(val == 0x03 && _bytesRead == 12) {
		// ID completely read
		byte checksum = 0;
		byte value = 0;
		String id = _tag.raw;
		
		_tag.mfr = hex2dec(id.substring(0,4));
		_tag.id  = hex2dec(id.substring(4,10));
		_tag.chk = hex2dec(id.substring(10,12));

		// Do checksum calculation
		int i2;		
		for(int i = 0; i < 5; i++) {
			i2 = 2*i;
			checksum ^= hex2dec(id.substring(i2,i2+2));
		}
#ifdef DEBUG
		Serial.println("VERIFICATION");
		Serial.print("  ID:\t");
		Serial.println(_tag.raw);
		Serial.print("  CHK:\t");
		Serial.println(checksum, HEX);
#endif		

		if (checksum == _tag.chk) {
			_tag.valid = _idAvailable = true;
#ifdef DEBUG
		Serial.println("VALID tag");
#endif		
		}
#ifdef DEBUG
		Serial.println("**********************END**********************");
#endif		
	  } else {
		_tag.raw[_bytesRead++] = val;	
	  }
	}
	return _idAvailable;
    } 
	
	

// Convert a HEX String to a decimal value (up to 8 bytes (16 hex characters))
long RDM6300::hex2dec(String hexCode) {
  char buf[19] = "";
  hexCode = "0x" + hexCode;
  hexCode.toCharArray(buf, 18);
#ifdef DEBUG
  Serial.print("Decoding ");
  Serial.print(hexCode);
  Serial.print(": ");
  Serial.println(strtol(buf, NULL, 0));
#endif
  return strtol(buf, NULL, 0);
}

