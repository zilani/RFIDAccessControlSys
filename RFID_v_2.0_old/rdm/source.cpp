#include "Arduino.h"
#include "source.h"

Ticker espTime;
Ticker serverTime;


Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);

RDM6300 RDMreader(RFID_RX_PIN, RFID_TX_PIN);
RFIDTag tag;

WiFiManager wifiManager;


bool minuteFlag=0;
bool hourFlag=0;
bool dayFlag=0;


struct dateTime timeStamp;
struct Command  command;

static unsigned int logNumber=0;                    // number of user log recorded in RAM;
static unsigned int recSize=0;                      // Number of registered UID  
static unsigned int currentLogAddress;              // initialization for EEPROM address.Some first bytes are resereved for registry. 100 byte empty gap between registry and log
static unsigned int unsentLogStartAddress;          // Address of the start of the unsent log
static unsigned int lastAddressOfLog=0;

static unsigned int registryLastAddress = EEPROM_REGISTRY_START_ADDRESS;
static byte registry[MAX_REGISTRY][RFID_BYTE_SIZE];              //LOAD registry from EEPROM to RAM


void sourceSetup(){

   timeStamp.Year=16;
   timeStamp.Month=11;
   timeStamp.Day=5;
   timeStamp.Hour=12;
   timeStamp.Minute=1;
   timeStamp.Second=1;
   command.deviceRegistered = true; // When rebooted its true.only server can make it false. if it is false esp is rebooted.

#ifdef ENABLE_LCD
   tft.initR(INITR_BLACKTAB);
   tft.setRotation(3);
   welcome_page();
#endif

   Wire.begin(1000000);
   //delay(1000);

#ifdef RESET_EEPROM  
   resetAddressPointers(RESET_FULL_EEPROM);
#endif
   
   loadAddressPointers();
   loadRegistry(); 

#ifdef ENABLE_LCD  
   rams_page();
   delay(1500);
#endif
   
   //comment out the line below to reset WiFi settings
   // wifiManager.resetSettings(); 

#ifdef ENABLE_LCD
   wifiSetupPage();
#endif
   
   wifiManager.setTimeout(WIFI_TIMEOUT);
   wifiManager.autoConnect(AP_SSID);

#ifdef ENABLE_LCD   
   tft.fillScreen(ST7735_BLACK);
#endif
   
   if(WiFi.status()==WL_CONNECTED)getCommands();                  // updates time, checks if the device is registered


#ifdef ENABLE_LCD
   wifiSection();
   lcdClockSection();
   lcdCardSection(REST,"");       ////// Show "SWIPE CARD" on LCD
#endif  

   pinMode(BUZZER_PIN,OUTPUT);
   espTime.attach(1,setTime);


#ifdef ENABLE_SERVER_COMMUNICATION
   serverTime.attach(UPDATE_INTERVAL_SECOND,commandFlag);    //updateInterval is the time interval to send log 
#endif
   
  }



void loadAddressPointers(){
  //Serial.println(F("Loaded address pointers"));

/*  
 *   READ REGISRTY END LOCATION
 */
  byte high_byte  =  readAddress(REGISTRY_HIGH_BYTE_ADDRESS);
  byte low_byte   =  readAddress(REGISTRY_LOW_BYTE_ADDRESS);
  registryLastAddress = high_byte * 256 + low_byte;

#ifdef ENABLE_DEBUG  
  Serial.print(F("registryLastAddress: "));
  Serial.println(registryLastAddress);
#endif

/*  
 *   READ UNSENT LOG ADDRESSES
 */

  high_byte  =  readAddress(UNSENT_LOG_STARTING_ADDRESS_H);
  low_byte   =  readAddress(UNSENT_LOG_STARTING_ADDRESS_L);
  
  unsentLogStartAddress = high_byte * 256 + low_byte;

#ifdef ENABLE_DEBUG    
  Serial.print(F("unsentLogStartAddress: "));
  Serial.println(unsentLogStartAddress);

  Serial.println("Searching EEPROM for End Log Address");
#endif

  byte tmp;
  for(int i=EEPROM_LOG_START_ADDRESS; i<=EEPROM_LOG_END_ADDRESS;i++){       
       if(readAddress(i)=='E')
         if(readAddress(i+1)=='N')
           if(readAddress(i+2)=='D'){
              currentLogAddress=i;  //// START contains 5 char
              break;
               }
    yield(); 
  }


  Serial.print(F("unsentLogStartAddress: "));
  Serial.println(unsentLogStartAddress);

  Serial.print(F("currentLogAddress: "));
  Serial.println(currentLogAddress);

  }


void saveAddressPointers(int choice){

   Serial.println("ACCESSING TO EEPROM 10 BYTE... SAVING");
  //Serial.println(F("Saved address pointers"));

  if(choice==REGISTRY_ADDR){
  byte low= registryLastAddress& 0xFF;
  byte high=registryLastAddress >> 8;
  
  writeAddress(REGISTRY_HIGH_BYTE_ADDRESS,high);                               
  writeAddress(REGISTRY_LOW_BYTE_ADDRESS,low);  

  Serial.print(F("registry last address: "));
  Serial.println(registryLastAddress);
  }


  else if(choice==ULOG_START){

  byte low=unsentLogStartAddress & 0xFF;
  byte high=unsentLogStartAddress >> 8;
  
  writeAddress(UNSENT_LOG_STARTING_ADDRESS_H,high);
  writeAddress(UNSENT_LOG_STARTING_ADDRESS_L,low);
  
  Serial.print(F("unsentLogStartAddress: "));
  Serial.println(unsentLogStartAddress);
  
  high  =  readAddress(UNSENT_LOG_STARTING_ADDRESS_H);
  low   =  readAddress(UNSENT_LOG_STARTING_ADDRESS_L);
  
  unsentLogStartAddress = high * 256 + low;
  
  }
  
  }



void resetAddressPointers(int choice){

  if(choice==REGISTRY_ADDR){
  writeAddress(REGISTRY_HIGH_BYTE_ADDRESS,0);                               
  writeAddress(REGISTRY_LOW_BYTE_ADDRESS,EEPROM_REGISTRY_START_ADDRESS); 

  }

  else if(choice==ULOG_START){
  byte low=EEPROM_LOG_START_ADDRESS & 0xFF;
  byte high=EEPROM_LOG_START_ADDRESS >> 8;
  
  writeAddress(UNSENT_LOG_STARTING_ADDRESS_H,high);
  writeAddress(UNSENT_LOG_STARTING_ADDRESS_L,low);

  }

  else if(choice== RESET_FULL_EEPROM){
     
      writeAddress(REGISTRY_HIGH_BYTE_ADDRESS,0);                               
      writeAddress(REGISTRY_LOW_BYTE_ADDRESS,EEPROM_REGISTRY_START_ADDRESS); 
      byte low=EEPROM_LOG_START_ADDRESS & 0xFF;
      byte high=EEPROM_LOG_START_ADDRESS >> 8;
  
      writeAddress(UNSENT_LOG_STARTING_ADDRESS_H,high);
      writeAddress(UNSENT_LOG_STARTING_ADDRESS_L,low);  
      writeAddress(EEPROM_LOG_START_ADDRESS,  'E');
      writeAddress(EEPROM_LOG_START_ADDRESS+1,'N');
      writeAddress(EEPROM_LOG_START_ADDRESS+2,'D');
  }
  
//  low=EEPROM_ROLL_START_ADDRESS & 0xFF;
//  high=EEPROM_ROLL_START_ADDRESS >> 8;
  
//  writeAddress(ROLL_HIGH_BYTE_ADDRESS,high);                               
//  writeAddress(ROLL_LOW_BYTE_ADDRESS,low);    

  //Serial.println(F("after the address pointers are reset and loaded: " ));
  //Serial.println("");
  //Serial.println("");
  }


void loadRegistry(){    
  
  recSize = 0;

#ifdef ENABLE_DEBUG
  Serial.println(EEPROM_REGISTRY_START_ADDRESS);
  Serial.print("to");
  Serial.println(registryLastAddress);
#endif
  
  for(int i=EEPROM_REGISTRY_START_ADDRESS;i<registryLastAddress;){
      for(int j=0;j<RFID_BYTE_SIZE;j++){
          registry[recSize][j]= readAddress(i);
          //Serial.println(registry[recSize][j]);
          i++;
      }    
      recSize++;
      yield();
      }
  //if(recSize > 0)
  //recSize--;                            //record size is one more than actual registered uids
}



/*
 * ADDS UID AFTER THE LAST REGISTRY IN EEPROM
 */

void addToRegistry(byte uid[], byte roll[]){

#ifdef ENABLE_DEBUG
    Serial.println("From add to reg:");

   for(int i=0;i<RFID_BYTE_SIZE;i++){
    Serial.print(uid[i]);
   }

    Serial.print("   ");

   for(int i=0;i<RFID_BYTE_SIZE;i++){
    Serial.print(roll[i]);
   }

#endif
    
    for(int i=0;i<RFID_BYTE_SIZE;i++){
      writeAddress(registryLastAddress,uid[i]);                                 ////////adding uid
      writeAddress((EEPROM_ROLL_START_ADDRESS+registryLastAddress-EEPROM_REGISTRY_START_ADDRESS),roll[i]);      /////// adding roll
      registryLastAddress++;
    }

}





//////////////////////////    BUZZER    ////////////////////////////////////////////////

void BuzzRFIDdetected(){
          digitalWrite(BUZZER_PIN, HIGH);
          delay(50);
          digitalWrite(BUZZER_PIN, LOW);
          }


          
void BuzzRFIDisRegistered(){
          digitalWrite(BUZZER_PIN, HIGH);
          delay(80);
          digitalWrite(BUZZER_PIN, LOW);
          delay(50);
          digitalWrite(BUZZER_PIN, HIGH);
          delay(50);
          digitalWrite(BUZZER_PIN, LOW);
          }

void BuzzRFIDisNotRegistered(){
          digitalWrite(BUZZER_PIN, HIGH);
          delay(50);
          digitalWrite(BUZZER_PIN, LOW);
          }

void BuzzlogIsSent(){
          digitalWrite(BUZZER_PIN, HIGH);
          delay(30);
          digitalWrite(BUZZER_PIN, LOW);
          delay(50);
          digitalWrite(BUZZER_PIN, HIGH);
          delay(30);
          digitalWrite(BUZZER_PIN, LOW);

          }


bool isRegistered(){

#ifdef ENABLE_DEBUG
  Serial.println("Checking for a match in registry");
#endif

  String class_roll = "";
  
  byte a=tag.id>>16 & 0xFF;
  byte b=tag.id>>8 & 0xFF;
  byte c=tag.id  & 0xFF;

  int roll_pointer;

  //Serial.print("recsize = ");
  //Serial.println(recSize);
  
  for(int i=0;i<recSize;i++){
   
     if(registry[i][0]==a && registry[i][1]==b && registry[i][2]==c){
              roll_pointer = EEPROM_ROLL_START_ADDRESS + i*RFID_BYTE_SIZE;      //// Roll size is taken as same as rfid uid size
              for(int j=roll_pointer;j<(roll_pointer+RFID_BYTE_SIZE);j++){
                  class_roll += String(readAddress(j)); 
                    
               }
               
               #ifdef ENABLE_LCD
                  lcdCardSection(CARD_MATCHED,class_roll);
               #endif
                  
                  serverTime.detach();
                  serverTime.attach(UPDATE_INTERVAL_SECOND,commandFlag);    //updateInterval is the time interval to send log  

               #ifdef ENABLE_DEBUG
                  Serial.println("MATCH FOUND");
               #endif
                     
                  return true;
     }
    yield();  
    }  

  String temp= String(a,HEX)+String(b,HEX)+String(c,HEX);

#ifdef ENABLE_LCD
  lcdCardSection(CARD_NOT_MATCHED,temp); 
#endif


#ifdef ENABLE_DEBUG
    Serial.println("NO MATCHING UID FOUND");
#endif
               
  return false;
  }

  
void saveLogToEEPROM(){

  
  if((EEPROM_LOG_END_ADDRESS-currentLogAddress)<=10){ 
    lastAddressOfLog=currentLogAddress;
    currentLogAddress=EEPROM_LOG_START_ADDRESS;        //////// go to the top of log section and replace data 

#ifdef ENABLE_DEBUG    
    Serial.println(F("restoring pointer...LAST ADDRESS OF log"));
    Serial.println(lastAddressOfLog);
#endif
    
    command.logOverflow=true;
  }
      writeAddress(currentLogAddress, (tag.id>>16 & 0xFF));
      currentLogAddress++;
      writeAddress(currentLogAddress, (tag.id>>8 & 0xFF));
      currentLogAddress++;
      writeAddress(currentLogAddress, tag.id  & 0xFF);
      currentLogAddress++;
      writeAddress(currentLogAddress, timeStamp.Hour);
      currentLogAddress++;
      writeAddress(currentLogAddress, timeStamp.Minute);
      currentLogAddress++;
      writeAddress(currentLogAddress, timeStamp.Second);
      currentLogAddress++;
      writeAddress(currentLogAddress, timeStamp.Year);
      currentLogAddress++;
      writeAddress(currentLogAddress, timeStamp.Month);
      currentLogAddress++;
      writeAddress(currentLogAddress, timeStamp.Day);
      currentLogAddress++;

      writeAddress(currentLogAddress,   'E');
      writeAddress(currentLogAddress+1, 'N');
      writeAddress(currentLogAddress+2, 'D');
      
   logNumber++;
}


void setTime(){
/*  timeStamp.Second++;
  timeStamp.Minute++;
  timeStamp.Hour++;
  minuteFlag=true;
  hourFlag=true;

  if(timeStamp.Second>=60){
    timeStamp.Second=0;
    }
    
  if(timeStamp.Minute==60){
    timeStamp.Minute=0;
    }
    
  if(timeStamp.Hour==24){
    timeStamp.Hour=0;
    dayFlag=true;
    }
*/

  timeStamp.Second++;
  if(timeStamp.Second>=60){
    timeStamp.Minute++;
    timeStamp.Second=0;
    minuteFlag=true;
    lcdClockSection();
    wifiSection();                    // Show green or red boundary in LCD to show wifi status    
    }
    
  if(timeStamp.Minute==60){
    timeStamp.Hour++;
    timeStamp.Minute=0;
    hourFlag=true;
    }
    
  if(timeStamp.Hour==24){
    timeStamp.Hour=0;
    dayFlag=true;
    }

    
  }



void commandFlag(){
   command.checkForCommand=true;
  }



void getCommands(){

  HTTPClient http;
  http.begin(command_server);
  int httpCode=http.GET();

#ifdef ENABLE_DEBUG
  Serial.print(F("Command Server :"));
  Serial.println(httpCode);
#endif
  
  String json=http.getString();

  http.end();
  if(httpCode > 0) {
   if(json == "{}\n"){
      command.deviceRegistered = false;
      command.update_registry = false;
      command.sendlog = false;
      return;
    }
    
   else command.deviceRegistered = true;
   
  StaticJsonBuffer<1000> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(json);
  
  if(!root.success()){
 //   Serial.println(F("data parsing failed"));
      return;
    } 

   String date;
   String time;
     
   command.update_registry = (bool)root["change_available"][0];
   
   date = root["date"][0].asString();

   time = root["time"][0].asString();
   command.sendlog = root["uinfo_flag"][0];
   
   timeStamp.Hour   = time.substring(0,2).toInt();
   timeStamp.Minute = time.substring(3,5).toInt();
   timeStamp.Second = time.substring(6,8).toInt();
   
   timeStamp.Year   = date.substring(2,4).toInt();
   timeStamp.Month  = date.substring(5,7).toInt();
   timeStamp.Day    = date.substring(8,10).toInt();

#ifdef ENABLE_DEBUG
   Serial.print(F("TIME :"));
   Serial.print(timeStamp.Hour);
   Serial.print(F("-"));
   Serial.print(timeStamp.Minute);
   Serial.print(F("-"));
   Serial.println(timeStamp.Second);

   Serial.print(F("DATE :"));
   Serial.print(timeStamp.Year);
   Serial.print(F("-"));
   Serial.print(timeStamp.Month);
   Serial.print(F("-"));
   Serial.println(timeStamp.Day);


   Serial.print(F("COMMANDS :"));
   Serial.print(F("update_registry :"));
   Serial.println(command.update_registry);
   
   Serial.print(F("deviceRegistered :"));
   Serial.println(command.deviceRegistered);

   Serial.print(F("Free Heap:"));
   Serial.println(ESP.getFreeHeap());

   Serial.print(F("sendlog :"));
   Serial.println(command.sendlog);
   Serial.print(F("update registry :"));
   Serial.println(command.update_registry);
#endif
  }
}



void send_data(){      //// converts the user log to json format and returns the json string
   
    int failedSendAttepmt=0;
    int recurse;   
    int tempStartAddress;
    int tempEndAddress;
  //  Serial.print("UnsentlogStart:");
   // Serial.println(unsentLogStartAddress);

    
    tempStartAddress=unsentLogStartAddress;
    
    if(command.logOverflow){
      tempEndAddress=lastAddressOfLog;
      command.logOverflow=false;
      }
      
    else{
      tempEndAddress=currentLogAddress;
    }

#ifdef ENABLE_DEBUG
    Serial.print("LOG SENDING FROM ADDRESS  ");
    Serial.print(tempStartAddress);
    Serial.print(" TO  ");
    Serial.println(tempEndAddress);
#endif

    if(tempStartAddress==tempEndAddress)return;

    lcdCardSection(BUSY,"");        // LCD prints that device is busy sending log to server
    recurse = (tempEndAddress-tempStartAddress)/(LOG_BYTE_SIZE*BATCH_SIZE);

#ifdef ENABLE_DEBUG
    Serial.print("TOTAL PACKAGES TO BE SENT :");
    Serial.println(recurse+1);
#endif

    HTTPClient http;
    http.begin(log_server);
    http.addHeader("content-type","text/json");
    
    for(int i=0;i<recurse;i++){
    String logData=convertLogToJson(tempStartAddress,tempStartAddress+BATCH_SIZE*LOG_BYTE_SIZE);
    int code = http.POST(logData);

#ifdef ENABLE_DEBUG
    Serial.println(code);
#endif
    
    if(code==HTTP_CODE_OK){
      tempStartAddress=tempStartAddress+BATCH_SIZE*LOG_BYTE_SIZE;
      unsentLogStartAddress=tempStartAddress;
      failedSendAttepmt=0;
      logNumber-=BATCH_SIZE;    /// Unsent log is reduced
    }
    else {
      
#ifdef ENABLE_DEBUG
      Serial.println(http.getString());
#endif

      i--;
      failedSendAttepmt++;
    }
    if(failedSendAttepmt== LOG_SEND_ATTEMPT)break;

    }
    
    if(failedSendAttepmt==0 && tempStartAddress!=tempEndAddress){

#ifdef ENABLE_DEBUG
     Serial.println();
     Serial.println("SENDING THE REST.....");
     Serial.println();
#endif
    
    String logData=convertLogToJson(tempStartAddress,tempEndAddress);
    
    int code = http.POST(logData);

#ifdef ENABLE_DEBUG
    Serial.println(code);
#endif
    
    if(code==HTTP_CODE_OK){
      tempStartAddress=tempEndAddress;
      unsentLogStartAddress=tempStartAddress;
      logNumber=0;
      BuzzlogIsSent();
    }
    http.end();
    }

      if(unsentLogStartAddress==lastAddressOfLog)unsentLogStartAddress=EEPROM_LOG_START_ADDRESS;
      saveAddressPointers(ULOG_START);  
  }


String convertLogToJson(int from, int to){
    StaticJsonBuffer<2900> jsonBuffer;
  
    JsonObject& root = jsonBuffer.createObject();
    JsonObject& nestedObject1 = root.createNestedObject("Row");
    
     int logs=0;
     String uid;
     String time;
     String date;
     String dateTime;

    for(int i=from;i<to;i++){
     
        yield();
        JsonObject& nestedObject2 = nestedObject1.createNestedObject(String(logs));
    
        uid= String(readAddress(i),HEX) + String(readAddress(++i),HEX) + String(readAddress(++i),HEX) + String(readAddress(++i),HEX);
        time= String(readAddress(++i)) + ":" + String(readAddress(++i)) + ":" + String(readAddress(++i));
        date=  "20" + String(readAddress(++i)) + "-" + String(readAddress(++i)) + "-" + String(readAddress(++i));
        dateTime=(date + " " + time);
    
        nestedObject2["PIN"] = uid;
        nestedObject2["DateTime"] = dateTime;
        nestedObject2["Verified"] = "1";
        nestedObject2["Status"] = "1";
        nestedObject2["WorkCode"] = "0";
   
        logs++;
    }

    nestedObject1["unit_id"] = UNIT_ID;

    String data;
    root.printTo(data);

#ifdef ENABLE_DEBUG    
    Serial.println(data);
#endif
    
    return data;
  }


void writeAddress(int address, byte val)
{
  Wire.beginTransmission(EEPROM_I2C_ADDRESS);
  Wire.write((int)(address >> 8));   // MSB
  Wire.write((int)(address & 0xFF)); // LSB
  Wire.write(val);
  Wire.endTransmission();
  delay(5);
}

byte readAddress(int address)
{
  byte rData = 0x00;
  
  Wire.beginTransmission(EEPROM_I2C_ADDRESS);
  Wire.write((int)(address >> 8));   // MSB
  Wire.write((int)(address & 0xFF)); // LSB
  Wire.endTransmission();  

  Wire.requestFrom(EEPROM_I2C_ADDRESS, 1);  

  rData =  Wire.read();

  return rData;
}




void updateRegistry(){
  lcdCardSection(BUSY,"");        // LCD prints that device is busy sending log to server
  Serial.println("Updating Registry");
  HTTPClient http;
  http.begin(acknowledge_server);
  http.GET();
  http.end();
  http.begin(registry_server);

  int httpCode=http.GET();
  
  if(httpCode<0){
    command.update_registry = true;
    return;
  }
  String json=http.getString();
  http.end();
  
  if(httpCode > 0) {
    command.update_registry = false;
      if(json == "{}"){
          return;
    }
  else {
      StaticJsonBuffer<2000> jsonBuffer;
      JsonObject& root = jsonBuffer.parseObject(json);
   
         if(!root.success()){
#ifdef ENABLE_DEBUG
    Serial.println(F("data parsing failed"));
#endif
         return;
    } 

   JsonArray& nestedArray = root["card_no"];
   JsonArray& nestedArray1 = root["classroll"];

   int i=0;
   String card;
   String rollS;

   resetAddressPointers(REGISTRY_ADDR);          //////// last Registry pointer goes to registry start address 

   while(true){  
          
          card=root["card_no"][i].asString();
          rollS=root["classroll"][i].asString();

#ifdef ENABLE_DEBUG
          Serial.print(card);
          Serial.print("  ");
          Serial.println(rollS);
#endif

          if(card==NULL)break;                                //// if card value is null, end is reached      
          if(card.length()!=RFID_BYTE_SIZE*2){
            i++;
            continue;   //// if card size is not equal to rfid size then skip to next iteration
          }
          char *tmp = (char *) card.c_str();
          for(int j=0; tmp[j]!='\0'; j++){                    
            if(!isxdigit(tmp[j])){
              i++;
              continue;                    //// if card is not valid hex, skip
            }
           }


          tmp = (char *) rollS.c_str();
          for(int j=0; tmp[j]!='\0'; j++){
            if(!isxdigit(tmp[j])){
              i++;
              continue;                    //// if roll is not valid hex, skip
            }
           }
          
          
          byte uid[RFID_BYTE_SIZE];
          byte roll[RFID_BYTE_SIZE];

         int tp=0;
         for(int j=0;j<RFID_BYTE_SIZE*2;j+=2){
            uid[tp]= strtol(card.substring(j,j+2).c_str(),NULL,16);
            tp++;
            yield();
      }
         
        String temp;
        for(int j=0;j<(RFID_BYTE_SIZE*2-rollS.length());j++){
          temp+="0";
          yield();
        }
          temp+=rollS;
          rollS=temp;


        tp=RFID_BYTE_SIZE-1;
        
        for(int j=RFID_BYTE_SIZE*2;j>0;j-=2){
          roll[tp]= strtol(rollS.substring(j-2,j).c_str(),NULL,10);
          yield();
          tp--;
        }

          addToRegistry(uid,roll);
          i++;
          }

      }
  }
  
  saveAddressPointers(REGISTRY_ADDR);
  loadRegistry();
  printRegistry();
 }







































 ///////////////////////////// Display /////////////////////////////

void welcome_page(){
  
  tft.fillScreen(ST7735_RED);
  tft.setFont(&FreeSansBold12pt7b);
  tft.setCursor(15,70);
  tft.print(F("WELCOME"));
  }

void wifiSetupPage(){

  if(WiFi.status()==WL_CONNECTED)return;
  tft.fillScreen(ST7735_BLACK);
  tft.setFont();
  tft.setTextSize(2);
  tft.setTextWrap(true);
  tft.setTextColor(ST7735_RED);
  tft.setCursor(0,20);
  tft.println(F("  WIFI NOT"));
  tft.println(F("  CONNECTED"));
  tft.setTextSize(0);
  tft.setCursor(5,55);
  tft.setTextColor(ST7735_MAGENTA);
  tft.print(F("WAIT SOME TIME OR"));
  tft.print(F(" CONNECT TO THIS DEVICE AND"));
  tft.setCursor(20,73);
  tft.setTextColor(ST7735_GREEN);
  tft.setTextSize(1);
  tft.println(F("GO TO : 192.168.4.1"));
  tft.setTextColor(ST7735_MAGENTA);
  tft.setCursor(10,83);
  tft.setTextSize(0);
  tft.println(F("   TO CONFIGURE WIFI."));
  tft.setCursor(5,93);
  tft.println(F("  THE DEVICE WILL START"));
  tft.print(F(" AUTOMATICALLY AFTER "));
  tft.println(WIFI_TIMEOUT);
  tft.println(F(" SECONDS IF NOTHING IS "));
  tft.println(F("        DONE."));
 
  }

void device_not_registered_page(){
  
  espTime.detach();///////////// because of this time and wifi connection is updated on lcd in some interval
  tft.fillScreen(ST7735_RED);
  tft.setFont(&FreeSansBold9pt7b);
  tft.setCursor(0,20);
  tft.print(F("Please register"));
  tft.setCursor(0,40);
  tft.print(F("your device . . ."));
  tft.setCursor(0,60);
  tft.print(F("Device ID :"));
  tft.setCursor(0,80);
  tft.println(UNIT_ID);
  tft.setFont();
  tft.setTextSize(0);
  tft.setCursor(5,100);
  tft.println("For registration go to");
  tft.println(" rumytecnologies.com/rams");
  
  }

void rams_page(){
    tft.fillScreen(ST7735_BLACK);
  tft.setTextColor(ST7735_CYAN);
  tft.setFont(&FreeSansBold24pt7b);
  tft.setCursor(7,60);
  tft.println(F("RAMS"));

  tft.setFont(&TomThumb);
  tft.setTextSize(0);
  tft.setCursor(15,70);
  tft.print(F("RUMY ATTENDANCE MANAGEMENT SYSTEM"));

  tft.setTextColor(0xfeaa);
  tft.setCursor(10,90);
  tft.setFont(&FreeSansOblique9pt7b);
  tft.print(F("RUMY"));
  tft.setCursor(25,110);
  tft.print(F("TECHNOLOGY"));

  for(int i=0;i<160;i++){
    tft.drawPixel(i+10,92,0x3ffe);
    tft.drawPixel(i+10,93,0x3ffe);
    tft.drawPixel(i+10,94,0x3ffe);
    
    tft.drawPixel(i+28,112,0x3ffe);
    tft.drawPixel(i+28,113,0x3ffe);
    tft.drawPixel(i+28,114,0x3ffe);
    delay(5);
    }
  
  }



void lcdCardSection(int matched, String roll){
  
   
    tft.fillRect(15,50,135,60,ST7735_BLACK);
    tft.setFont(&FreeSerifBold9pt7b);
    tft.setTextSize(0);
    
    
  if(matched==CARD_MATCHED){
    tft.setTextColor(0x07ec);
    lcdRect(0x07ec);
    tft.setCursor(30,85);
   // tft.println(F("THANKS"));
    //tft.println(F("       GRANTED"));
    tft.print(F("ID:"));
    tft.print(roll);
    }
    
  else if(matched==CARD_NOT_MATCHED){
    tft.setTextColor(0xf80a);
    lcdRect(0xf80a);
    tft.setCursor(15,85);
    tft.println(F("  TRY AGAIN"));
   // tft.println(F("         DENIED"));
    }
    
  else if(matched==REST){
    tft.setTextColor(ST7735_DARKGREEN);
    lcdRect(ST7735_DARKGREEN);
    tft.setCursor(15,75);
    tft.println(F("  SWIPE CARD"));
//    tft.println(F("      TO ACCESS"));
    }
  else if(matched==BUSY){
    tft.setTextColor(ST7735_DARKGREEN);
    lcdRect(ST7735_DARKGREEN);
    tft.setCursor(15,65);
    tft.println(F("  DEVICE BUSY"));
    tft.println(F("     PLEASE WAIT"));
    }
}


void wifiSection(){

  int COLOR;
  String WifiStatus;
  if(WiFi.status()!=WL_CONNECTED){
    COLOR =ST7735_RED;
 //   WifiStatus="NOT CONNECTED";
  }
  else {
    COLOR =ST7735_DARKGREEN;
  //  WifiStatus="  CONNECTED";
  }
  
  tft.setFont(&TomThumb);
  tft.setTextSize(0);
//  tft.setCursor(110,15);
//  tft.setTextColor(ST7735_BLUE);
//  tft.print(F("WIFI STATUS"));
  tft.setCursor(130,12);
  tft.setTextColor(COLOR);
  tft.print("WIFI");
  
  
  }


void lcdRect(uint16_t COLOR){
//  tft.fillRect(0,0,3,128,COLOR);
//  tft.fillRect(0,0,160,3,COLOR);
//  tft.fillRect(0,125,160,3,COLOR);
//  tft.fillRect(157,0,3,128,COLOR);

  tft.fillRect(15,47,135,1,COLOR);
  tft.fillRect(15,47,1,66,COLOR);
  tft.fillRect(15,113,135,1,COLOR);
  tft.fillRect(150,47,1,66,COLOR);
  
  }


void lcdClockSection(){
   tft.setFont(&TomThumb);
/*    tft.setTextColor(ST7735_GREEN);
    tft.setCursor(10,20); 
    tft.setTextSize(2); 
    tft.print("TIME:");

    tft.setCursor(10,35); 
    tft.print("DATE:");
*/
    tft.setFont();
    tft.setTextSize(1); 
    int mm=timeStamp.Minute;
    int hh=timeStamp.Hour;
    

    
    String hr;
    String min;
    String ampm;

    bool nightF;
    if(hh<12){            /// 0-11
      ampm= "AM";
      nightF=false;
    }

    else if(hh==12){        //////// 12 is 12PM.... 12-12 becomes 0 PM.... so seperately written
      ampm= "PM";
      nightF=true;
    }
    else {                //13-23 becomes 1-11
      ampm= "PM";
      hh=hh-12;
      nightF=true;
    }

 ///////////////////////CLEAR AND PRINT HOUR ///////////////////////////////////


 ////////////FIRST CLEAR HOUR //////////////////////////////////////////      
   
   if(hourFlag){      ///////////hour is changed
    tft.setTextColor(ST7735_BLACK);   
    tft.setCursor(18,10); 
    if(hh==0) tft.print("11");              //////// after 11 comes 0
    else if(hh==1 && nightF==false) tft.print("00");              
    else if(hh==1 && nightF==true) tft.print("12");                          
    
    else{
      hh-=1;                               /// Else print previous number in Black
      if(hh<10)hr= "0"+String(hh);
      else hr=String(hh);
      tft.print(hr);
      hh++;
    }
      
  }


///////////////NOW PRINT HOUR///////////////////////

    tft.setCursor(18,10);
    tft.setTextColor(ST7735_CYAN);   
    if(hh<10)hr= "0"+String(hh) + ":";
    else hr=String(hh)+":";

//    Serial.print(hr);

    tft.print(hr);

/////////////////////////CLEAR AND PRINT MINUTE /////////////////////////
    
    if(minuteFlag){ 
      tft.setTextColor(ST7735_BLACK);    
      tft.setCursor(38,10);  
      if(mm==0)tft.print("59");

      else{
      mm--;
      if(mm<10)min= "0"+String(mm);
      else min=String(mm);
      tft.print(min);
      mm++; 
      }  
      minuteFlag=false; 
    }
  
    
    tft.setCursor(38,10);
    tft.setTextColor(ST7735_CYAN);
    if(mm<10)min= "0"+String(mm);
    else min=String(mm);
  
 //   Serial.print(min);
    tft.print(min);



/////////////////////////CLEAR AND PRINT AM/PM/////////////////////////////////////
    
    if(hourFlag){
      tft.setCursor(54,10);
      tft.setTextColor(ST7735_BLACK);
      tft.print("AM");
      tft.setCursor(54,20); 
      tft.print("PM");
      hourFlag=false; 
    }

    tft.setTextColor(ST7735_CYAN);
    tft.setCursor(54,10);
    tft.print(ampm);
    //Serial.println(ampm);


/////////////////////////PRINT DATE ////////////////////////////////////////

    tft.fillRect(18,24,70,13,ST7735_BLACK);
    tft.setCursor(18,25); 
    tft.print(timeStamp.Day);
    tft.print(F("."));
    tft.print(timeStamp.Month);
    tft.print(F(".20"));
    tft.print(timeStamp.Year);
    
}


























 void getUnsentLog(){

    int totalLogs=0;
    for(int i=EEPROM_LOG_START_ADDRESS;i<currentLogAddress;){
       totalLogs++;
       delay(1);
        Serial.print(F("UID: "));
      for(int uid=0;uid<RFID_BYTE_SIZE;uid++){
         Serial.print(readAddress(i),HEX);
          i++;
         }

        Serial.print(F("  TIME: "));

      for(int tym=0;tym<3;tym++){
        Serial.print(readAddress(i));
          i++;
        }
        Serial.print(F("  DATE: "));
 
     for(int date=0;date<3;date++){
        Serial.print(readAddress(i));
         i++;
        }
        Serial.println();
      }

      Serial.println(F("TOTAL LOGS:"));  
      Serial.println(totalLogs);   
/*
 * 
 * UNSENT LOGS
 */

    Serial.println(F("Unsent LOG:"));    
    Serial.println(logNumber);
    for(int i=unsentLogStartAddress;i<currentLogAddress;){
      delay(1);
      Serial.print(F("UID: "));
      for(int uid=0;uid<RFID_BYTE_SIZE;uid++){
          Serial.print(readAddress(i),HEX);
          i++;
        }

         Serial.print(F("  TIME: "));

    for(int tym=0;tym<3;tym++){
         Serial.print(readAddress(i));
         i++;
        }
         Serial.print(F("  DATE: "));
 
    for(int date=0;date<3;date++){
         //Serial.print(readAddress(i));
         i++;
        }
         Serial.println();
      }
  }




void printRegistry(){

  Serial.print(F("Number of registered user:"));
  Serial.println(recSize);
 //////////// display registered files ///////////////////  

  int reg=EEPROM_REGISTRY_START_ADDRESS;
  int rol=EEPROM_ROLL_START_ADDRESS;
  
  for(int i=EEPROM_REGISTRY_START_ADDRESS;i<registryLastAddress;){
      for(int j=0;j<RFID_BYTE_SIZE;j++){
          Serial.print(readAddress(i+j),HEX);
      }

      Serial.print("    ");
      for(int j=0;j<RFID_BYTE_SIZE;j++){
          Serial.print(readAddress(EEPROM_ROLL_START_ADDRESS+i-EEPROM_REGISTRY_START_ADDRESS+j));
      }

      i+=RFID_BYTE_SIZE;
      Serial.println();
      yield();
    }
  }

