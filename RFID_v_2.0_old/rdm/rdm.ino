#include "source.h"    
#include "Arduino.h"
#include "rdm6300.h"


extern RFIDTag tag;
extern RDM6300 RDMreader;
extern Command command;

void setup() {

  delay(100);  

#ifdef ENABLE_DEBUG
  Serial.begin(115200);
  Serial.println();
  Serial.println();
#endif

  sourceSetup();
  
}



void loop() {


 if(RDMreader.isIdAvailable()){    // if RFID is found , get the UID , search for a match in stored registry
      //// Need T0 Halt
      tag = RDMreader.readId();      
      BuzzRFIDdetected();   // buzzer signals that a card is detected  
      if(isRegistered()){  
            saveLogToEEPROM();
            BuzzRFIDisRegistered();   // buzzer signals that the detected rfid is registered
            delay(1000);

            #ifdef ENABLE_LCD
            lcdCardSection(REST,"");     // LCD goes back to normal "SWIPE CARD" position
            #endif

         }
      else{

            BuzzRFIDisNotRegistered();   // buzzer signals that rfid is not registered
            delay(1000);
            #ifdef ENABLE_LCD
            lcdCardSection(REST,"");        // LCD goes back to normal "SWIPE CARD" position
            #endif
      }
             
 }



#ifdef ENABLE_SERVER_COMMUNICATION

  else if(command.checkForCommand){       // Time to get commands from server
        command.checkForCommand=false;

        if(WiFi.status()==WL_CONNECTED){
        getCommands();                    // Get commands from server and update necessary flags

        if(command.deviceRegistered==false){
           #ifdef ENABLE_LCD
            device_not_registered_page();
            #endif
            
            delay(60000);
           // Serial.println(F("Device is not Registered, Restarting..."));
            ESP.restart();
        }

        
       if(command.sendlog){
            send_data();
        }

        if(command.update_registry)updateRegistry();
        #ifdef ENABLE_LCD
        lcdCardSection(REST,"");        // LCD goes back to normal "SWIPE CARD" position
        #endif
        }
       
    }
    
#endif

}


