#include "source.h"
#include "Arduino.h"

extern MFRC522 rfid;
extern Command command;

void setup() {
  delay(100);

#ifdef DEBUG

  Serial.begin(115200);
  delay(2000);
  Serial.println("############################################################################");
  Serial.print(F("DEVICE JUST STARTED. DEVICE ID:"));
  Serial.println(UNIT_ID);

  Serial.println(registry_server);
  Serial.println(acknowledge_server);
  Serial.println(command_server);
  Serial.println(log_server);
  Serial.println(report_server);
  Serial.println(confirm_update);
  Serial.println(CodeVersion);


#endif


  sourceSetup();
  SPI.begin();
  rfid.PCD_Init();
  command.restarted = true;


#ifdef  DEBUG
  Serial.println(F("SETUP COMPLETED"));
  Serial.println(F("ENTERING TO LOOP"));
  Serial.println();
  Serial.println();
  Serial.println(F("############################################################################"));
  printEEPROM();
  printRegistry();
  delay(2000);
#endif

}



void loop() {

  if (rfid.PICC_IsNewCardPresent() && rfid.PICC_ReadCardSerial()) {
    
    rfid.PICC_HaltA();
    rfid.PCD_StopCrypto1();

    //BuzzRFIDdetected();   // buzzer signals that a card is detected
    if (isRegistered()) {

#ifdef DEBUG
      Serial.println(F("Registered ID found"));
#endif

      saveLogToEEPROM();
      BuzzRFIDisRegistered();   // buzzer signals that the detected rfid is registered
      //delay(1000);
      lcdCardSection(REST, "");    // LCD goes back to normal "SWIPE CARD" position
    }
    else {

#ifdef DEBUG
      Serial.println(F("ID Not registered"));
#endif

      BuzzRFIDisNotRegistered();   // buzzer signals that rfid is not registered
      //delay(1000);
      lcdCardSection(REST, "");       // LCD goes back to normal "SWIPE CARD" position
    }

  }

  else if (command.checkForCommand) {     // Time to get commands from server
    command.checkForCommand = false;

    if (WiFi.status() == WL_CONNECTED) {
      getCommands();                    // Get commands from server and update necessary flags

      if (command.deviceRegistered == false) {
        device_not_registered_page();

#ifdef DEBUG
        Serial.println(F("Device is not Registered, Restarting..."));
#endif
      }


      if (command.sendlog) {
        send_data();
      }

      if (command.update_registry)
      {
        modify_serverTime(20);
        updateRegistry();
      }
      else {
        modify_serverTime(60);
      }

      if (command.deviceRegistered == true)
      {
        wifiSection();
        lcdClockSection();
        lcdCardSection(REST, "");       // LCD goes back to normal "SWIPE CARD" position
      }
    }
  }

  else if (command.restarted) {
    report_restart();
    command.restarted = false;
  }
  
  else if (command.sendRecSize == true) {
    sendRecsize();
  }

  else if (command.updateFirmware == true && WiFi.status() == WL_CONNECTED) {
    confirmUpdate();
#ifdef DEBUG
    Serial.println(F("Updating firmware...."));
#endif
    delay(1500);
    update_page();
    executeUpdate();
  }
}

/****/
//END



