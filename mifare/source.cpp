#include "Arduino.h"
#include "source.h"


MFRC522 rfid(RFID_SS_PIN, RFID_RST_PIN);
Ticker espTime;
Ticker serverTime;
Ticker dispUnitIDTime;

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);


WiFiManager wifiManager;


struct dateTime timeStamp;
struct Command  command;
struct flags  Flag;

static unsigned int logNumber = 0;                  // number of user log recorded in RAM;
static unsigned int recSize = 0;                    // Number of registered UID
static  int currentLogAddress;              // initialization for EEPROM address.Some first bytes are resereved for registry. 100 byte empty gap between registry and log
static  int unsentLogStartAddress;          // Address of the start of the unsent log
//static  int lastAddressOfLog = 0;

static int registryLastAddress = EEPROM_REGISTRY_START_ADDRESS;
static byte registry[MAX_REGISTRY][RFID_BYTE_SIZE];              //LOAD registry from EEPROM to RAM
static byte uidToBeDeleted[MAX_SERVER_DATA][RFID_BYTE_SIZE];


static int roll_pointer;

void sourceSetup() {

  ////Default time setup////////////////
  timeStamp.Year = 16;
  timeStamp.Month = 11;
  timeStamp.Day = 5;
  timeStamp.Hour = 12;
  timeStamp.Minute = 1;
  timeStamp.Second = 1;


  // When rebooted its true.only server can make it false. if it is false esp is rebooted.
  command.deviceRegistered = true;
  command.updateFirmware = false;



  // TFT INIYIALIZATION //////////////////////
  tft.initR(INITR_BLACKTAB);
  tft.setRotation(3);
  welcome_page();



  Wire.begin(1000000);
  delay(1000);

  stellar_logo();
  delay(2000);

  wifiSetupPage();
  wifiManager.setTimeout(WIFI_TIMEOUT);
  wifiManager.autoConnect(AP_SSID, AP_PASSWORD);

  if (WiFi.status() == WL_CONNECTED) {
#ifdef DEBUG
    Serial.println("WIFI CONNECTED");
#endif

    getCommands();                  // updates time, checks if the device is registered
  }

  if (command.deviceRegistered == false) {
    device_not_registered_page();
    delay(60000);

#ifdef DEBUG
    Serial.println(F("Device is not Registered, Restarting..."));
#endif
    // ESP.restart();
  }

  else {
    /*********************************************************************************
      TRY TO GET UPDATE AT SETUP IF NECESSARY
    *********************************************************************************/
    if (command.updateFirmware == true && WiFi.status() == WL_CONNECTED) {
      confirmUpdate();
#ifdef DEBUG
      Serial.println(F("Updating firmware at setup....."));
#endif
      delay(1500);
      update_page();
      executeUpdate();
    }
    /*********************************************************************************/
    wifiSection();
    lcdClockSection();
    lcdCardSection(REST, "");                                 // Show "SWIPE CARD" on LCD
  }

#ifdef RESET_DEVICE
  resetAddressPointers(RESET_FULL_EEPROM);
#endif
  loadAddressPointers();
  loadRegistry();
  printRegistry();

  pinMode(BUZZER_PIN, OUTPUT);
  espTime.attach(1, setTime);
  serverTime.attach(UPDATE_INTERVAL_SECOND, commandFlag);   //updateInterval is the time interval to send log
  dispUnitIDTime.attach(30, displayUnitID);
}



void loadAddressPointers() {


#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : loadAddressPointers()"));
  Serial.println();
  Serial.print(F("registry start Address: "));
  Serial.println(EEPROM_REGISTRY_START_ADDRESS);
#endif

  /*
      READ REGISRTY END LOCATION
  */
  byte high_byte  =  readAddress(REGISTRY_HIGH_BYTE_ADDRESS);
  byte mid_byte   =  readAddress(REGISTRY_MID_BYTE_ADDRESS);
  byte low_byte   =  readAddress(REGISTRY_LOW_BYTE_ADDRESS);

  registryLastAddress = high_byte * 65536 + mid_byte * 256 + low_byte;

#ifdef DEBUG
  Serial.print(F("registryLastAddress: "));
  Serial.println(registryLastAddress);
#endif
  /*
       READ UNSENT LOG ADDRESSES
  */

  high_byte  =  readAddress(UNSENT_LOG_STARTING_ADDRESS_H);
  mid_byte   =  readAddress(UNSENT_LOG_STARTING_ADDRESS_M);
  low_byte   =  readAddress(UNSENT_LOG_STARTING_ADDRESS_L);

  unsentLogStartAddress = high_byte * 65536 + mid_byte * 256 + low_byte;

#ifdef DEBUG
  Serial.print(F("unsentLogStartAddress: "));
  Serial.println(unsentLogStartAddress);
#endif

  byte tmp;
  bool notfound = false;
  for (int i = unsentLogStartAddress; i < EEPROM_LOG_END_ADDRESS; i++) {// a change to find current log address faster
#ifdef DEBUG
    Serial.print((char)tmp);
#endif

    if (readAddress(i) == 'E') {
      if (readAddress(i + 1) == 'N') {
        if (readAddress(i + 2) == 'D') {
          currentLogAddress = i; //// START contains 5 char
          notfound = false;
          break;
        }
        else {
          notfound = true;
        }
      }
      else {
        notfound = true;
      }
    }
    else {
      notfound = true;
    }
    yield();
  }

  if (notfound == true) {
    for (int i = EEPROM_LOG_START_ADDRESS; i < unsentLogStartAddress; i++) {// a change to find current log address faster
#ifdef DEBUG
      Serial.print((char)tmp);
#endif

      if (readAddress(i) == 'E') {
        if (readAddress(i + 1) == 'N') {
          if (readAddress(i + 2) == 'D') {
            currentLogAddress = i; //// START contains 5 char
            break;
          }
        }
      }
      yield();
    }
  }
#ifdef DEBUG
  Serial.print(F("unsentLogStartAddress: "));
  Serial.println(unsentLogStartAddress);

  Serial.print(F("currentLogAddress: "));
  Serial.println(currentLogAddress);

#endif
}


void saveAddressPointers(int choice) {


#ifdef DEBUG
  Serial.println("ACCESSING TO EEPROM 10 BYTE... SAVING");
  Serial.println(F("Saved address pointers"));
#endif


  if (choice == REGISTRY_ADDR) {
    byte low  = registryLastAddress & 0xFF;
    byte mid  = registryLastAddress >> 8;
    byte high = registryLastAddress >> 16;

    writeAddress(REGISTRY_HIGH_BYTE_ADDRESS, high);
    writeAddress(REGISTRY_MID_BYTE_ADDRESS, mid);
    writeAddress(REGISTRY_LOW_BYTE_ADDRESS, low);
#ifdef DEBUG
    Serial.print(F("registry last address: "));
    Serial.println(registryLastAddress);
#endif
  }


  else if (choice == ULOG_START) {

    byte low  = unsentLogStartAddress & 0xFF;
    byte mid  = unsentLogStartAddress >> 8;
    byte high = unsentLogStartAddress >> 16;

    writeAddress(UNSENT_LOG_STARTING_ADDRESS_H, high);
    writeAddress(UNSENT_LOG_STARTING_ADDRESS_M, mid);
    writeAddress(UNSENT_LOG_STARTING_ADDRESS_L, low);
#ifdef DEBUG
    Serial.print(F("unsentLogStartAddress: "));
    Serial.println(unsentLogStartAddress);
#endif
    high  =  readAddress(UNSENT_LOG_STARTING_ADDRESS_H);
    mid   =  readAddress(UNSENT_LOG_STARTING_ADDRESS_M);
    low   =  readAddress(UNSENT_LOG_STARTING_ADDRESS_L);

    unsentLogStartAddress = high * 65536 + mid * 256 + low;
  }
}



void resetAddressPointers(int choice) {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : resetAddressPointers()"));
  Serial.println();
#endif


  if (choice == REGISTRY_ADDR) {
    writeAddress(REGISTRY_HIGH_BYTE_ADDRESS, 0);
    writeAddress(REGISTRY_MID_BYTE_ADDRESS, 0);
    writeAddress(REGISTRY_LOW_BYTE_ADDRESS, EEPROM_REGISTRY_START_ADDRESS);

#ifdef  DEBUG
    Serial.println(F("SAVED REGISTRY ADDRESSES IN EEPROM ARE RESET"));
#endif
  }

  else if (choice == ULOG_START) {
    byte low  = EEPROM_LOG_START_ADDRESS & 0xFF;
    byte mid  = EEPROM_LOG_START_ADDRESS >> 8;
    byte high = EEPROM_LOG_START_ADDRESS >> 16;

    writeAddress(UNSENT_LOG_STARTING_ADDRESS_H, high);
    writeAddress(UNSENT_LOG_STARTING_ADDRESS_M, mid);
    writeAddress(UNSENT_LOG_STARTING_ADDRESS_L, low);
#ifdef  DEBUG
    Serial.println(F("SAVED UNSENT LOG STARTING ADDRESSES IN EEPROM ARE RESET"));
#endif
  }

  else if (choice == RESET_FULL_EEPROM) {

    writeAddress(REGISTRY_HIGH_BYTE_ADDRESS, 0);
    writeAddress(REGISTRY_MID_BYTE_ADDRESS, 0);
    writeAddress(REGISTRY_LOW_BYTE_ADDRESS, EEPROM_REGISTRY_START_ADDRESS);

    byte low  = EEPROM_LOG_START_ADDRESS & 0xFF;
    byte mid  = EEPROM_LOG_START_ADDRESS >> 8;
    byte high = EEPROM_LOG_START_ADDRESS >> 16;

    writeAddress(UNSENT_LOG_STARTING_ADDRESS_H, high);
    writeAddress(UNSENT_LOG_STARTING_ADDRESS_M, mid);
    writeAddress(UNSENT_LOG_STARTING_ADDRESS_L, low);
    writeAddress(EEPROM_LOG_START_ADDRESS,  'E');
    writeAddress(EEPROM_LOG_START_ADDRESS + 1, 'N');
    writeAddress(EEPROM_LOG_START_ADDRESS + 2, 'D');

#ifdef  DEBUG
    Serial.println(F("ALL ADDRESSES IN EEPROM ARE RESET. (REGISTRY LOCAION ADDRESS, UNSENT LOG START ADDRESS and E,N,D written at the start of log)"));
#endif

  }
}

/*
   ADDS UID AFTER THE LAST REGISTRY IN EEPROM
*/

void loadRegistry() {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : loadRegistry()"));
  Serial.println();
#endif


  recSize = 0;

#ifdef DEBUG
  Serial.println(F("REGISTRY LOADING TO RAM"));
  Serial.print(F("REGISTRY STARTS FROM EEPROM ADDRESS:"));
  Serial.print(EEPROM_REGISTRY_START_ADDRESS);
  Serial.print("  TO:");
  Serial.println(registryLastAddress);
#endif


  for (int i = EEPROM_REGISTRY_START_ADDRESS; i < registryLastAddress;) {
    for (int j = 0; j < RFID_BYTE_SIZE; j++) {
      registry[recSize][j] = readAddress(i);
      i++;
    }
    recSize++;
    yield();
  }

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println();
#endif

}

/*********************************************************************************
  The addToRegistry() function adds new Registry; i.e. User Card ID and Roll to
  the EEPROM.
*********************************************************************************/

void addToRegistry(byte uid[], char roll[]) {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : addToRegistry()"));
  Serial.println();
#endif


#ifdef DEBUG

  Serial.print(F("ADDING UID:"));
  for (int i = 0; i < RFID_BYTE_SIZE; i++) {
    Serial.print(uid[i], HEX);
  }

  Serial.print(" AND ROLL:");
  for (int i = 0; i < ROLL_BYTE_SIZE; i++) {
    Serial.print(roll[i]);
  }

  Serial.println("TO REGISTRY (IN EEPROM)");
  Serial.print("PREVIOUS REGISTRY LAST ADDRESS");
  Serial.println(registryLastAddress);

#endif

  bool addCard = false;
  bool matched = false;

  if (recSize == 0) {
    addCard = true;
  }

  for (int i = 0; i < recSize; i++) {

    for (int j = (ROLL_BYTE_SIZE - 1); j >= 0; j--) {
      if ((char)readAddress(EEPROM_ROLL_START_ADDRESS + j + i * ROLL_BYTE_SIZE) == roll[j]) {
        matched = true;
      }
      else {
        matched = false;
        break;
      }
      yield();
    }

    Serial.print("Matched : ");
    Serial.println(matched);


    if (matched == true) {
      Serial.println("A match is found in the EEPROM");
      Serial.println("Replacing the following UID");
      for (int z = 0; z < RFID_BYTE_SIZE; z++) {
        Serial.print(readAddress((EEPROM_REGISTRY_START_ADDRESS + i * RFID_BYTE_SIZE + z)));
        Serial.print("---->");
        writeAddress((EEPROM_REGISTRY_START_ADDRESS + i * RFID_BYTE_SIZE + z), uid[z]);
        Serial.println(readAddress((EEPROM_REGISTRY_START_ADDRESS + i * RFID_BYTE_SIZE + z)));
        yield();
      }
      addCard = false;
      break;
    }
    else addCard = true;
  }
  ////////////////////////////////////////////////////////////////////////////////////
  if (addCard == true) {
    addCard = false;
    int address ;

    for (int i = 0; i < ROLL_BYTE_SIZE; i++) {
      //address = EEPROM_ROLL_START_ADDRESS + ((registryLastAddress - EEPROM_REGISTRY_START_ADDRESS) / RFID_BYTE_SIZE) * ROLL_BYTE_SIZE + i;
      address = EEPROM_ROLL_START_ADDRESS + ((registryLastAddress - EEPROM_REGISTRY_START_ADDRESS) / RFID_BYTE_SIZE) * ROLL_BYTE_SIZE;
#ifdef DEBUG
      Serial.print("Roll Address : ");
      Serial.print(address);
      Serial.print ("  ");
      Serial.println(roll[i]);
#endif
      writeAddress((address + i), roll[i]);
    }

    int currentAddress = registryLastAddress;

    for (int i = 0; i < RFID_BYTE_SIZE; i++) {
#ifdef DEBUG
      Serial.print("Card Address : ");
      Serial.println(registryLastAddress);
#endif
      writeAddress(registryLastAddress, uid[i]);
      registryLastAddress++;
    }

    for (int i = 0; i < RFID_BYTE_SIZE; i++) {
#ifdef DEBUG
      Serial.print("Matching UID");
#endif
      if (readAddress(currentAddress + i) != uid[i]) {
        Flag.registryAddSuccess = false;
        break;
      }
      else {
        Flag.registryAddSuccess = true;
      }
    }
  }
#ifdef DEBUG
  Serial.print("UPDATED REGISTRY LAST ADDRESS");
  Serial.println(registryLastAddress);
#endif
  if ( Flag.registryAddSuccess = true) {
    loadRegistry();
  }
}


//////////////////////////    BUZZER    ////////////////////////////////////////////////

void BuzzRFIDdetected() {
  digitalWrite(BUZZER_PIN, HIGH);
  delay(50);
  digitalWrite(BUZZER_PIN, LOW);
}



void BuzzRFIDisRegistered() {
  digitalWrite(BUZZER_PIN, HIGH);
  delay(80);
  digitalWrite(BUZZER_PIN, LOW);
  delay(50);
  digitalWrite(BUZZER_PIN, HIGH);
  delay(50);
  digitalWrite(BUZZER_PIN, LOW);
}

void BuzzRFIDisNotRegistered() {
  digitalWrite(BUZZER_PIN, HIGH);
  delay(100);
  digitalWrite(BUZZER_PIN, LOW);
}

void BuzzlogIsSent() {
  digitalWrite(BUZZER_PIN, HIGH);
  delay(30);
  digitalWrite(BUZZER_PIN, LOW);
  delay(50);
  digitalWrite(BUZZER_PIN, HIGH);
  delay(30);
  digitalWrite(BUZZER_PIN, LOW);

}





bool isRegistered() {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : isRegistered()"));
  Serial.println();
#endif


  serverTime.detach();  ////////stop counting time for communicating with server

  String class_roll = "";   ////// Whole roll is stored here

  byte a = rfid.uid.uidByte[0];
  byte b = rfid.uid.uidByte[1];
  byte c = rfid.uid.uidByte[2];
  byte d = rfid.uid.uidByte[3];



#ifdef DEBUG
  Serial.println();
  Serial.print(F("DETECTED UID: "));
  Serial.print(a, HEX);
  Serial.print(" ");
  Serial.print(b, HEX);
  Serial.print(" ");
  Serial.print(c, HEX);
  Serial.print(" ");
  Serial.println(d, HEX);

  Serial.println(F("SEARCHING FOR A UID MATCH IN REGISTRY.."));
#endif
  Serial.print("roll start address: ");
  Serial.println(EEPROM_ROLL_START_ADDRESS);


  for (int i = 0; i < recSize; i++) {

    if (registry[i][0] == a && registry[i][1] == b && registry[i][2] == c && registry[i][3] == d) { //////A match is found in registry

      Serial.print("Matched ID: ");
      Serial.print(registry[i][0], HEX);
      Serial.print(registry[i][1], HEX);
      Serial.print(registry[i][2], HEX);
      Serial.println(registry[i][3], HEX);
      Serial.print("Matched index: ");
      Serial.println(i);


      roll_pointer = EEPROM_ROLL_START_ADDRESS + i * ROLL_BYTE_SIZE;                //////Roll size is taken as same as rfid uid size

      Serial.print("roll pointer: ");
      Serial.println(roll_pointer);


      for (int j = roll_pointer; j < (roll_pointer + ROLL_BYTE_SIZE); j++) {

        char val = readAddress(j);
        if (val == 94)continue;

        else {
          class_roll += String(val);
          Serial.println(val);
        }
      }
      lcdCardSection(CARD_MATCHED, class_roll);
      serverTime.attach(UPDATE_INTERVAL_SECOND, commandFlag);   //updateInterval is the time interval to send log
      return true;
    }
    yield();
  }

  String cardNo = "";
  char aa[2];

  for (byte i = 0; i < RFID_BYTE_SIZE; i++) {
    sprintf(aa, "%02x", rfid.uid.uidByte[i]);
    Serial.println (aa);
    cardNo += String(aa);
  }
  //String not_matched_card = String(a, HEX) + String(b, HEX) + String(c, HEX) + String(d, HEX);

  lcdCardSection(CARD_NOT_MATCHED, cardNo);
  serverTime.attach(UPDATE_INTERVAL_SECOND, commandFlag);   //updateInterval is the time interval to send log
  return false;
}


void saveLogToEEPROM() {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : saveLogToEEPROM()"));
  Serial.println();
#endif

  if ((EEPROM_LOG_END_ADDRESS - currentLogAddress) < LOG_BYTE_SIZE) {
    //lastAddressOfLog  = currentLogAddress;
    currentLogAddress = EEPROM_LOG_START_ADDRESS;      //////// go to the top of log section and replace data
#ifdef DEBUG
    Serial.println(F("Log roll over ...LAST ADDRESS OF log"));
    //Serial.println(lastAddressOfLog);
#endif
    //Flag.logOverflow = true;
  }

  //  if ((EEPROM_LOG_END_ADDRESS - currentLogAddress) > 0) {

#ifdef DEBUG
  Serial.print("log adding to the currentLogAddress:");
  Serial.println(currentLogAddress);
  Serial.print("corresponding roll stored at roll_pointer:");
  Serial.println(roll_pointer);
  int temp = currentLogAddress;
#endif
  for (int i = 0; i < ROLL_BYTE_SIZE; i++) {
    writeAddress(currentLogAddress, readAddress(roll_pointer + i));
    currentLogAddress++;
  }
  writeAddress(currentLogAddress, timeStamp.Hour);
  currentLogAddress++;
  writeAddress(currentLogAddress, timeStamp.Minute);
  currentLogAddress++;
  writeAddress(currentLogAddress, timeStamp.Second);
  currentLogAddress++;
  writeAddress(currentLogAddress, timeStamp.Year);
  currentLogAddress++;
  writeAddress(currentLogAddress, timeStamp.Month);
  currentLogAddress++;
  writeAddress(currentLogAddress, timeStamp.Day);
  currentLogAddress++;

  writeAddress(currentLogAddress,   'E');
  writeAddress(currentLogAddress + 1, 'N');
  writeAddress(currentLogAddress + 2, 'D');

  logNumber++;


#ifdef DEBUG
  Serial.println("Access is logged");
  Serial.print((char)readAddress(temp++));
  Serial.print((char)readAddress(temp++));
  Serial.print((char)readAddress(temp++));
  Serial.print((char)readAddress(temp++));
  Serial.print((char)readAddress(temp++));
  Serial.print((char)readAddress(temp++));
  Serial.print((char)readAddress(temp++));
  Serial.print((char)readAddress(temp++));
  Serial.print((char)readAddress(temp++));
  Serial.print((char)readAddress(temp++));
  Serial.print("    ");
  Serial.print(readAddress(temp++));
  Serial.print(readAddress(temp++));
  Serial.print(readAddress(temp++));
  Serial.print("    ");
  Serial.print(readAddress(temp++));
  Serial.print(readAddress(temp++));
  Serial.print(readAddress(temp++));
  Serial.print("    ");
  Serial.print((char)readAddress(temp++));
  Serial.print((char)readAddress(temp++));
  Serial.println((char)readAddress(temp));
#endif
}


void setTime() {

  timeStamp.Second++;
  if (timeStamp.Second >= 60) {
    timeStamp.Minute++;
    timeStamp.Second = 0;
    Flag.minuteFlag = true;
    lcdClockSection();
    wifiSection();                    // Show green or red boundary in LCD to show wifi status
  }

  if (timeStamp.Minute == 60) {
    timeStamp.Hour++;
    timeStamp.Minute = 0;
    Flag.hourFlag = true;
  }

  if (timeStamp.Hour == 24) {
    timeStamp.Hour = 0;
    Flag.dayFlag = true;
  }


}



void commandFlag() {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : commandFlag()"));
#endif

  command.checkForCommand = true;

}



void getCommands() {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : getCommands()"));
  Serial.println(F("GETTING COMMANDS FROM SERVER"));
#endif


  HTTPClient http;
  http.begin(command_server);
  int httpCode = http.GET();


#ifdef DEBUG
  Serial.print(F("SERVER REPLY:"));
  Serial.println(httpCode);
#endif


  String json = http.getString();

#ifdef DEBUG
  Serial.print(F("RECIEVED DATA :"));
  Serial.println(json);
#endif

  http.end();
  if (httpCode > 0) {
    if (json == "{}\n") {

#ifdef DEBUG
      Serial.println("NULL JSON RECIEVED, THE DEVICE MAY NOT BE REGISTERED.");
#endif

      command.deviceRegistered = false;
      command.update_registry = false;
      command.sendlog = false;
      command.updateFirmware = false;
      command.sendRecSize = false;
      return;
    }

    else command.deviceRegistered = true;


    StaticJsonBuffer<1000>jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(json);

    if (!root.success()) {
#ifdef DEBUG
      Serial.println(F("josn parsing failed"));
#endif
      return;
    }

    String date;
    String time;

    command.update_registry = (bool)root["change_available"][0];
    command.sendlog = root["uinfo_flag"][0];
    command.updateFirmware = root["update_flag"][0];
    command.sendRecSize = root["send_recsize"][0];

    date = root["date"][0].asString();
    time = root["time"][0].asString();
    timeStamp.Hour   = time.substring(0, 2).toInt();
    timeStamp.Minute = time.substring(3, 5).toInt();
    timeStamp.Second = time.substring(6, 8).toInt();

    timeStamp.Year   = date.substring(2, 4).toInt();
    timeStamp.Month  = date.substring(5, 7).toInt();
    timeStamp.Day    = date.substring(8, 10).toInt();

#ifdef DEBUG
    Serial.print(F("TIME :"));
    Serial.print(timeStamp.Hour);
    Serial.print(F("-"));
    Serial.print(timeStamp.Minute);
    Serial.print(F("-"));
    Serial.println(timeStamp.Second);

    Serial.print(F("DATE :"));
    Serial.print(timeStamp.Year);
    Serial.print(F("-"));
    Serial.print(timeStamp.Month);
    Serial.print(F("-"));
    Serial.println(timeStamp.Day);


    Serial.print(F("UPDATED COMMANDS FROM SERVER:"));
    Serial.print(F("update_registry :"));
    Serial.println(command.update_registry);

    Serial.print(F("deviceRegistered :"));
    Serial.println(command.deviceRegistered);

    Serial.print(F("sendlog :"));
    Serial.println(command.sendlog);

    Serial.print(F("update firmware :"));
    Serial.println(command.updateFirmware);

    Serial.print(F("send record size:"));
    Serial.println(command.sendRecSize);

    Serial.println();
    Serial.println(F("############################################################################"));

#endif

  }
}



void send_data() {     //// converts the user log to json format and returns the json string

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : send_data()"));
#endif

  int failedSendAttepmt = 0;
  int recurse;
  int tempStartAddress;
  int tempEndAddress;

#ifdef DEBUG
  Serial.print("UnsentlogStart:");
  Serial.println(unsentLogStartAddress);
#endif

  tempStartAddress = unsentLogStartAddress;
  tempEndAddress   = currentLogAddress;

  /*
    if (Flag.logOverflow == true) {
      tempEndAddress = lastAddressOfLog;
      Flag.logOverflow = false;
    }
  */
  if (unsentLogStartAddress > currentLogAddress) {
    tempEndAddress = EEPROM_LOG_END_ADDRESS;
    //Flag.logOverflow = false;
  }
  else {
    tempEndAddress = currentLogAddress;
  }


#ifdef DEBUG
  Serial.print("LOG SENDING FROM ADDRESS  ");
  Serial.print(tempStartAddress);
  Serial.print(" TO  ");
  Serial.println(tempEndAddress);
#endif

  if (tempStartAddress == tempEndAddress)return;

  lcdCardSection(BUSY, "");       // LCD prints that device is busy sending log to server
  recurse = (tempEndAddress - tempStartAddress) / (LOG_BYTE_SIZE * BATCH_SIZE);

#ifdef DEBUG
  Serial.print("TOTAL PACKAGES TO BE SENT :");
  Serial.println(recurse + 1);
#endif

  HTTPClient http;
  http.begin(log_server);
  http.addHeader("content-type", "text/json");

  for (int i = 0; i < recurse; i++) {
    String logData = convertLogToJson(tempStartAddress, tempStartAddress + BATCH_SIZE * LOG_BYTE_SIZE);
    int code = http.POST(logData);

#ifdef DEBUG
    Serial.println(code);
#endif

    if (code == HTTP_CODE_OK) {
      tempStartAddress = tempStartAddress + BATCH_SIZE * LOG_BYTE_SIZE;
      unsentLogStartAddress = tempStartAddress;
      failedSendAttepmt = 0;
      logNumber -= BATCH_SIZE;  /// Unsent log is reduced
    }
    else {

#ifdef DEBUG
      Serial.println(http.getString());
#endif
      i--;
      failedSendAttepmt++;
    }
    if (failedSendAttepmt == LOG_SEND_ATTEMPT)break;
    yield();
    delay(500);
  }

  if (failedSendAttepmt == 0 && tempStartAddress != tempEndAddress) {

#ifdef DEBUG
    Serial.println();
    Serial.println("SENDING THE REST.....");
    Serial.println();
#endif

    String logData = convertLogToJson(tempStartAddress, tempEndAddress);

    int code = http.POST(logData);


#ifdef DEBUG
    Serial.println(code);
    Serial.println(http.getString());
#endif
    if (code == HTTP_CODE_OK) {
      tempStartAddress = tempEndAddress;
      unsentLogStartAddress = tempStartAddress;
      logNumber = 0;
      //BuzzlogIsSent();
    }
    http.end();
  }

  if (unsentLogStartAddress == EEPROM_LOG_END_ADDRESS) {
    unsentLogStartAddress = EEPROM_LOG_START_ADDRESS;
  }
  saveAddressPointers(ULOG_START);
}


String convertLogToJson(int from, int to) {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : convertLogToJson()"));
#endif

  StaticJsonBuffer<2900> jsonBuffer;

  JsonObject& root = jsonBuffer.createObject();
  JsonObject& nestedObject1 = root.createNestedObject("Row");

  int logs = 0;
  String uid;
  String time;
  String date;
  String dateTime;


  for (int i = from; i < to; i++) {

    yield();

    JsonObject& nestedObject2 = nestedObject1.createNestedObject(String(logs));

    char roll_a[4];
    String roll = "";

    for (int j = i; j < i + ROLL_BYTE_SIZE; j++) {

      char val = readAddress(j);
      if (val == 94)continue;

      else {
        roll += String(val);
        Serial.println(val);
      }
    }

    char HOUR[4];
    char MINUTE[4];
    char SECOND[4];

    char YEAR[4];
    char MONTH[4];
    char DAY[4];

    i += (ROLL_BYTE_SIZE - 1);

    sprintf(HOUR, "%02d", readAddress(++i));
    sprintf(MINUTE, "%02d", readAddress(++i));
    sprintf(SECOND, "%02d", readAddress(++i));
    sprintf(YEAR, "%02d", readAddress(++i));
    sprintf(MONTH, "%02d", readAddress(++i));
    sprintf(DAY, "%02d", readAddress(++i));

    time = String(HOUR) + ":" + String(MINUTE) + ":" + String(SECOND);
    date = "20" + String(YEAR) + "-" + String(MONTH) + "-" + String(DAY);

    dateTime = (date + " " + time);

    nestedObject2["PIN"]      = roll;
    nestedObject2["DateTime"] = dateTime;
    nestedObject2["Verified"] = "1";
    nestedObject2["Status"]   = "1";
    nestedObject2["WorkCode"] = "0";

    logs++;
    yield();
  }

  nestedObject1["unit_id"] = UNIT_ID;

  String data;
  root.printTo(data);

#ifdef DEBUG
  Serial.println(data);
#endif
  return data;
}


void writeAddress(int address, byte val)
{
  int device;

  if ( address > 65535 ) {
    device = EEPROM_I2C_ADDRESS_2;
  }
  else {
    device = EEPROM_I2C_ADDRESS_1;
  }
  Wire.beginTransmission(device);
  Wire.write((int)(address >> 8));   // left-part of pointer address
  Wire.write((int)(address & 0xFF)); // and the right
  Wire.write(val);
  Wire.endTransmission();
  delay(10);
}

byte readAddress(int address)
{
  byte result; // returned value
  int device;

  if ( address > 65535 ) {
    device = EEPROM_I2C_ADDRESS_2;
  }
  else {
    device = EEPROM_I2C_ADDRESS_1;
  }
  Wire.beginTransmission(device); // these three lines set the pointer
  // position in the EEPROM
  Wire.write((int)(address >> 8));    // left-part of pointer address
  Wire.write((int)(address & 0xFF));  // and the right
  Wire.endTransmission();
  Wire.requestFrom(device, 1);    // now get the byte of data...
  result = Wire.read();
  return result;
}




void updateRegistry() {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : updateRegistry()"));
#endif

  command.cardsToDelete = false;
  lcdCardSection(BUSY, "");             // LCD prints that device is busy sending log to server


  HTTPClient http;
  http.begin(registry_server);          /// comp_table

  int httpCode = http.GET();

  if (httpCode < 0) {
    command.update_registry = true;     // if fails to connect, registry is to be updated later
    return;
  }
  String json = http.getString();

#ifdef DEBUG
  Serial.println(json);
#endif

  http.end();

  unsigned int newCardCounter = 0;
  unsigned int delCardCounter = 0;


  if (httpCode > 0) {

    if (json == "{}\n")return;

    else {
      StaticJsonBuffer<2500> jsonBuffer;
      JsonObject& root = jsonBuffer.parseObject(json);

      if (!root.success()) {

#ifdef DEBUG
        Serial.println(F("data parsing failed"));
#endif
        return;
      }

      JsonArray& nestedArray   = root["add_card"];
      JsonArray& nestedArray1  = root["add_roll"];
      JsonArray& nestedArray2  = root["remove_card"];

      String card;
      String rollS;
      String delCards;

      //resetAddressPointers(REGISTRY_ADDR);          //////// last Registry pointer goes to registry start address
      //loadRegistry();

      while (true) {
        card = "";
        rollS = "";
        delCards = "";
        card = root["add_card"][newCardCounter].asString();
        rollS = root["add_roll"][newCardCounter].asString();
        delCards = root["remove_card"][delCardCounter].asString();


#ifdef DEBUG

        Serial.print("TO BE ADDED, ");
        Serial.print(newCardCounter + 1);
        Serial.print("  UID :");
        Serial.print(card);
        Serial.print("  ROLL: ");
        Serial.print(rollS);
        Serial.print("   delCardCounter   ");
        Serial.print(delCardCounter);
        Serial.print("   TO BE DELETED: ");
        Serial.println(delCards);
#endif


        if (card == NULL && delCards == NULL) {
#ifdef DEBUG
          Serial.println("ALL NEW CARDS ADDED.ALL REQUESTED CARDS DELETED");
#endif
          break;                                //// if card value is null, end is reached
        }

        ///////// ELSE NEW CARDS OR CARDS TO BE DELETED PRESENT////////////////////////////
        byte uid[RFID_BYTE_SIZE] ;
        char roll[RFID_BYTE_SIZE] ;


        //////// Checking If there are cards to delete //////////////////////////////////////
        if (delCards != NULL) {
          command.cardsToDelete = true;    // there are cards to delete

          int tp = 0;

          for (int j = 0; j < RFID_BYTE_SIZE * 2; j += 2) {
            uidToBeDeleted[delCardCounter][tp] = strtol(delCards.substring(j, j + 2).c_str(), NULL, 16);
            tp++;
            yield();
          }
          delCardCounter++;

        }


        if (card != NULL) {



          /*
                   int tp = RFID_BYTE_SIZE;

                   for (int j = (( RFID_BYTE_SIZE * 2)-1) ; j >= 0; j += 2) {
                      uid[tp] = strtol(card.substring(j, j - 2).c_str(), NULL, 16);
                      tp--;
                      yield();
                    }
          */
          int tp = 0;

          for (int j = 0; j < RFID_BYTE_SIZE * 2; j += 2) {
            uid[tp] = strtol(card.substring(j, j + 2).c_str(), NULL, 16);
            tp++;
            yield();
          }

          // Roll can appear less then actual roll byte size. Like 111, it is to be formated 00000111 if 4 byte roll is considered.
          int roll_length = rollS.length();
          tp = 0;
          for (int j = 0; j < roll_length; j++) {
            //roll[j] = *(byte*)(rollS.substring(j,j+1).c_str());
            roll[j] = *(char*)rollS.substring(j, j + 1).c_str();
            yield();
          }
          for (int j = roll_length; j < ROLL_BYTE_SIZE; j++) {
            roll[j] = 94;
            yield();
          }

          addToRegistry(uid, roll);
          if ( Flag.registryAddSuccess = false) {
            break;
          }
          newCardCounter++;
        }

      }
    }
  }


  if (command.cardsToDelete == true) {
    deleteRegistry(delCardCounter);
  }

  saveAddressPointers(REGISTRY_ADDR);
  loadRegistry();

#ifdef DEBUG
  Serial.println("UPDATED REGISTRY:");
  printRegistry();
#endif

  http.begin(acknowledge_server);
  http.GET();
  command.update_registry = false;
  http.end();

}



void deleteRegistry(unsigned int numOfCardsToDel) {


#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : deleteRegistry()"));
  Serial.println(F("PREVIOUS REGISTRY"));
  printRegistry();
#endif


  int del_count = 0;
  int uidToBeDeleted_index[numOfCardsToDel];  ////// Holds the location in registry(in RAM) from where the uid is to be deleted

  for (int i = 0; i < recSize; i++) {
    for (int m = 0; m < numOfCardsToDel ; m++) {
      if (registry[i][0] == uidToBeDeleted[m][0] && registry[i][1] == uidToBeDeleted[m][1] && registry[i][2] == uidToBeDeleted[m][2] && registry[i][3] == uidToBeDeleted[m][3]) {

#ifdef DEBUG
        Serial.print(registry[i][0], HEX);
        Serial.print(registry[i][1], HEX);
        Serial.print(registry[i][2], HEX);
        Serial.print(registry[i][3], HEX);
        Serial.print("   IS DELETED    ");
        Serial.print(uidToBeDeleted[m][0], HEX);
        Serial.print(uidToBeDeleted[m][1], HEX);
        Serial.print(uidToBeDeleted[m][2], HEX);
        Serial.println(uidToBeDeleted[m][3], HEX);
#endif

        uidToBeDeleted_index[del_count] = i;

#ifdef DEBUG
        Serial.print("uidToBeDeleted_index // uid to be deleted found at location:");
        Serial.println(uidToBeDeleted_index[del_count]);
#endif

        del_count++;
      }
      yield();
    }
  }



  int j = 0;
  for (int i = 0; i < recSize; i++) {
    if (i == uidToBeDeleted_index[j]) {      /// If the location from where the uid is to be deleted is reached, skip the uid
      j++;
      continue;
    }

    else {
      for (int k = 0; k < RFID_BYTE_SIZE; k++) {
#ifdef DEBUG
        Serial.print(EEPROM_REGISTRY_START_ADDRESS + (i * RFID_BYTE_SIZE) + k - (j * RFID_BYTE_SIZE));
        Serial.print("---->");
        Serial.println(EEPROM_REGISTRY_START_ADDRESS + (i * RFID_BYTE_SIZE) + k);
#endif

        /*
           The location from where uid's to be deleted are stored in uidToBeDeleted_index....Suppose the current registry map is like==>
                      10 11 12 13 <==actual address in eeprom
             UID-1    aa bb cc dd
                      14 15 16 17
             UID-2    ff aa ee cc
                      18 19 20 21
             UID-2    11 22 33 44


            UID-2 is to be deleted.
            uidToBeDeleted_index[0] will be 1  // location 1 comes after 0
        */

        writeAddress((EEPROM_REGISTRY_START_ADDRESS + (i * RFID_BYTE_SIZE) + k - (j * RFID_BYTE_SIZE)), readAddress(EEPROM_REGISTRY_START_ADDRESS + (i * RFID_BYTE_SIZE) + k)); ////////adding ui
      }
      for (int k = 0; k < ROLL_BYTE_SIZE; k++) {
#ifdef DEBUG
        Serial.print(EEPROM_ROLL_START_ADDRESS + (i * ROLL_BYTE_SIZE) + k - (j * ROLL_BYTE_SIZE));
        Serial.print("---->");
        Serial.println(EEPROM_ROLL_START_ADDRESS + (i * ROLL_BYTE_SIZE) + k);
#endif
        writeAddress((EEPROM_ROLL_START_ADDRESS + (i * ROLL_BYTE_SIZE) + k - (j * ROLL_BYTE_SIZE)), readAddress(EEPROM_ROLL_START_ADDRESS + (i * ROLL_BYTE_SIZE) + k));                   /////// adding roll                 /////// adding roll
      }
    }
  }

#ifdef DEBUG
  Serial.print("reg add before  ");
  Serial.println(registryLastAddress);
  Serial.print("rec to delete  ");
  Serial.println(numOfCardsToDel);

#endif
  registryLastAddress -= del_count * 4;

#ifdef DEBUG
  Serial.print("reg add after   ");
  Serial.println(registryLastAddress);

#endif
  loadRegistry();
  command.cardsToDelete = false;
}



void report_restart() {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : report_restart()"));
#endif


  String report;

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["Device_id"] = UNIT_ID;
  root["Reset Cause"] = ESP.getResetReason();
  root.printTo(report);


#ifdef DEBUG
  Serial.println(report);
#endif
  HTTPClient http;
  http.begin(report_server);
  http.addHeader("content-type", "text/json");
  int code = http.POST(report);
  http.end();
  command.restarted = false;

}

void confirmUpdate() {

  HTTPClient http;
  http.begin(confirm_update);
  int code = http.GET();
  delay(100);
  http.end();

}


void executeUpdate() {

  Serial.end();
  SPI.end();
  Wire.endTransmission();
  t_httpUpdate_return ret = ESPhttpUpdate.update("104.236.205.172", 80, "/rams/static/mifare.bin");

}

void sendRecsize() {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : sendRecsize()"));
#endif


  String totalUsers;

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["Device Id"] = UNIT_ID;
  root["No of Users"] = recSize;
  root["Code Version"] = CodeVersion;
  root.printTo(totalUsers);


#ifdef DEBUG
  Serial.println(totalUsers);
#endif

  HTTPClient http;
  http.begin(reportRecSizeServer);
  http.addHeader("content-type", "text/json");
  int code = http.POST(totalUsers);
  http.end();
  if (code == HTTP_CODE_OK) {
    confirmSentRecSize();
    command.sendRecSize = false;
  }
}


void confirmSentRecSize() {

  HTTPClient http;
  http.begin(acknowlegde_recSize);
  int code = http.GET();
  http.end();

}



///////////////////////////// Display /////////////////////////////
void stellar_logo() {

  tft.fillScreen(ST7735_BLUE);
  float multiplier = 3.1416 / 180;

  uint16_t center_x = 30;
  uint16_t center_y = 65;
  uint16_t rad_large = 20;
  uint16_t rad_small = 12;

  uint16_t x_1 = center_x + (uint16_t)rad_large * sin(0 * multiplier);
  uint16_t y_1 = center_y + (uint16_t)rad_large * cos(0 * multiplier);

  uint16_t x_2 = center_x + (uint16_t)rad_large * sin(60 * multiplier);
  uint16_t y_2 = center_y + (uint16_t)rad_large * cos(60 * multiplier);

  uint16_t x_3 = center_x + (uint16_t)rad_large * sin(120 * multiplier);
  uint16_t y_3 = center_y + (uint16_t)rad_large * cos(120 * multiplier);

  uint16_t x_4 = center_x + (uint16_t)rad_large * sin(180 * multiplier);
  uint16_t y_4 = center_y + (uint16_t)rad_large * cos(180 * multiplier);

  uint16_t x_5 = center_x + (uint16_t)rad_large * sin(240 * multiplier);
  uint16_t y_5 = center_y + (uint16_t)rad_large * cos(240 * multiplier);

  uint16_t x_6 = center_x + (uint16_t)rad_large * sin(300 * multiplier);
  uint16_t y_6 = center_y + (uint16_t)rad_large * cos(300 * multiplier);


  uint16_t xx_1 = center_x + (uint16_t)rad_small * sin(30 * multiplier);
  uint16_t yy_1 = center_y + (uint16_t)rad_small * cos(30 * multiplier);

  uint16_t xx_2 = center_x + (uint16_t)rad_small * sin(90 * multiplier);
  uint16_t yy_2 = center_y + (uint16_t)rad_small * cos(90 * multiplier);

  uint16_t xx_3 = center_x + (uint16_t)rad_small * sin(150 * multiplier);
  uint16_t yy_3 = center_y + (uint16_t)rad_small * cos(150 * multiplier);

  uint16_t xx_4 = center_x + (uint16_t)rad_small * sin(210 * multiplier);
  uint16_t yy_4 = center_y + (uint16_t)rad_small * cos(210 * multiplier);

  uint16_t xx_5 = center_x + (uint16_t)rad_small * sin(270 * multiplier);
  uint16_t yy_5 = center_y + (uint16_t)rad_small * cos(270 * multiplier);

  uint16_t xx_6 = center_x + (uint16_t)rad_small * sin(330 * multiplier);
  uint16_t yy_6 = center_y + (uint16_t)rad_small * cos(330 * multiplier);

  tft.drawLine(x_1, y_1, xx_1, yy_1, ST7735_WHITE);
  tft.drawLine(xx_1, yy_1, x_2, y_2, ST7735_WHITE);
  tft.drawLine(x_2, y_2, xx_2, yy_2, ST7735_WHITE);
  tft.drawLine(xx_2, yy_2, x_3, y_3, ST7735_WHITE);
  tft.drawLine(x_3, y_3, xx_3, yy_3, ST7735_WHITE);
  tft.drawLine(xx_3, yy_3, x_4, y_4, ST7735_WHITE);
  tft.drawLine(x_4, y_4, xx_4, yy_4, ST7735_WHITE);
  tft.drawLine(xx_4, yy_4, x_5, y_5, ST7735_WHITE);
  tft.drawLine(x_5, y_5, xx_5, yy_5, ST7735_WHITE);
  tft.drawLine(xx_5, yy_5, x_6, y_6, ST7735_WHITE);
  tft.drawLine(x_6, y_6, xx_6, yy_6, ST7735_WHITE);
  tft.drawLine(xx_6, yy_6, x_1, y_1, ST7735_WHITE);
  tft.drawLine(x_1, y_1, x_4, y_4, ST7735_WHITE);
  tft.drawLine(x_2, y_2, x_5, y_5, ST7735_WHITE);
  tft.drawLine(x_3, y_3, x_6, y_6, ST7735_WHITE);

  uint16_t circle_x_1 = center_x + (uint16_t)1 * rad_large * sin(0 * multiplier);
  uint16_t circle_y_1 = center_y + (uint16_t)1 * rad_large * cos(0 * multiplier);

  uint16_t circle_x_2 = center_x + (uint16_t)1 * rad_large * sin(60 * multiplier);
  uint16_t circle_y_2 = center_y + (uint16_t)1 * rad_large * cos(60 * multiplier);

  uint16_t circle_x_3 = center_x + (uint16_t)1 * rad_large * sin(120 * multiplier);
  uint16_t circle_y_3 = center_y + (uint16_t)1 * rad_large * cos(120 * multiplier);

  uint16_t circle_x_4 = center_x + (uint16_t)1 * rad_large * sin(180 * multiplier);
  uint16_t circle_y_4 = center_y + (uint16_t)1 * rad_large * cos(180 * multiplier);

  uint16_t circle_x_5 = center_x + (uint16_t)1 * rad_large * sin(240 * multiplier);
  uint16_t circle_y_5 = center_y + (uint16_t)1 * rad_large * cos(240 * multiplier);

  uint16_t circle_x_6 = center_x + (uint16_t)1 * rad_large * sin(300 * multiplier);
  uint16_t circle_y_6 = center_y + (uint16_t)1 * rad_large * cos(300 * multiplier);

  tft.drawCircle(circle_x_1, circle_y_1, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_2, circle_y_2, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_3, circle_y_3, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_4, circle_y_4, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_5, circle_y_5, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_6, circle_y_6, 2, ST7735_WHITE);

  tft.setFont(&FreeSansBold9pt7b);
  tft.setTextSize(0);
  tft.setCursor(60, 70);
  tft.print(F("STELLAR"));

}
void welcome_page() {

  tft.fillScreen(ST7735_RED);
  tft.setFont(&FreeSansBold12pt7b);
  tft.setCursor(15, 70);
  tft.print(F("WELCOME"));
}

void wifiSetupPage() {

  if (WiFi.status() == WL_CONNECTED)return;
  tft.fillScreen(ST7735_BLACK);
  tft.setFont();
  tft.setFont(&FreeSansBold9pt7b);
  tft.setTextSize(0);
  tft.setTextWrap(true);
  tft.setTextColor(ST7735_RED);
  tft.setCursor(25, 25);
  tft.println(F("SETUP WIFI"));
  //tft.println(F("  CONNECTED"));
  tft.setFont();
  tft.setTextSize(0);
  tft.setCursor(10, 45);
  tft.setTextColor(ST7735_MAGENTA);
  tft.print(F("Check Instruction Manual"));
  tft.setCursor(10, 100);
  tft.println(F("THE DEVICE WILL START "));
  tft.setCursor(35, 110);
  tft.print(F("IN "));
  tft.print(WIFI_TIMEOUT);
  tft.println(F(" SECONDS "));
}

void device_not_registered_page() {

  espTime.detach();///////////// because of this time and wifi connection is updated on lcd in some interval
  tft.fillScreen(ST7735_RED);
  tft.setTextColor(ST7735_WHITE);
  tft.setFont(&FreeSansBold9pt7b);
  tft.setCursor(10, 20);
  tft.print(F("Register Device"));
  tft.setCursor(35, 60);
  tft.print(F("Device ID"));
  tft.setCursor(35, 80);
  tft.println(UNIT_ID);
  tft.setFont();
  tft.setTextSize(0);
  tft.setCursor(5, 100);
  tft.println("rumytecnologies.com/rams");


}

void rams_page() {
  tft.fillScreen(ST7735_BLACK);
  tft.setTextColor(ST7735_WHITE);
  tft.setFont(&FreeSansBold24pt7b);
  tft.setCursor(7, 80);
  tft.println(F("RAMS"));
}



void lcdCardSection(int condition, String roll) {


  tft.fillRect(0, 50, 180, 90, ST7735_BLACK);
  tft.setFont(&FreeSerifBold9pt7b);
  tft.setTextSize(0);


  if (condition == CARD_MATCHED) {
    tft.setTextColor(0x07ec);
    lcdRect(ST7735_BLACK);
    tft.setCursor(15, 85);
    // tft.println(F("THANKS"));
    //tft.println(F("       GRANTED"));
    tft.print(F("ID:"));
    tft.print(roll);
  }

  else if (condition == CARD_NOT_MATCHED) {
    tft.setTextColor(0xf80a);
    lcdRect(ST7735_BLACK);
    tft.setCursor(30, 75);
    tft.print(F("unregistered"));
    tft.setCursor(40, 100);
    tft.print(roll);
    delay(5000);
    // tft.println(F("         DENIED"));
  }

  else if (condition == REST) {
    tft.setTextColor(ST7735_DARKGREEN);
    lcdRect(ST7735_BLACK);
    tft.setCursor(15, 85);
    tft.println(F("  SCAN CARD"));

    //    tft.println(F("      TO ACCESS"));
  }
  else if (condition == BUSY) {
    tft.setTextColor(ST7735_DARKGREEN);
    lcdRect(ST7735_BLACK);
    tft.setCursor(10, 70);
    tft.println(F("  DEVICE BUSY"));
    tft.setCursor(10, 90);
    tft.println(F("  PLEASE WAIT"));
  }
}


void wifiSection() {

  tft.fillRect(120, 0, 60, 60, ST7735_BLACK);
  int COLOR;
  String WifiStatus;
  if (WiFi.status() != WL_CONNECTED) {
    COLOR = ST7735_RED;
    //   WifiStatus="NOT CONNECTED";
  }
  else {
    COLOR = ST7735_DARKGREEN;
    //  WifiStatus="  CONNECTED";
  }

  tft.setFont(&TomThumb);
  tft.setTextSize(0);
  tft.setCursor(130, 12);
  tft.setTextColor(COLOR);
  tft.print("WIFI");
}

void update_page() {
  tft.fillScreen(ST7735_RED);
  tft.setTextColor(ST7735_WHITE);
  tft.setFont(&FreeSansBold9pt7b);
  tft.setCursor(20, 60);
  tft.println(F("UPDATING......"));
  tft.setCursor(20, 80);
  tft.println(F("PLEASE WAIT"));
}

void lcdRect(uint16_t COLOR) {
  //  tft.fillRect(0,0,3,128,COLOR);
  //  tft.fillRect(0,0,160,3,COLOR);
  //  tft.fillRect(0,125,160,3,COLOR);
  //  tft.fillRect(157,0,3,128,COLOR);

  tft.fillRect(15, 47, 135, 1, COLOR);
  tft.fillRect(15, 47, 1, 66, COLOR);
  tft.fillRect(15, 113, 135, 1, COLOR);
  tft.fillRect(150, 47, 1, 66, COLOR);

}


void lcdClockSection() {

  dispUnitIDTime.detach();

  tft.setFont(&TomThumb);
  tft.setFont();
  tft.setTextSize(1);
  int mm = timeStamp.Minute;
  int hh = timeStamp.Hour;

  String hr;
  String min;
  String ampm;

  bool nightF;
  if (hh < 12) {        /// 0-11
    ampm = "AM";
    nightF = false;
  }

  else if (hh == 12) {    //////// 12 is 12PM.... 12-12 becomes 0 PM.... so seperately written
    ampm = "PM";
    nightF = true;
  }
  else {                //13-23 becomes 1-11
    ampm = "PM";
    hh = hh - 12;
    nightF = true;
  }




  ///////////////////////CLEAR AND PRINT HOUR ///////////////////////////////////


  ////////////FIRST CLEAR HOUR //////////////////////////////////////////

  if (Flag.hourFlag) {    ///////////hour is changed

    tft.setTextColor(ST7735_BLACK);
    tft.setCursor(18, 10);
    if (hh == 0) tft.print("11");           //////// after 11 comes 0
    else if (hh == 1 && nightF == false) tft.print("00");
    else if (hh == 1 && nightF == true) tft.print("12");

    else {
      hh -= 1;                             /// Else print previous number in Black
      if (hh < 10)hr = "0" + String(hh);
      else hr = String(hh);
      tft.print(hr);
      hh++;
    }

  }


  ///////////////NOW PRINT HOUR///////////////////////

  tft.fillRect(0, 0, 37, 30, ST7735_BLACK);
  tft.setCursor(18, 10);
  tft.setTextColor(ST7735_CYAN);
  if (hh < 10)hr = "0" + String(hh) + ":";
  else hr = String(hh) + ":";

  //    Serial.print(hr);

  tft.print(hr);

  /////////////////////////CLEAR AND PRINT MINUTE /////////////////////////

  if (Flag.minuteFlag) {
    tft.setTextColor(ST7735_BLACK);
    tft.setCursor(38, 10);
    if (mm == 0)tft.print("59");

    else {
      mm--;
      if (mm < 10)min = "0" + String(mm);
      else min = String(mm);
      tft.print(min);
      mm++;
    }
    Flag.minuteFlag = false;
  }

  tft.fillRect(37, 0, 16, 30, ST7735_BLACK);
  tft.setCursor(38, 10);
  tft.setTextColor(ST7735_CYAN);
  if (mm < 10)min = "0" + String(mm);
  else min = String(mm);

  //   Serial.print(min);
  tft.print(min);



  /////////////////////////CLEAR AND PRINT AM/PM/////////////////////////////////////

  if (Flag.hourFlag) {

    tft.setCursor(54, 10);
    tft.setTextColor(ST7735_BLACK);
    tft.print("AM");
    tft.setCursor(54, 20);
    tft.print("PM");
    Flag.hourFlag = false;
  }

  tft.fillRect(53, 0, 77, 30, ST7735_BLACK);
  tft.setTextColor(ST7735_CYAN);
  tft.setCursor(54, 10);
  tft.print(ampm);
  //Serial.println(ampm);


  /////////////////////////PRINT DATE ////////////////////////////////////////

  tft.fillRect(0, 20, 130, 30, ST7735_BLACK);
  tft.setCursor(18, 25);
  tft.print(timeStamp.Day);
  tft.print(F("."));
  tft.print(timeStamp.Month);
  tft.print(F(".20"));
  tft.print(timeStamp.Year);

  dispUnitIDTime.attach(30, displayUnitID);
}




void displayUnitID() {
  if (command.deviceRegistered == true) {
    tft.setFont(&TomThumb);
    tft.setFont();
    tft.setTextColor(ST7735_CYAN);
    tft.setTextSize(1);
    tft.fillRect(0, 20, 130, 30, ST7735_BLACK);
    tft.setCursor(18, 25);
    tft.print(UNIT_ID);
  }
}

void modify_serverTime(int server_time) {
  serverTime.detach();
  serverTime.attach(server_time, commandFlag);
}





















void getUnsentLog() {


#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : getUnsentLog()"));
#endif


  int totalLogs = 0;
  for (int i = EEPROM_LOG_START_ADDRESS; i < currentLogAddress;) {
    totalLogs++;
    delay(1);
    //Serial.print(F("UID: "));
    for (int uid = 0; uid < RFID_BYTE_SIZE; uid++) {
      // Serial.print(readAddress(i),HEX);
      i++;
    }

    // Serial.print(F("  TIME: "));

    for (int tym = 0; tym < 3; tym++) {
      //Serial.print(readAddress(i));
      i++;
    }
    // Serial.print(F("  DATE: "));

    for (int date = 0; date < 3; date++) {
      // Serial.print(readAddress(i));
      i++;
    }
    // Serial.println();
  }

  //  Serial.println(F("TOTAL LOGS:"));
  // Serial.println(totalLogs);
  /*

     UNSENT LOGS
  */

  Serial.println(F("Unsent LOG:"));
  Serial.println(logNumber);
  for (int i = unsentLogStartAddress; i < currentLogAddress;) {
    delay(1);
    Serial.print(F("UID: "));
    for (int uid = 0; uid < RFID_BYTE_SIZE; uid++) {
      Serial.print(readAddress(i), HEX);
      i++;
    }

    Serial.print(F("  TIME: "));

    for (int tym = 0; tym < 3; tym++) {
      Serial.print(readAddress(i));
      i++;
    }
    Serial.print(F("  DATE: "));

    for (int date = 0; date < 3; date++) {
      //Serial.print(readAddress(i));
      i++;
    }
    Serial.println();
  }
}




void printRegistry() {

  Serial.println("############################## REGISTRY ##############################################");
  Serial.println("_________________________________________________________________________________________");

  int reg = EEPROM_REGISTRY_START_ADDRESS;
  int rol = EEPROM_ROLL_START_ADDRESS;
  char tempUid[1];

  int roll_increment = 0;

  for (int card_increment = EEPROM_REGISTRY_START_ADDRESS; card_increment < registryLastAddress;) {
    for (int j = 0; j < RFID_BYTE_SIZE; j++) {
      sprintf(tempUid, "%2x", readAddress(card_increment + j));
      //Serial.print(readAddress(card_increment + j), HEX);
      Serial.print(tempUid);
    }

    Serial.print("    ");

    //Serial.print(EEPROM_ROLL_START_ADDRESS + roll_increment * ROLL_BYTE_SIZE);

    roll_increment = (card_increment - EEPROM_REGISTRY_START_ADDRESS) / RFID_BYTE_SIZE ;

    Serial.print(EEPROM_ROLL_START_ADDRESS + roll_increment * ROLL_BYTE_SIZE);

    Serial.print("    ");

    for (int j = 0; j < ROLL_BYTE_SIZE; j++) {
      //Serial.print(" roll : ");
      //Serial.print(EEPROM_ROLL_START_ADDRESS+roll_increment*ROLL_BYTE_SIZE+j);
      //Serial.print("  ");
      if (readAddress(EEPROM_ROLL_START_ADDRESS + roll_increment * ROLL_BYTE_SIZE + j) == 94) continue;
      else Serial.print((char)readAddress(EEPROM_ROLL_START_ADDRESS + roll_increment * ROLL_BYTE_SIZE + j));
      //Serial.print(readAddress(EEPROM_ROLL_START_ADDRESS+j+roll_increment));
    }

    card_increment += RFID_BYTE_SIZE;
    //roll_increment +=ROLL_BYTE_SIZE;
    Serial.println();
    yield();
  }

  Serial.println("_________________________________________________________________________________________");

}



void printEEPROM() {

  Serial.println("############################################################################");
  Serial.println();
  Serial.println("_________________________________________________________________________________________");
  Serial.print("|REGISTRY_HIGH_BYTE_ADDRESS(        ");
  Serial.print(REGISTRY_HIGH_BYTE_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(REGISTRY_HIGH_BYTE_ADDRESS));

  Serial.print("|REGISTRY_MID_BYTE_ADDRESS(         ");
  Serial.print(REGISTRY_MID_BYTE_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(REGISTRY_MID_BYTE_ADDRESS));

  Serial.print("|REGISTRY_LOW_BYTE_ADDRESS(         ");
  Serial.print(REGISTRY_LOW_BYTE_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(REGISTRY_LOW_BYTE_ADDRESS));


  Serial.print("|UNSENT_LOG_STARTING_ADDRESS_H(     ");
  Serial.print(UNSENT_LOG_STARTING_ADDRESS_H);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(UNSENT_LOG_STARTING_ADDRESS_H));

  Serial.print("|UNSENT_LOG_STARTING_ADDRESS_M(     ");
  Serial.print(UNSENT_LOG_STARTING_ADDRESS_M);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(UNSENT_LOG_STARTING_ADDRESS_M));

  Serial.print("|UNSENT_LOG_STARTING_ADDRESS_L(     ");
  Serial.print(UNSENT_LOG_STARTING_ADDRESS_L);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(UNSENT_LOG_STARTING_ADDRESS_L));
  Serial.println("_________________________________________________________________________________________");
  Serial.print("|EEPROM_REGISTRY_START_ADDRESS(     ");
  Serial.print(EEPROM_REGISTRY_START_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(EEPROM_REGISTRY_START_ADDRESS));
  Serial.println("_________________________________________________________________________________________");
  Serial.print("|EEPROM_ROLL_START_ADDRESS(         ");
  Serial.print(EEPROM_ROLL_START_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(EEPROM_ROLL_START_ADDRESS));
  Serial.println("_________________________________________________________________________________________");
  Serial.print("|EEPROM_LOG_START_ADDRESS(          ");
  Serial.print(EEPROM_LOG_START_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.print((char)readAddress(EEPROM_LOG_START_ADDRESS));
  Serial.print((char)readAddress(EEPROM_LOG_START_ADDRESS + 1));
  Serial.println((char)readAddress(EEPROM_LOG_START_ADDRESS + 2));

  Serial.print("|EEPROM_LOG_END_ADDRESS(           ");
  Serial.print(EEPROM_LOG_END_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(EEPROM_LOG_END_ADDRESS));
  Serial.println("_________________________________________________________________________________________");
}

