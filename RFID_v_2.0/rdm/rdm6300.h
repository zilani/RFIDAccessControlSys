#ifndef rdm6300_h
#define rdm6300_h

#include <SoftwareSerial.h>
#include "Arduino.h"


#define DEBUG true // Use this if you want to debug your RFID stuff
#define MAX_BITS 100
#define SEEED_EBRICK_RFID 26  // RFID Electronic Brick from Seeedstudio has 26 Bit Wiegand


/**** 
 * Struct for storing an RFID tag
 */
struct RFIDTag {
	int mfr;         // Manufacturer (?) Code (2 bytes), only useful in UART Mode
	long id;         // Tag ID (3 bytes)
	byte chk;        // Checksum (1 byte), only useful in UART Mode
	boolean valid;   // Validity of the Tag, based on the Checksum (UART Mode) or the parity bits (Wiegand Mode)
	char raw[13];    // The whole tag as a raw string, only useful in UART Mode
};


/***
 * Class for reading and checking RFID tags (UART Mode)
 */
 
 
class RDM6300
{
private:
	SoftwareSerial * _rfidIO;
	RFIDTag _tag;
	byte _bytesRead;
  byte _dataLen;
	boolean _idAvailable;
	
public: 
    RDM6300(int rxPin, int txPin);
    boolean isIdAvailable();
    RFIDTag readId();
    void    restart();
    static long hex2dec(String hexCode);
};
#endif	
