#ifndef source_h
#define source_h

#include "Fs.h"
#include <WiFiManager.h> 
#include <ESP8266WiFi.h> 
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <MFRC522.h>
#include "Arduino.h"
#include <Ticker.h>
#include "Wire.h"
#include <WiFiClientSecure.h>
#include <ESP8266WebServer.h>
#include <DNSServer.h> 
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <Fonts/FreeSansBold12pt7b.h>
#include <Fonts/FreeSansBold9pt7b.h>
#include <Fonts/FreeSansBold24pt7b.h>
#include <Fonts/FreeSans9pt7b.h>
#include <Fonts/TomThumb.h>
#include <Fonts/FreeSansOblique9pt7b.h>
#include <Fonts/FreeSerifBold9pt7b.h>



//////////////////////// DEUGGING  //////////////////////////////////////
#define DEBUG     true


//////////////Uncomment this to make the device complete new  ////////
//#define RESET_DEVICE    true


///////////////////// PIN CONFIGURATION ////////////////////////

#define BUZZER_PIN                        3
#define RFID_SS_PIN                       15
#define RFID_RST_PIN                      16   

#define TFT_CS                            0   //previusly 1 checked no change
#define TFT_RST                           16                     
#define TFT_DC                            2
#define TFT_SCLK                          14   
#define TFT_MOSI                          13 

////////////////////// CONFIGURING SYSTEM //////////////////////////////
#define MODEL                            "rtrfid"
#define UNIT_ID                          String(String(MODEL)+String(ESP.getChipId()))
#define AP_SSID                          "RFID AMSS"
#define AP_PASSWORD                      "password"
#define WIFI_TIMEOUT                      120

#define UPDATE_INTERVAL_SECOND            60      // Log sending and registry update checkint interval in second
#define BATCH_SIZE                        20      // Size of log that is send at a time
#define LOG_SEND_ATTEMPT                  4



#define MAX_LOG_EEPROM                    1000//11500    // Maximum log that is saved to the EEPROM
#define MAX_REGISTRY                      500//2000    // Maximum registry stored in EEPROM
#define LOG_BYTE_SIZE                     10 
#define RFID_BYTE_SIZE                    4       //// Roll size is taken as same as rfid uid size
#define REGISTRY_LOG_GAP                  10
#define MAX_SERVER_DATA                   50

//////////////////////// EEPROM SEGMENT MANAGEMENT //////////////////////
#define EEPROM_I2C_ADDRESS                0x50    // physical address of the external EEPROM for I2C commnunication
#define REGISTRY_HIGH_BYTE_ADDRESS        0x00
#define REGISTRY_LOW_BYTE_ADDRESS         0x01
#define UNSENT_LOG_STARTING_ADDRESS_H     0x02
#define UNSENT_LOG_STARTING_ADDRESS_L     0x03

#define EEPROM_REGISTRY_START_ADDRESS     0x0A   // 1st 10 byte stores pointer information
#define EEPROM_ROLL_START_ADDRESS         (MAX_REGISTRY*RFID_BYTE_SIZE+REGISTRY_LOG_GAP)
#define EEPROM_LOG_START_ADDRESS          (2*MAX_REGISTRY*RFID_BYTE_SIZE+REGISTRY_LOG_GAP*2)   // 10 empty bytes
#define EEPROM_LOG_END_ADDRESS            (EEPROM_LOG_START_ADDRESS+MAX_LOG_EEPROM*LOG_BYTE_SIZE)

          

//#define Fingerprint                    "E0 C4 96 AC 2E BE 53 6D 1C 4B 7D B1 AF 2F CE F5 D9 0A 09 9F" 
//#define registry_server                "http://rumytechnologies.com/rams_test/default/registered_user?unit_id=rtrfid0010"
#define registry_server                String("http://rumytechnologies.com/rams/comp_table.json?unit_id="+UNIT_ID)
#define acknowledge_server             String("http://rumytechnologies.com/rams/default/edited_users?unit_id="+UNIT_ID)        ///updated registry is appeared after a GET request is sent to this link
#define command_server                 String("http://rumytechnologies.com/rams/default/get_instruction.json?unit_id="+UNIT_ID)
#define log_server                     "http://rumytechnologies.com/rams/default/get_att_log.json"
#define report_server                  "http://rumytechnologies.com/rams/log_time"
#define confirm_update                 String("http://rumytechnologies.com/rams/firmware_updated?unit_id="+UNIT_ID)







/////////////////////////LCD ///////////////////////////////
#define CARD_NOT_MATCHED  0
#define CARD_MATCHED      1
#define REST              2
#define BUSY              3
//#define CLOCK_SEGMENT_HEIGHT       40


//////////////////////REGISTRY SETTING OR RESETTING COMMANDS ////////////////////////
#define REGISTRY_ADDR              0
#define ULOG_START                 1
#define RESET_FULL_EEPROM          2







struct Command{
  bool checkForCommand:1;
  bool update_registry:1;
  bool sendlog:1;
  bool deviceRegistered:1;
  bool restarted:1;
  bool cardsToDelete:1;       ///// 
  bool updateFirmware:1;
  };


struct flags{
  bool minuteFlag:1;
  bool hourFlag:1;
  bool dayFlag:1;
  bool logOverflow:1;
  };
  
struct dateTime{
  byte Hour;
  byte Minute;
  byte Second;
  byte Day;
  byte Month;
  byte Year;
  };

////////////////////////  STARTUP  /////////////////////////////

void sourceSetup();
void resetAddressPointers(int choice);
void saveAddressPointers();
void loadAddressPointers();

/////////////////////// REGISTRY //////////////////////////////
void loadRegistry();
void addToRegistry(byte uid[], byte roll[]);

////////////////////// BUZZER ////////////////////////////////
void BuzzRFIDdetected();
void BuzzRFIDisRegistered();
void BuzzRFIDisNotRegistered();
void BuzzlogIsSent();


///////////////////// LOGGING //////////////////////////////////
bool isRegistered();
void getUnsentLog();              /// for debugging
void saveLogToEEPROM();

void setTime();
void commandFlag();

void    getCommands();
String  convertLogToJson(int from , int to);
void    printRegistry();             /// for debugging


void writeAddress(int address, byte val);
byte readAddress(int address);
void send_data();

void updateRegistry();

void lcdCardSection(int matched,String roll);
void lcdClockSection();
void lcdRect(uint16_t COLOR);

void stellar_logo();
void wifiSection();
void welcome_page();
void wifiSetupPage();
void device_not_registered_page();
void rams_page();
void report_restart();
void deleteRegistry(unsigned int numOfCardsToDel);
void confirmUpdate();


void printEEPROM();
#endif
