#include "source.h"    
#include "Arduino.h"
#include <SoftwareSerial.h>

SoftwareSerial RFID (RFID_RX_PIN, RFID_TX_PIN);

extern Command command; 

boolean readTag = false;
String tagString;
char tagNumber[14];
char realTagNum[7];
static int initTime;
static int finalTime;
int tagId;
static int oldTag = 0;
int readVal;

void setup() {

delay(100);  

 RFID.begin(9600);
 
#ifdef DEBUG
  
  Serial.begin(115200);
  delay(2000);
  Serial.println("############################################################################");
  Serial.print(F("DEVICE JUST STARTED. DEVICE ID:"));
  Serial.println(UNIT_ID);

  Serial.println(registry_server);
  Serial.println(acknowledge_server);
  Serial.println(command_server);
  Serial.println(log_server);
  Serial.println(report_server);
  Serial.println(confirm_update);
  
  
#endif

  sourceSetup();
  command.restarted = true;

#ifdef  DEBUG

  Serial.println(F("SETUP COMPLETED"));
  Serial.println(F("ENTERING TO LOOP"));
  Serial.println();
  Serial.println();
  Serial.println(F("############################################################################"));
  printEEPROM();
  printRegistry();
  delay(2000);
  
#endif

}



void loop() {

 if(RFID.available()){    // if RFID is found , get the UID , search for a match in stored registry
      for(int i = 0; i <= 13; i++){
            readVal = RFID.read();
            //tagNumber[i] = RFID.read();
            #ifdef  DEBUG
                    Serial.println(F("################# RAW CARD VALUE #############"));
                    Serial.println(readVal,HEX);
            #endif
            tagNumber[i] = readVal;
            delay(5);
        }
       initTime = millis();
       while(Serial.available() != 0){
          char d = Serial.read();
          #ifdef  DEBUG
          Serial.print("Clearing Buffer  ");
          Serial.print(d);
          #endif
          } 
          #ifdef  DEBUG
      Serial.println("");
      #endif
      if(tagNumber[0] == char(2) && tagNumber[13] == char(3)){
        
      tagString=tagNumber;
      #ifdef  DEBUG
              Serial.print(F("tagString : "));
              Serial.println(tagString);
      #endif
      
      tagString.substring(5,11).toCharArray(realTagNum, sizeof(realTagNum));
      tagId = hex2int(realTagNum, 6);

          #ifdef  DEBUG
                  Serial.println(F("inside available code"));
          #endif
          BuzzRFIDdetected();   // buzzer signals that a card is detected  
          if(tagId != oldTag){
                oldTag = tagId;
                #ifdef  DEBUG
                        Serial.print(F("oldTag : "));
                        Serial.println(oldTag,HEX);
                #endif                
                readTag = true;
                #ifdef  DEBUG
                        Serial.print(F("tagID : "));
                        Serial.println(tagId,HEX);
                #endif
                 
                 if(isRegistered(tagId)){  
                      #ifdef DEBUG 
                             Serial.println(F("Registered ID found"));
                      #endif
                           saveLogToEEPROM();
                           BuzzRFIDisRegistered();   // buzzer signals that the detected rfid is registered
                           delay(1000);
                           lcdCardSection(REST,"");     // LCD goes back to normal "SWIPE CARD" position
         }
         
      else{

           #ifdef DEBUG 
            Serial.println(F("ID Not registered"));
           #endif


            BuzzRFIDisNotRegistered();   // buzzer signals that rfid is not registered
            delay(1000);
            lcdCardSection(REST,"");        // LCD goes back to normal "SWIPE CARD" position
      } 
    }      
  }
}

  else if(RFID.available()==0 && readTag==true){
    //Serial.println("inside not available code");
    finalTime = millis();
    if((finalTime-initTime) > 100){
      oldTag = 0;
      readTag = false;
      }
   }


  else if(command.checkForCommand)// Time to get commands from server
  {       
          command.checkForCommand=false;

          if(WiFi.status()==WL_CONNECTED)
          {
              getCommands();                    // Get commands from server and update necessary flags

              if(command.deviceRegistered==false)
              {
                  device_not_registered_page();
                  //delay(60000);
                  #ifdef DEBUG 
                      Serial.println(F("Device is not Registered, Restarting..."));
                  #endif          
              }

              if(command.sendlog)
              {
                  send_data();
              }

              if(command.update_registry)
              {
                  updateRegistry();
              }
              if(command.deviceRegistered == true)
              {
                  wifiSection();
                  lcdClockSection();
                  lcdCardSection(REST,"");        // LCD goes back to normal "SWIPE CARD" position
              } 
          }
    }

    else if(command.restarted){
    report_restart();
    command.restarted = false;
    }
    
  else if(command.updateFirmware==true && WiFi.status()==WL_CONNECTED){

      confirmUpdate();
      
      #ifdef DEBUG
      Serial.println(F("Updating firmware...."));
      #endif

      delay(1500); 
      Serial.end();
      SPI.end();
      Wire.endTransmission();
      
      t_httpUpdate_return ret=ESPhttpUpdate.update("104.236.205.172", 80, "/rams/static/RDM6300.bin");

    }
  else if(command.switchDevice == true){
    switch_device();
    }
  else if(restartESP()){
    Serial.println(F("ESP WILL RESTART"));
    ESP.restart();
    }
}


