#ifndef source_h
#define source_h

#include "Arduino.h"
#include "Fs.h"                     
#include "Wire.h"
#include <WiFiManager.h>  
#include <ESP8266WiFi.h> 
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <Ticker.h>
#include <WiFiClientSecure.h>
#include <ESP8266WebServer.h>
#include <SoftwareSerial.h>
#include <DNSServer.h> 
#include <Adafruit_GFX.h>            // Core graphics library
#include <Adafruit_ST7735.h>         // Hardware-specific library
/*
 *  LCD FONTS LIBRARY
 */
#include <Fonts/FreeSansBold12pt7b.h>
#include <Fonts/FreeSansBold9pt7b.h>
#include <Fonts/FreeSansBold24pt7b.h>
#include <Fonts/FreeSans9pt7b.h>
#include <Fonts/TomThumb.h>
#include <Fonts/FreeSansOblique9pt7b.h>
#include <Fonts/FreeSerifBold9pt7b.h>


//////////////////////// DEUGGING  //////////////////////////////////////

#define DEBUG     true

//////////////Uncomment this to clear the EEPROM and make the device complete new  ////////

#define RESET_DEVICE    true
                        
///////////////////// PIN CONFIGURATION ////////////////////////
#define BUZZER_PIN                        3
#define RFID_RX_PIN                       2
#define RFID_TX_PIN                       1

#define TFT_CS                            15   
#define TFT_RST                           16                 
#define TFT_DC                            0
#define TFT_SCLK                          14   
#define TFT_MOSI                          13 

//////////////////////// SPI EEPROM COMMANDS ///////////////////////////

#define WB_WRITE_ENABLE       0x06
#define WB_WRITE_DISABLE      0x04
#define WB_CHIP_ERASE         0xc7
#define WB_READ_STATUS_REG_1  0x05
#define WB_READ_DATA          0x03
#define WB_PAGE_PROGRAM       0x02
#define WB_JEDEC_ID           0x9f

#define SS                    4             // Chip select for SPI eeprom

////////////////////// CONFIGURING SYSTEM //////////////////////////////
#define MODEL                            "rtrfid"
#define UNIT_ID                          String(String(MODEL)+String(ESP.getChipId()))
#define AP_SSID                          "STELLAR_ACCESS_POINT"
#define AP_PASSWORD                      "12345678"
#define WIFI_TIMEOUT                     300

#define UPDATE_INTERVAL_SECOND            60      // Log sending and registry update checkin interval in second
#define BATCH_SIZE                        20      // Size of log that is send at a time
#define LOG_SEND_ATTEMPT                  4       // Number of attempts to be made to send log to server 



#define MAX_LOG_EEPROM                    11500   // Maximum log that is saved to the EEPROM
#define MAX_REGISTRY                      2000    // Maximum registry stored in EEPROM
#define LOG_BYTE_SIZE                     10 
#define RFID_BYTE_SIZE                    3       // Roll size is taken as same as rfid uid size
#define ROLL_BYTE_SIZE                    4
#define REGISTRY_LOG_GAP                  10
#define MAX_SERVER_DATA                   50

//////////////////////// EEPROM SEGMENT MANAGEMENT //////////////////////
#define EEPROM_I2C_ADDRESS                0x50    // physical address of the external EEPROM for I2C commnunication

#define REGISTRY_HIGH_BYTE_ADDRESS        0x00
#define REGISTRY_MID_BYTE_ADDRESS         0x01
#define REGISTRY_LOW_BYTE_ADDRESS         0x02

#define UNSENT_LOG_STARTING_ADDRESS_H     0x03
#define UNSENT_LOG_STARTING_ADDRESS_M     0x04
#define UNSENT_LOG_STARTING_ADDRESS_L     0x05

#define EEPROM_REGISTRY_START_ADDRESS     10  // 1st 10 byte stores pointer information
#define EEPROM_ROLL_START_ADDRESS         (MAX_REGISTRY*RFID_BYTE_SIZE+REGISTRY_LOG_GAP)
#define EEPROM_LOG_START_ADDRESS          ((2*MAX_REGISTRY*RFID_BYTE_SIZE)+(REGISTRY_LOG_GAP*2))   // 10 empty bytes
#define EEPROM_LOG_END_ADDRESS            (EEPROM_LOG_START_ADDRESS+MAX_LOG_EEPROM*LOG_BYTE_SIZE)



//#define Fingerprint                    "E0 C4 96 AC 2E BE 53 6D 1C 4B 7D B1 AF 2F CE F5 D9 0A 09 9F" 
//#define registry_server                "http://rumytechnologies.com/rams_test/default/registered_user?unit_id=rtrfid0010"
#define registry_server                String("http://rumytechnologies.com/rams/comp_table.json?unit_id="+UNIT_ID)
#define acknowledge_server             String("http://rumytechnologies.com/rams/default/edited_users?unit_id="+UNIT_ID)        ///updated registry is appeared after a GET request is sent to this link
#define command_server                 String("http://rumytechnologies.com/rams/default/get_instruction.json?unit_id="+UNIT_ID)
#define log_server                     "http://rumytechnologies.com/rams/default/get_att_log.json"
#define report_server                  "http://rumytechnologies.com/rams/log_time"
#define confirm_update                 String("http://rumytechnologies.com/rams/firmware_updated?unit_id="+UNIT_ID)
#define device_switch_server           String("http://rumytechnologies.com/rams/device_switch_request?unit_id="+UNIT_ID)


//////////////////// RESTART TIMES ///////////////

#define restartHour1  19
#define restartHour2  6
#define restratMinute 59
#define restartSecond 00

/////////////////////////LCD ///////////////////////////////
#define CARD_NOT_MATCHED  0
#define CARD_MATCHED      1
#define REST              2
#define BUSY              3
//#define CLOCK_SEGMENT_HEIGHT       40


//////////////////////REGISTRY SETTING OR RESETTING COMMANDS ////////////////////////
#define REGISTRY_ADDR              0
#define ULOG_START                 1
#define RESET_FULL_EEPROM          2





struct Command{
  bool checkForCommand:1;
  bool update_registry:1;
  bool sendlog:1;
  bool deviceRegistered:1;
  bool restarted:1;
  bool cardsToDelete:1;       
  bool updateFirmware:1;
  bool switchDevice:1;
  };


struct flags{
  bool minuteFlag:1;
  bool hourFlag:1;
  bool dayFlag:1;
  bool logOverflow:1;
  };
  
struct dateTime{
  byte Hour;
  byte Minute;
  byte Second;
  byte Day;
  byte Month;
  byte Year;
  };

////////////////////////  STARTUP  /////////////////////////////

void sourceSetup();
void resetAddressPointers(int choice);
void saveAddressPointers();
void loadAddressPointers();

/////////////////////// REGISTRY //////////////////////////////
void loadRegistry();                                 // loads the complete registry from EEPROM to the RAM
void addToRegistry(byte uid[], byte roll[]);         // adds a new registered user from server to the registry
void deleteRegistry(unsigned int numOfCardsToDel);   // deletes a user from the registry; takes in the number of cards to delete as input

////////////////////// BUZZER ////////////////////////////////
void BuzzRFIDdetected();
void BuzzRFIDisRegistered();
void BuzzRFIDisNotRegistered();
void BuzzlogIsSent();


///////////////////// LOGGING //////////////////////////////////
bool isRegistered(int tagId);     // takes in the RFID card id as input and checks if the device is registered
void getUnsentLog();              // (for debugging) prints all unsent logs
void saveLogToEEPROM();           // saves any verifed RFID card id to external EEPROM

void setTime();                   // sets the time flags
void commandFlag();               // sets the command flags

void    getCommands();            // pings the server at regular interval for command
String  convertLogToJson(int from , int to);
void    printRegistry();          // (for debugging) prints all the registered RFID card id along with roll


void writeAddress(int address, byte val); // write a byte to a definite address in EEPROM
byte readAddress(int address);            // reads a byte from definite address in EEPROM
void send_data();                         // sends the verified users log in a batch of 20 to the server

void updateRegistry();                    // updates the registry (users)

void lcdCardSection(int matched,String roll);  
void lcdClockSection();
void lcdRect(uint16_t COLOR);
void stellar_logo();
void wifiSection();
void welcome_page();
void wifiSetupPage();
void device_not_registered_page();
void rams_page();
void report_restart();
void confirmUpdate();
unsigned long hex2int(char *a, unsigned int len);
void printEEPROM();
bool restartESP();
void switch_device();
#endif
