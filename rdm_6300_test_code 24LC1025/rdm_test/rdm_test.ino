/*********************************************************************************
   Project Name: Stellar RFID Access Management System
   Author: Sopan Sarkar; Kazi Abu Zialni
   Contributor: None
   Author URI: https://www.linkedin.com/in/sopan-sarkar-915b39110/
               https://www.linkedin.com/in/kazi-abu-zilani-12276b122/
   Device Type: EM Card
   Version: 2.0.1
   License: Private
 *********************************************************************************/

#include "source.h"
#include "Arduino.h"

#include <SoftwareSerial.h>

SoftwareSerial RFID (RFID_RX_PIN, RFID_TX_PIN);

extern Command command;

/*********************************************************************************
    VARIALES USED TO RAD TAG ID FROM RDM6300
    readTag      ---> Flag to detect if a card was just previously read or not
    tagString    ---> Holds the tagNumber as string
    tagNumber[]  ---> This array stores the whole tag
    realTagNum[] ---> This array stores the real tag without start and end bytes
    initTime     ---> Stores the time when the RFID Card is brought near the sensor
    finalTime    ---> Stores the time when the RFID Card is taken away from sensor
    tagId        ---> Stores the 3 byte tagID
    oldTag       ---> Old tag data is stored in this variable
    readVal      ---> Data from serial port is stored in this variable
 *********************************************************************************/


boolean readTag = false;
String tagString;
char tagNumber[14];
char realTagNum[7];
static int initTime;
static int finalTime;
int tagId;
static int oldTag = 0;
int readVal;


/*********************************************************************************/

void setup() {

  RFID.begin(9600);                         // RDM6300 HAS A BAUD RATE OF 9600

#ifdef DEBUG
  Serial.begin(115200);                     // Sereial Monitor debugging baud rate 115200
  delay(2000);
  Serial.println("############################################################################");
  Serial.print(F("DEVICE JUST STARTED. DEVICE ID:"));
  Serial.println(UNIT_ID);                  // Prints the Unit Id of the device

  Serial.println(registry_server);
  Serial.println(acknowledge_server);
  Serial.println(command_server);
  Serial.println(log_server);
  Serial.println(report_server);
  Serial.println(confirm_update);
  Serial.println(reportRecSizeServer);
  Serial.println(acknowlegde_recSize);

#endif

  sourceSetup();                            // sourceSetup() function executes all the necessary setup code
  command.restarted = true;                 // After every restart this is made true to send reset cause and time to the server

#ifdef  DEBUG

  Serial.println(F("SETUP COMPLETED"));
  Serial.println(F("ENTERING TO LOOP"));
  Serial.println();
  Serial.println();
  Serial.println(F("############################################################################"));
  printEEPROM();                             // Prints the EEPROM memory allocations
  printRegistry();                           // Prints all the Registry i.e. user data
  delay(2000);

#endif

}



void loop() {


  /*********************************************************************************
     EM cards has a tag length of 14 bytes. The first byte 2 indicates the start
     and the last byte 3 indicates the end of the tag. Out of the rest 12 bytes,first
     4 bytes is manufacturer ID,next 6 byte is tag number and the rest 2 byte is
     checksum.For example, a tag is 2 52 48 48 48 56 54 66 49 52 70 51 56 3

     |Start byte|     MFG ID    |     Tag number    | Checksum |End byte|
     |    2     |  52 48 48 48  | 56 54 66 49 52 70 |  51 56   |    3   |

     RDM6300 has no way of detecting if a tag was read just previously.
   *********************************************************************************/
  if (RFID.available() && (command.deviceRegistered == true)) {
    /*******************************************************************************
       When a new tag is detected,it is saved in the array tagNumber[]
     *******************************************************************************/
    for (int i = 0; i <= 13; i++) {

      readVal = RFID.read();
#ifdef  DEBUG
      Serial.println(F("################# RAW CARD VALUE #############"));
      Serial.println(readVal, HEX);
#endif
      tagNumber[i] = readVal;
      delay(5);

    }

    initTime = millis();
    /*******************************************************************************
       This is used to clear the Serial input buffer.
     *******************************************************************************/
    while (Serial.available() != 0) {
      char d = Serial.read();
#ifdef  DEBUG
      Serial.print("Clearing Buffer  ");
      Serial.print(d);
#endif
    }

#ifdef  DEBUG
    Serial.println("");
#endif
    /*******************************************************************************
       Sometimes it is seen that the Serial buffer is not cleared properly. So, wrong
       values may be read. We check if the first and last bytes are 2 and 3 respectively.
       If it doesn't match we discard the value and take another read. We convert the
       tagNumber to string and place it in string variable tagString. realTagNum
       contains the actual 6 bytes of tagId that we need for verification.
     *******************************************************************************/
    if (tagNumber[0] == char(2) && tagNumber[13] == char(3)) {
      tagString = tagNumber;
#ifdef  DEBUG
      Serial.print(F("tagString : "));
      Serial.println(tagString);
#endif
      tagString.substring(5, 11).toCharArray(realTagNum, sizeof(realTagNum));
      tagId = hex2int(realTagNum, 6);
#ifdef  DEBUG
      Serial.println(F("inside available code"));
#endif
      BuzzRFIDdetected();             // buzzer signals that a card is detected
      /*******************************************************************************
        If a new Id is detected we check the registry for any match. If the Id matches
        we save the log to EEPROM.
      *******************************************************************************/
      if (tagId != oldTag) {
        oldTag = tagId;
#ifdef  DEBUG
        Serial.print(F("oldTag : "));
        Serial.println(oldTag, HEX);
#endif
        readTag = true;
#ifdef  DEBUG
        Serial.print(F("tagID : "));
        Serial.println(tagId, HEX);
#endif

        if (isRegistered(tagId)) {
#ifdef DEBUG
          Serial.println(F("Registered ID found"));
#endif
          saveLogToEEPROM();
          BuzzRFIDisRegistered();      // buzzer signals that the detected rfid is registered
          delay(1000);
          lcdCardSection(REST, "");    // LCD goes back to normal "SWIPE CARD" position
        }

        else {
#ifdef DEBUG
          Serial.println(F("ID Not registered"));
#endif
          BuzzRFIDisNotRegistered();      // buzzer signals that rfid is not registered
          delay(1000);
          lcdCardSection(REST, "");       // LCD goes back to normal "SWIPE CARD" position
        }
      }
    }
  }
  /*******************************************************************************
    If a card was perviously read and now it has been removed away from the sensor,
    we check the time interval between the time when the card was detected(initTime)
    and when the card was moved away(finalTime). If the time difference is more than
    100ms than the same card if brought again near the reader is read, else it wont
    read it. This is done because, RDM6300 reads the tagID continousy. It has no way
    of reading it once and stop. So when a new card is read, we save it in a variable
    oldTag. If tagId and old tag are equal, we discard the value. If the time differce
    between two reads is more than 100ms we clear the value in oldTag. 100ms time is
    taken because the minimum time differece between bring a card near and moving it
    away is almost 100ms.
  *******************************************************************************/
  else if (RFID.available() == 0 && readTag == true) {
    finalTime = millis();
    if ((finalTime - initTime) > 100) {
      oldTag = 0;
      readTag = false;
    }
  }

  /*******************************************************************************
    If command.checkForCommand flag is true, the device check pings the server for
    the commands. The commands are:
    Send Log to server
    Update Registry
    Update Firmware
    Report Restart Cause
    Device Switch Request
  *******************************************************************************/
  else if (command.checkForCommand) {
    command.checkForCommand = false;

    if (WiFi.status() == WL_CONNECTED) {
      getCommands();

      if (command.deviceRegistered == false) {
        device_not_registered_page();

#ifdef DEBUG
        Serial.println(F("Device is not Registered, Restarting..."));
#endif
      }
      if (command.sendlog) {
        send_data();
      }
      if (command.update_registry) {
        modify_serverTime(20);
        updateRegistry();
      }
      else {
        modify_serverTime(60);
      }

      if (command.deviceRegistered == true) {
        wifiSection();
        lcdClockSection();
        lcdCardSection(REST, "");       // LCD goes back to normal "SWIPE CARD" position
      }
    }
  }
  else if (command.restarted) {
    report_restart();
    command.restarted = false;
  }

  else if (command.sendRecSize == true) {
    sendRecsize();
    confirmSentRecSize();
  }

  else if (command.updateFirmware == true && WiFi.status() == WL_CONNECTED) {
    confirmUpdate();
#ifdef DEBUG
    Serial.println(F("Updating firmware...."));
#endif
    delay(1500);
    update_page();
    executeUpdate();
  }
}

/*********************************************************************************/

