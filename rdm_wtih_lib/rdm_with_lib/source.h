#ifndef source_h
#define source_h

/*********************************************************************************
    HEADER FILES
 *********************************************************************************/

#include "Arduino.h"
#include "Fs.h"
#include "Wire.h"
#include <WiFiManager.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <Ticker.h>
#include <WiFiClientSecure.h>
#include <ESP8266WebServer.h>
#include <SoftwareSerial.h>
#include <RDM6300.h>
#include <DNSServer.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>

/**********************************************************************************
    LCD FONTS LIBRARY
 **********************************************************************************/

#include <Fonts/FreeSansBold12pt7b.h>
#include <Fonts/FreeSansBold9pt7b.h>
#include <Fonts/FreeSansBold24pt7b.h>
#include <Fonts/FreeSans9pt7b.h>
#include <Fonts/TomThumb.h>
#include <Fonts/FreeSansOblique9pt7b.h>
#include <Fonts/FreeSerifBold9pt7b.h>

/*********************************************************************************
    DEBUGGING
    COMMENT OUT THE FOLLOWING LINE TO ENABLE SERIAL DEBUG
 *********************************************************************************/

#define DEBUG     true

/*********************************************************************************
    RESETTING THE EEPROM TO INITIAL STATE
    COMMENT OUT THE FOLLOWING TO RESET THE EEPROM AND CLEAR ALL THE EXSISTING DATA
 *********************************************************************************/
//#define RESET_DEVICE    true

/*********************************************************************************
    RESETTING THE WiFi PASSWORD TO REMOVE THE CREDENTIALS FROM EEPROM
    COMMENT OUT THE FOLLOWING TO RESET THE WiFi SETTINGS
 *********************************************************************************/
//#define RESET_WIFI true

/*********************************************************************************
    DISPLAY ORIENTATION DEFINITION
 *********************************************************************************/

#define VERTICAL true
//#define HORIZONTAL true

/*********************************************************************************
    Server definations
 *********************************************************************************/
//#define RAMS          true
#define RAMS_TEST_2   true
//#define LOCAL_SERVER  true

#if RAMS
  #define SERVER String("http://rumytechnologies.com/rams")
#elif  RAMS_TEST_2
  #define SERVER String("http://rumytechnologies.com/rams_test_2")
#else  LOCAL_SERVER
  #define SERVER String("http://192.168.0.105/rams_test_2")
#endif

/*********************************************************************************
    DEBUGGING
    COMMENT OUT THE FOLLOWING LINE TO ENABLE SERIAL DEBUG
 *********************************************************************************/
#define BUZZER_PIN                        3

#define RFID_RX_PIN                       2
#define RFID_TX_PIN                       1  //No need to connect

#define TFT_CS                            15
#define TFT_RST                           16
#define TFT_DC                            0
#define TFT_SCLK                          14
#define TFT_MOSI                          13

/*********************************************************************************
    SYSTEM CONFIGURATION
 *********************************************************************************/

#define CodeVersion                      "RDM_6.4.0"
#define MODEL                            "EM"                                           // EM for RDM6300 devices and MI for Mifare devices
#define UNIT_ID                          String(String(MODEL)+String(ESP.getChipId()))  // Gets the Chip ID of ESP8266 device
#define AP_SSID                          "STELLAR_ACCESS_POINT"                         // ESP8266 Access point name
#define AP_PASSWORD                      "12345678"                                     // ESP8266 Access point password
#define WIFI_TIMEOUT                      300                                           // The time ESP will stay in Access Point Mode. The time is in seconds.

#define UPDATE_INTERVAL_SECOND            60                                            // The time interval to Ping the server and check for Updates and Commnads
#define BATCH_SIZE                        20                                            // The maximum number of logs sent at a time
#define REG_BATCH_SIZE                    20
#define LOG_SEND_ATTEMPT                  4                                             // Number of attempts to be made to send log to the server incase of any failure

#define MAX_LOG_EEPROM                    2300                                          // Maximum log that is saved in the EEPROM
#define MAX_REGISTRY                      2000                                          // Maximum registry stored in the EEPROM
#define LOG_BYTE_SIZE                     16                                            // Length of each log data in bytes
#define RFID_BYTE_SIZE                    3                                             // Length of each RFID tag in bytes
#define ROLL_BYTE_SIZE                    10                                            // Length of each ROLL in bytes
#define REGISTRY_LOG_GAP                  10                                            // Gap between two EEPROM memory segments in bytes
#define MAX_SERVER_DATA                   50                                            // Maximum number of User Data that is send from the server at a time

/*********************************************************************************
    EEPROM SEGMENT MANAGEMENT
 *********************************************************************************/

#define EEPROM_I2C_ADDRESS_1              0x50
#define EEPROM_I2C_ADDRESS_2              0x54

#define REGISTRY_HIGH_BYTE_ADDRESS        0x00
#define REGISTRY_MID_BYTE_ADDRESS         0x01
#define REGISTRY_LOW_BYTE_ADDRESS         0x02

#define UNSENT_LOG_STARTING_ADDRESS_H     0x03
#define UNSENT_LOG_STARTING_ADDRESS_M     0x04
#define UNSENT_LOG_STARTING_ADDRESS_L     0x05

#define MEMORY_IN_MBITS                   1
#define MEMORY_IN_BYTES                   ((MEMORY_IN_MBITS*1024*1024)/8)
#define EEPROM_REGISTRY_START_ADDRESS     0x0A   // 1st 10 byte stores pointer information
#define EEPROM_ROLL_START_ADDRESS         (MAX_REGISTRY*RFID_BYTE_SIZE+2*REGISTRY_LOG_GAP)
#define EEPROM_LOG_START_ADDRESS          (MAX_REGISTRY*RFID_BYTE_SIZE+MAX_REGISTRY*ROLL_BYTE_SIZE+REGISTRY_LOG_GAP*3)   // 10 empty bytes
#define MAX_LOG_CAPACITY_EEPROM           ((MEMORY_IN_BYTES-EEPROM_LOG_START_ADDRESS)/LOG_BYTE_SIZE)
#define EEPROM_LOG_END_ADDRESS            ((MAX_LOG_CAPACITY_EEPROM*LOG_BYTE_SIZE)+EEPROM_LOG_START_ADDRESS)


/*********************************************************************************
    Server communication URLs
 *********************************************************************************/

#define registry_server                String(SERVER+"/comp_table.json?unit_id="+UNIT_ID)
#define acknowledge_server             String(SERVER+"/default/edited_users?unit_id="+UNIT_ID)        
#define command_server                 String(SERVER+"/default/get_instruction.json?unit_id="+UNIT_ID)
#define log_server                     String(SERVER+"/default/get_att_log.json")
#define report_server                  String(SERVER+"/log_time")
#define confirm_update                 String(SERVER+"/default/firmware_updated2")
//#define confirm_update                 String(SERVER+"/default/firmware_updated?unit_id="+UNIT_ID)
#define reportRecSizeServer            String(SERVER+"/default/send_rec_size")                                  
#define acknowlegde_recSize            String(SERVER+"/confirm_send_recsize.json?unit_id="+UNIT_ID)
#define cardRegistration_server        String(SERVER+"/default/scan_card.json")
#define registrationConfirm            String(SERVER+"/device_ready.json?unit_id="+UNIT_ID)
#define regTimeOutServer               String(SERVER+"/default/time_out.json?unit_id="+UNIT_ID)
#define sendUsersServer                String(SERVER+"/default/pull_registry")
#define confirmSentUsers               String(SERVER+"/default/confirmSentPullRegistry?unit_id="+UNIT_ID)
//#define firmwareUpdateServer           String(SERVER+"/static/RDM6300.bin")
#define OTAfileNameServer              String(SERVER+"/default/get_ota_file_name.json?unit_id="+UNIT_ID)


/*********************************************************************************
    LCD COMMAND DATA
 *********************************************************************************/

#define CARD_NOT_MATCHED                  0
#define CARD_MATCHED                      1
#define REST                              2
#define BUSY                              3

/*********************************************************************************
    RESETTING COMMANDS
 *********************************************************************************/

#define REGISTRY_ADDR                     0
#define ULOG_START                        1
#define RESET_FULL_EEPROM                 2

/*********************************************************************************/

struct Command {
  bool checkForCommand: 1;
  bool update_registry: 1;
  bool sendlog: 1;
  bool deviceRegistered: 1;
  bool restarted: 1;
  bool updateFirmware: 1;
  bool sendRecSize: 1;
  bool readSerial: 1;
  bool regMode: 1;
  bool confirmReg: 1;
  bool sendRegistry: 1;
};

struct flags {
  bool minuteFlag: 1;
  bool hourFlag: 1;
  bool dayFlag: 1;
  bool logOverflow: 1;
  bool registryAddSuccess: 1;
  bool updateSuccess: 1;
};

struct dateTime {
  byte Hour;
  byte Minute;
  byte Second;
  byte Day;
  byte Month;
  byte Year;
};

/*********************************************************************************
    STARTUP FUNCTIONS
 *********************************************************************************/

void sourceSetup();
void resetAddressPointers(int choice);
void saveAddressPointers();
void loadAddressPointers();
void modify_serverTime(int server_time);

/*********************************************************************************
    REGISTRY DATA FUNCTIONS
 *********************************************************************************/

void loadRegistry();                                            // loads the complete registry from EEPROM to the RAM
void addToRegistry(byte uid[], char roll[]);                    // adds a new registered user from server to the registry
void deleteRegistry(byte uidToBeDeleted[]);                     // deletes a user from the registry; takes in the number of cards to delete as input

/*********************************************************************************
    BUZZER FUNCTIONS
 *********************************************************************************/

void BuzzRFIDisRegistered();
void BuzzRFIDisNotRegistered();

/*********************************************************************************
    DATA LOGGING FUNCTIONS
 *********************************************************************************/

bool isRegistered();                                   // takes in the RFID card id as input and checks if the device is registered
void getUnsentLog();                                            // (for debugging) prints all unsent logs
void saveLogToEEPROM();                                         // saves any verifed RFID card id to external EEPROM
void setTime();                                                 // sets the time flags
void commandFlag();                                             // sets the command flags
void printRegistry();                                           // (for debugging) prints all the registered RFID card id along with roll
void writeAddress(int address, byte val);                       // write a byte to a definite address in EEPROM
byte readAddress(int address);                                  // reads a byte from definite address in EEPROM

/*********************************************************************************
    SERVER FUNCTIONS
 *********************************************************************************/

void getCommands();                                             // pings the server at regular interval for command
String convertLogToJson(int from , int to);                     // Converts log to JSON format
String convertRegistryToJson(int from_index, int to_index);
void send_data();                                               // sends the verified users log in a batch of 20 to the server
void sendRegistry();
void updateRegistry();                                          // updates the registry (users)
void report_restart();
void confirmUpdate();
void executeUpdate();
unsigned long hex2int(char *a, unsigned int len);
void printEEPROM();
void switch_device();
void confirmSentRecSize();
void sendRecsize();
void sendCardForRegistration(String data);
void confirmRegistrationMode();
void endRegMode();
void confirmSentRegistry();
void getFileName();

String getRequest(String url);
bool postRequest(String url, String payload);

/*********************************************************************************
    LCD DISPLAY FUNCTIONS
 *********************************************************************************/

void lcdCardSection(int matched, String roll);
void lcdClockSection();
void lcdRect(uint16_t COLOR);
void stellar_logo();
void wifiSection();
void welcome_page();
void wifiSetupPage();
void device_not_registered_page();
void displayUnitID();
void update_page();

/*********************************************************************************
    OTHER FUNCTIONS
 *********************************************************************************/
void regModeExit();

#endif
