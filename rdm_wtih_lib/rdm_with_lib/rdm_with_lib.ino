/*********************************************************************************
   Project Name: Stellar RFID Access Management System
   Author: Sopan Sarkar; Kazi Abu Zialni
   Contributor: None
   Author URI: https://www.linkedin.com/in/sopan-sarkar-915b39110/
               https://www.linkedin.com/in/kazi-abu-zilani-12276b122/
   Device Type: EM Card
   Version: 2.0.1
   License: Private
 *********************************************************************************/

#include "source.h"
#include "Arduino.h"
#include <SoftwareSerial.h>
#include <RDM6300.h>

RDM6300 RFID(RFID_RX_PIN, RFID_TX_PIN);
int tag;

extern Command command;
extern flags  Flag;
extern Ticker regModeExitTime;

void setup() {

#ifdef DEBUG
  Serial.begin(115200);                     // Sereial Monitor debugging baud rate 115200
  delay(2000);
  Serial.println("############################################################################");
  Serial.print(F("DEVICE JUST STARTED. DEVICE ID:"));
  Serial.println(UNIT_ID);                  // Prints the Unit Id of the device

  Serial.println(registry_server);
  Serial.println(acknowledge_server);
  Serial.println(command_server);
  Serial.println(log_server);
  Serial.println(report_server);
  Serial.println(confirm_update);
  Serial.println(reportRecSizeServer);
  Serial.println(acknowlegde_recSize);
  Serial.println(regTimeOutServer);
  Serial.println(sendUsersServer);
  Serial.println(confirmSentUsers);
  //Serial.println(firmwareUpdateServer);
#endif

  sourceSetup();                            // sourceSetup() function executes all the necessary setup code
  command.restarted = true;                 // After every restart this is made true to send reset cause and time to the server
  command.confirmReg = true;

#ifdef  DEBUG

  Serial.println(F("SETUP COMPLETED"));
  Serial.println(F("ENTERING TO LOOP"));
  Serial.println();
  Serial.println();
  Serial.println(F("############################################################################"));
  printEEPROM();                             // Prints the EEPROM memory allocations
  printRegistry();                           // Prints all the Registry i.e. user data
  delay(2000);

#endif

}


void loop() {
  if (command.regMode == false) {
    if (command.confirmReg == false) {
      endRegMode();
    }
    command.confirmReg = true;
    regModeExitTime.detach();

    if (RFID.isIdAvailable() > 0 && command.deviceRegistered == true) {
      tag = RFID.readId();
      Serial.print("TAG : ");
      Serial.println(tag);
      int first = millis();
      if (isRegistered()) {
#ifdef DEBUG
        Serial.println(F("Registered ID found"));
#endif
        saveLogToEEPROM();
        BuzzRFIDisRegistered();         // buzzer signals that the detected rfid is registered
        lcdCardSection(REST, "");       // LCD goes back to normal "SWIPE CARD" position
      }

      else {
#ifdef DEBUG
        Serial.println(F("ID Not registered"));
#endif
        lcdCardSection(CARD_NOT_MATCHED, RFID.realTagString);
        BuzzRFIDisNotRegistered();      // buzzer signals that rfid is not registered
        delay(5000);
        lcdCardSection(REST, "");       // LCD goes back to normal "SWIPE CARD" position
      }
      int last = millis();

      int timeval = last - first;
      Serial.print("Time taken in ms : ");
      Serial.println(timeval);
    }


    /*******************************************************************************
      If command.checkForCommand flag is true, the device check pings the server for
      the commands. The commands are:
      Send Log to server
      Update Registry
      Update Firmware
      Report Restart Cause
      Device Switch Request
    *******************************************************************************/
    else if (command.checkForCommand) {
      command.checkForCommand = false;

      if (WiFi.status() == WL_CONNECTED) {
        getCommands();

        if (command.deviceRegistered == false) {
          device_not_registered_page();

#ifdef DEBUG
          Serial.println(F("Device is not Registered, Restarting..."));
#endif
        }
        if (command.sendlog) {
          send_data();
          //RFID.enableRx(true);
        }
        if (command.update_registry) {
          modify_serverTime(20);
          updateRegistry();
        }
        else {
          modify_serverTime(60);
        }

        if (command.deviceRegistered == true) {
          wifiSection();
          lcdClockSection();
          lcdCardSection(REST, "");       // LCD goes back to normal "SWIPE CARD" position
        }
      }
    }

    else if (command.restarted) {
      report_restart();
      command.restarted = false;
    }

    else if (command.sendRecSize == true) {
      sendRecsize();
    }

    else if (command.sendRegistry == true) {
      sendRegistry();
    }


    else if (command.updateFirmware == true && WiFi.status() == WL_CONNECTED) {
      getFileName();
#ifdef DEBUG
      Serial.println(F("Updating firmware...."));
#endif
      delay(1500);
      update_page();
      executeUpdate();
      delay(1500);
      confirmUpdate();
    }
  }
  //////////////////////////////////////// REGISTRATION MODE ////////////////////////////////
  if (command.regMode == true) {
    if (command.confirmReg == true) {
      confirmRegistrationMode();
      command.confirmReg = false;
    }
    if (RFID.isIdAvailable() > 0 && command.deviceRegistered == true) {
      tag = RFID.readId();
      regModeExitTime.attach(60, regModeExit);
      Serial.print("Real Tag String :  ");
      Serial.println( String(tag, HEX));
      //sendCardForRegistration(RFID.realTagString);
      BuzzRFIDisNotRegistered();      // buzzer signals that rfid is not registered
      lcdCardSection(CARD_NOT_MATCHED, RFID.realTagString);
      sendCardForRegistration(RFID.realTagString);
      lcdCardSection(REST, "");
    }
  }
}
