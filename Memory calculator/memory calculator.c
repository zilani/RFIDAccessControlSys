#include <stdio.h>

void main(){

int memory_Mbit, memory_Kbit, memory_bit, memory_bytes;
int max_regsize,registry_gap,log_size,uid_size,roll_size;
int reg_start_address,reg_last_address,roll_start_address,roll_end_address,log_start_address,log_end_address;
int max_log;

printf("MEMEORY IN Mbit         : ");
scanf("%d", &memory_Mbit);
printf("MAXIMUM REGISTRY SIZE   : ");
scanf("%d", &max_regsize);
printf("REGISTRY LOG GAP        : ");
scanf("%d", &registry_gap);
printf("UID BYTE SIZE           : ");
scanf("%d", &uid_size);
printf("ROLL BYTE SIZE          : ");
scanf("%d", &roll_size);
printf("LOG BYTE SIZE           : ");
scanf("%d", &log_size);
printf("\n\n");

memory_Kbit  = memory_Mbit*1024;
memory_bit   = memory_Kbit*1024;
memory_bytes = memory_bit/8;

reg_start_address = 10;
reg_last_address  = (max_regsize*uid_size)+reg_start_address;
roll_start_address = (max_regsize*uid_size)+(2*registry_gap);
roll_end_address = (max_regsize*roll_size)+roll_start_address;
log_start_address = (max_regsize*uid_size)+(max_regsize*roll_size)+3*registry_gap;
max_log = (memory_bytes-log_start_address)/log_size;
log_end_address = (max_log*log_size)+log_start_address;

printf("MEMORY IN BYTES         : %d\n\n", memory_bytes);
printf("REGISTRY START ADDRESS  : %d\n", reg_start_address);
printf("REGISTRY END ADDRESS    : %d\n\n", reg_last_address);
printf("ROLL START ADDRESS      : %d\n", roll_start_address);
printf("ROLL END ADDRESS        : %d\n\n", roll_end_address);
printf("MAXIMUM LOG AVAILABLE   : %d\n\n", max_log);
printf("LOG START ADDRESS       : %d\n", log_start_address);
printf("LOG END ADDRESS         : %d\n", log_end_address);

}
