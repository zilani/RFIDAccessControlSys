#include "Arduino.h"
#include "source.h"

#ifdef MIFARE
MFRC522 rfid(RFID_SS_PIN, RFID_RST_PIN);
#endif

#ifdef RDM
extern RDM6300 RFID;
#endif

Ticker espTime;
Ticker serverTime;
Ticker dispUnitIDTime;
Ticker regModeExitTime;
Ticker scrollTextTime;



Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

WiFiManager wifiManager;

static String firmwareServer = "";
static String firmwareUrl = "";

bool minuteFlag = 0;
bool hourFlag = 0;
bool dayFlag = 0;

struct dateTime timeStamp;
struct Command  command;
struct flags  Flag;

static unsigned int logNumber = 0;                  // number of user log recorded in RAM;
static unsigned int recSize = 0;                    // Number of registered UID
static unsigned int currentLogAddress;              // initialization for EEPROM address.Some first bytes are resereved for registry. 100 byte empty gap between registry and log
static unsigned int unsentLogStartAddress;          // Address of the start of the unsent log
static unsigned int lastAddressOfLog = 0;           // End address of Log in EEPROM

static unsigned int registryLastAddress = EEPROM_REGISTRY_START_ADDRESS;              // Initially registry last address is equal to EEPROm registry start address
static byte registry[MAX_REGISTRY][RFID_BYTE_SIZE];                                                // Variable to load registry from EEPROM to RAM

static int roll_pointer;                            // Holds the address of ROll from EEPROM

static String text = "www.stellarbd.com  ";
static String t = "";
static int backColor = ST7735_RED;
static int textColor = ST7735_WHITE;

static uint8_t width = 18;
static uint8_t offset = 0;


char k[20];                                         // Holds debug data

static byte user[RFID_BYTE_SIZE];
/*********************************************************************************
    The sourceSetup() function contains all the necessary initializations.
 *********************************************************************************/

void sourceSetup() {

  pinMode(BUZZER_PIN, OUTPUT);
  digitalWrite(BUZZER_PIN, LOW);

  /*********************************************************************************
    Default time setup
  *********************************************************************************/

  timeStamp.Year = 17;
  timeStamp.Month = 1;
  timeStamp.Day = 1;
  timeStamp.Hour = 12;
  timeStamp.Minute = 1;
  timeStamp.Second = 1;

  /*********************************************************************************
    When rebooted Device Registered flag is true. Only server can make it false.
    This is done because, if the device fails to communicate with the server and the
    it is restarted than it won't show that the device is not registered.
  *********************************************************************************/

  command.deviceRegistered = true;
  command.updateFirmware = false;
  command.endConnection = false;

  tft.initR(INITR_BLACKTAB);
  tft.fillScreen(ST7735_BLACK);

#ifdef HORIZONTAL
  tft.setRotation(3);
#endif

#ifdef VERTICAL
  tft.setRotation(2);
#endif

  welcome_page();

  Wire.begin(1000000);
  delay(2000);

  stellar_logo();
  delay(2000);

  /*********************************************************************************
    Uncomment the following line "wifiManger.resetSettings()" we reset wifi SSID and
    password that was previously saved in the Flash ROM.
  *********************************************************************************/

#ifdef RESET_WIFI
  wifiManager.resetSettings();
#endif

  wifiSetupPage();
  wifiManager.setTimeout(WIFI_TIMEOUT);
  wifiManager.autoConnect(AP_SSID, AP_PASSWORD);



  /*********************************************************************************
    If ESP connects to the WIFI it receives commands from the server
  *********************************************************************************/
  if (WiFi.status() == WL_CONNECTED) {

#ifdef DEBUG
    Serial.println("WIFI CONNECTED");
#endif
    getCommands();                  // updates time, checks if the device is registered
  }

  if (command.deviceRegistered == false) {

    device_not_registered_page();

#ifdef DEBUG
    Serial.println(F("Device is not Registered"));
#endif

  }
  else {
    /*********************************************************************************
      TRY TO GET UPDATE AT SETUP IF NECESSARY
    *********************************************************************************/
    if (command.updateFirmware == true && WiFi.status() == WL_CONNECTED) {
      getFileName();

#ifdef DEBUG
      Serial.println(F("Updating firmware at setup....."));
#endif
      delay(1500);
      update_page();
      executeUpdate();
      confirmUpdate();
    }
    /*********************************************************************************/
    report_restart();
    delay(100);

    getMessage();
    delay(100);

#ifdef RESET_DEVICE
    resetAddressPointers(RESET_FULL_EEPROM);
#endif

    wifiSection();
    lcdClockSection();
    lcdCardSection(REST, "");
    scrollingTextRect();
    loadAddressPointers();
    lcdCardSection(BUSY, "");
    delay(10);
    loadRegistry();
    lcdCardSection(REST, "");
  }
  espTime.attach(1, setTime);
  serverTime.attach(UPDATE_INTERVAL_SECOND, commandFlag);   //updateInterval is the time interval to send log
  dispUnitIDTime.attach(30, displayUnitID);
  regModeExitTime.attach(60, regModeExit);
  scrollTextTime.attach_ms(250, scrollingText);
}

/*********************************************************************************
  loadAddressPointers() function loads the current address of Registry, Roll and
  Unsentlog to the RAM
*********************************************************************************/

void loadAddressPointers() {


#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : loadAddressPointers()"));
  Serial.println();
  Serial.print(F("registry start Address: "));
  Serial.println(EEPROM_REGISTRY_START_ADDRESS);
#endif
  lcdCardSection(BUSY, "");
  /*********************************************************************************
    Read Registry End location
  *********************************************************************************/
  byte high_byte  =  readAddress(REGISTRY_HIGH_BYTE_ADDRESS);
  byte mid_byte   =  readAddress(REGISTRY_MID_BYTE_ADDRESS);
  byte low_byte   =  readAddress(REGISTRY_LOW_BYTE_ADDRESS);

  registryLastAddress = high_byte * 65536 + mid_byte * 256 + low_byte;

#ifdef DEBUG
  Serial.print(F("registryLastAddress: "));
  Serial.println(registryLastAddress);
#endif

  /*********************************************************************************
    Read Unsent log address
  *********************************************************************************/

  high_byte  =  readAddress(UNSENT_LOG_STARTING_ADDRESS_H);
  mid_byte   =  readAddress(UNSENT_LOG_STARTING_ADDRESS_M);
  low_byte   =  readAddress(UNSENT_LOG_STARTING_ADDRESS_L);

  unsentLogStartAddress = high_byte * 65536 + mid_byte * 256 + low_byte;

#ifdef DEBUG
  Serial.print(F("unsentLogStartAddress: "));
  Serial.println(unsentLogStartAddress);
#endif

  //byte tmp;

  bool notfound = false;

  for (int i = unsentLogStartAddress; i < (EEPROM_LOG_END_ADDRESS + 3); i++) {

    if (readAddress(i) == 'E') {
      if (readAddress(i + 1) == 'N') {
        if (readAddress(i + 2) == 'D') {
          currentLogAddress = i; //// START contains 5 char
          notfound = false;
          break;
        }
        else {
          notfound = true;
        }
      }
      else {
        notfound = true;
      }
    }
    else {
      notfound = true;
    }
    yield();
  }

  if (notfound == true) {
    for (int i = EEPROM_LOG_START_ADDRESS; i < unsentLogStartAddress; i++) {// a change to find current log address faster
#ifdef DEBUG
      // Serial.print((char)tmp);
#endif

      if (readAddress(i) == 'E') {
        if (readAddress(i + 1) == 'N') {
          if (readAddress(i + 2) == 'D') {
            currentLogAddress = i; //// START contains 5 char
            break;
          }
        }
      }
      yield();
    }
  }

#ifdef DEBUG

  Serial.print(F("currentLogAddress: "));
  Serial.println(currentLogAddress);

#endif
}

/*********************************************************************************
  saveAddressPointers() function saves the address of the Registry, Roll and
  Unsent Log to EEPROM
*********************************************************************************/

void saveAddressPointers(int choice) {


#ifdef DEBUG
  Serial.println("ACCESSING TO EEPROM 10 BYTE... SAVING");
  Serial.println(F("Saved address pointers"));
#endif


  if (choice == REGISTRY_ADDR) {
    byte low  = registryLastAddress & 0xFF;
    byte mid  = registryLastAddress >> 8;
    byte high = registryLastAddress >> 16;

    writeAddress(REGISTRY_HIGH_BYTE_ADDRESS, high);
    writeAddress(REGISTRY_MID_BYTE_ADDRESS, mid);
    writeAddress(REGISTRY_LOW_BYTE_ADDRESS, low);
#ifdef DEBUG
    Serial.print(F("registry last address: "));
    Serial.println(registryLastAddress);
#endif
  }


  else if (choice == ULOG_START) {

    byte low  = unsentLogStartAddress & 0xFF;
    byte mid  = unsentLogStartAddress >> 8;
    byte high = unsentLogStartAddress >> 16;

    writeAddress(UNSENT_LOG_STARTING_ADDRESS_H, high);
    writeAddress(UNSENT_LOG_STARTING_ADDRESS_M, mid);
    writeAddress(UNSENT_LOG_STARTING_ADDRESS_L, low);
#ifdef DEBUG
    Serial.print(F("unsentLogStartAddress: "));
    Serial.println(unsentLogStartAddress);
#endif
    high  =  readAddress(UNSENT_LOG_STARTING_ADDRESS_H);
    mid   =  readAddress(UNSENT_LOG_STARTING_ADDRESS_M);
    low   =  readAddress(UNSENT_LOG_STARTING_ADDRESS_L);

    unsentLogStartAddress = high * 65536 + mid * 256 + low;
  }
}

/*********************************************************************************
  resetAddressPointers() function resets the address of the Registry, Roll and
  Unsent Log to initial position. There are three choices:
  a. Reset Registry Address Pointer
  b. Reset Unsent Log Address Pointer
  c. Reset the whole EEPROM ; i.e. All address Pointers
*********************************************************************************/

void resetAddressPointers(int choice) {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : resetAddressPointers()"));
  Serial.println();
#endif


  if (choice == REGISTRY_ADDR) {
    writeAddress(REGISTRY_HIGH_BYTE_ADDRESS, 0);
    writeAddress(REGISTRY_MID_BYTE_ADDRESS, 0);
    writeAddress(REGISTRY_LOW_BYTE_ADDRESS, EEPROM_REGISTRY_START_ADDRESS);

#ifdef  DEBUG
    Serial.println(F("SAVED REGISTRY ADDRESSES IN EEPROM ARE RESET"));
#endif
  }

  else if (choice == ULOG_START) {
    byte low  = EEPROM_LOG_START_ADDRESS & 0xFF;
    byte mid  = EEPROM_LOG_START_ADDRESS >> 8;
    byte high = EEPROM_LOG_START_ADDRESS >> 16;

    writeAddress(UNSENT_LOG_STARTING_ADDRESS_H, high);
    writeAddress(UNSENT_LOG_STARTING_ADDRESS_M, mid);
    writeAddress(UNSENT_LOG_STARTING_ADDRESS_L, low);

    writeAddress(EEPROM_LOG_START_ADDRESS,  'E');
    writeAddress(EEPROM_LOG_START_ADDRESS + 1, 'N');
    writeAddress(EEPROM_LOG_START_ADDRESS + 2, 'D');
#ifdef  DEBUG
    Serial.println(F("SAVED UNSENT LOG STARTING ADDRESSES IN EEPROM ARE RESET"));
#endif
  }

  else if (choice == RESET_FULL_EEPROM) {

    writeAddress(REGISTRY_HIGH_BYTE_ADDRESS, 0);
    writeAddress(REGISTRY_MID_BYTE_ADDRESS, 0);
    writeAddress(REGISTRY_LOW_BYTE_ADDRESS, EEPROM_REGISTRY_START_ADDRESS);

    byte low  = EEPROM_LOG_START_ADDRESS & 0xFF;
    byte mid  = EEPROM_LOG_START_ADDRESS >> 8;
    byte high = EEPROM_LOG_START_ADDRESS >> 16;

    writeAddress(UNSENT_LOG_STARTING_ADDRESS_H, high);
    writeAddress(UNSENT_LOG_STARTING_ADDRESS_M, mid);
    writeAddress(UNSENT_LOG_STARTING_ADDRESS_L, low);
    writeAddress(EEPROM_LOG_START_ADDRESS,  'E');
    writeAddress(EEPROM_LOG_START_ADDRESS + 1, 'N');
    writeAddress(EEPROM_LOG_START_ADDRESS + 2, 'D');

#ifdef  DEBUG
    Serial.println(F("ALL ADDRESSES IN EEPROM ARE RESET. (REGISTRY LOCAION ADDRESS, UNSENT LOG START ADDRESS and E,N,D written at the start of log)"));
#endif

  }
}

/*********************************************************************************
  the loadRegistry() function loads the whole Registry; i.e. the User Card number
  to the RAM.
*********************************************************************************/
void loadRegistry() {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : loadRegistry()"));
  Serial.println();
#endif


  recSize = 0;

#ifdef DEBUG
  Serial.println(F("REGISTRY LOADING TO RAM"));
  Serial.print(F("REGISTRY STARTS FROM EEPROM ADDRESS:"));
  Serial.print(EEPROM_REGISTRY_START_ADDRESS);
  Serial.print("  TO:");
  Serial.println(registryLastAddress);
#endif


  for (int i = EEPROM_REGISTRY_START_ADDRESS; i < registryLastAddress;) {
    for (int j = 0; j < RFID_BYTE_SIZE; j++) {
      registry[recSize][j] = readAddress(i);
      i++;
    }
    recSize++;
    yield();
  }

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println();
#endif

}

/*********************************************************************************
  The addToRegistry() function adds new Registry; i.e. User Card ID and Roll to
  the EEPROM.
*********************************************************************************/
void addToRegistry(byte uid[], char roll[]) {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : addToRegistry()"));
  Serial.println();
#endif


#ifdef DEBUG

  Serial.print(F("ADDING UID:"));
  for (int i = 0; i < RFID_BYTE_SIZE; i++) {
    Serial.print(uid[i], HEX);
  }

  Serial.print(" AND ROLL:");
  for (int i = 0; i < ROLL_BYTE_SIZE; i++) {
    Serial.print(roll[i]);
  }

  Serial.println("TO REGISTRY (IN EEPROM)");
  Serial.print("PREVIOUS REGISTRY LAST ADDRESS");
  Serial.println(registryLastAddress);

#endif

  bool addCard = false;
  bool matched = false;

  if (recSize == 0) {
    addCard = true;
  }

  for (int i = 0; i < recSize; i++) {

    for (int j = (ROLL_BYTE_SIZE - 1); j >= 0; j--) {
      if ((char)readAddress(EEPROM_ROLL_START_ADDRESS + j + i * ROLL_BYTE_SIZE) == roll[j]) {
        matched = true;
      }
      else {
        matched = false;
        break;
      }
      yield();
    }

    Serial.print("Matched : ");
    Serial.println(matched);


    if (matched == true) {
      Serial.println("A match is found in the EEPROM");
      Serial.println("Replacing the following UID");
      for (int z = 0; z < RFID_BYTE_SIZE; z++) {
        Serial.print(readAddress((EEPROM_REGISTRY_START_ADDRESS + i * RFID_BYTE_SIZE + z)));
        Serial.print("---->");
        writeAddress((EEPROM_REGISTRY_START_ADDRESS + i * RFID_BYTE_SIZE + z), uid[z]);
        Serial.println(readAddress((EEPROM_REGISTRY_START_ADDRESS + i * RFID_BYTE_SIZE + z)));
        yield();
      }
      addCard = false;
      break;
    }
    else addCard = true;
  }
  ////////////////////////////////////////////////////////////////////////////////////
  if (addCard == true) {
    addCard = false;
    int address ;

    for (int i = 0; i < ROLL_BYTE_SIZE; i++) {
      address = EEPROM_ROLL_START_ADDRESS + ((registryLastAddress - EEPROM_REGISTRY_START_ADDRESS) / RFID_BYTE_SIZE) * ROLL_BYTE_SIZE;
#ifdef DEBUG
      Serial.print("Roll Address : ");
      Serial.print(address);
      Serial.print ("  ");
      Serial.println(roll[i]);
#endif
      writeAddress((address + i), roll[i]);
    }

    int currentAddress = registryLastAddress;

    for (int i = 0; i < RFID_BYTE_SIZE; i++) {
#ifdef DEBUG
      Serial.print("Card Address : ");
      Serial.println(registryLastAddress);
#endif
      writeAddress(registryLastAddress, uid[i]);
      registryLastAddress++;
    }

    for (int i = 0; i < RFID_BYTE_SIZE; i++) {
#ifdef DEBUG
      Serial.print("Matching UID");
#endif
      if (readAddress(currentAddress + i) != uid[i]) {
        Flag.registryAddSuccess = false;
        break;
      }
      else {
        Flag.registryAddSuccess = true;
      }
    }
  }
#ifdef DEBUG
  Serial.print("UPDATED REGISTRY LAST ADDRESS");
  Serial.println(registryLastAddress);
#endif
  if ( Flag.registryAddSuccess = true) {
    loadRegistry();
  }
}


/*********************************************************************************
  BUZZER functions
*********************************************************************************/

void BuzzRFIDisRegistered() {
  digitalWrite(BUZZER_PIN, HIGH);
  delay(50);
  digitalWrite(BUZZER_PIN, LOW);
  delay(50);
  digitalWrite(BUZZER_PIN, HIGH);
  delay(80);
  digitalWrite(BUZZER_PIN, LOW);
}

void BuzzRFIDisNotRegistered() {
  digitalWrite(BUZZER_PIN, HIGH);
  delay(50);
  digitalWrite(BUZZER_PIN, LOW);
}



bool isRegistered( ) {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : isRegistered()"));
  Serial.println();
#endif

  serverTime.detach();  ////////stop counting time for communicating with server

  String class_roll = "";   ////// Whole roll is stored here
  bool cardMatched = false;

  //getUser();

#ifdef DEBUG
  Serial.println(F("SEARCHING FOR A UID MATCH IN REGISTRY.."));
  Serial.print("TagID : ");
  for (int i = 0; i < RFID_BYTE_SIZE; i++) {
    Serial.print(user[i], HEX);
  }
  Serial.println("");
#endif

  for (int slIndex = 0; slIndex < recSize; slIndex++) {
    for (int uidIndex = 0; uidIndex < RFID_BYTE_SIZE; uidIndex++) {
      if (registry[slIndex][uidIndex] == user[uidIndex]) {
        cardMatched = true;
      }
      else {
        cardMatched = false;
        break;
      }
    }

    if (cardMatched == true) {
      roll_pointer = EEPROM_ROLL_START_ADDRESS + slIndex * ROLL_BYTE_SIZE;

      for (int j = roll_pointer; j < (roll_pointer + ROLL_BYTE_SIZE); j++) {

        char val = readAddress(j);
        if (val == 94)continue;

        else {
          class_roll += String(val);

#ifdef DEBUG
          Serial.println(val);
#endif
        }
      }

      Flag.memoryFull = false;
      Serial.print("Unsent :  ");
      Serial.println(unsentLogStartAddress);
      Serial.print("Current :  ");
      Serial.println(currentLogAddress);
      Serial.print("Difference :  ");
      Serial.println((unsentLogStartAddress - currentLogAddress));

      if ((unsentLogStartAddress - currentLogAddress) <= LOG_BYTE_SIZE && (unsentLogStartAddress - currentLogAddress) > 0) {

        Serial.println("Inside first condition");
        Flag.memoryFull = true;

      }

      else {

        Serial.println("Inside second condition");
        Flag.memoryFull = false;
      }


      if (Flag.memoryFull == false) {
        lcdCardSection(CARD_MATCHED, class_roll);
      }
      else {
        lcdCardSection(MEMORY_FULL, "");
      }
      serverTime.attach(UPDATE_INTERVAL_SECOND, commandFlag);   //updateInterval is the time interval to send log
      return true;
    }
  }

  serverTime.attach(UPDATE_INTERVAL_SECOND, commandFlag);   //updateInterval is the time interval to send log
  return false;
}


void saveLogToEEPROM() {

  /*******************************************************************************
     SAVE DEBUG DATA TO EEPROM
  *******************************************************************************/
  if (command.error == true) {
    k[1] = 2;
    saveDebugToEEPROM(1);
  }
  /******************************************************************************/

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : saveLogToEEPROM()"));
  Serial.println();
#endif

  if ((EEPROM_LOG_END_ADDRESS - currentLogAddress) < LOG_BYTE_SIZE) {
#ifdef DEBUG
    Serial.println(F("Reached the end of Log Address...."));
    Serial.print(F("Log start address :"));
    Serial.println(EEPROM_LOG_START_ADDRESS);
    Serial.print(F("Log end address :"));
    Serial.println(EEPROM_LOG_END_ADDRESS);
    Serial.print(F("Current log address :"));
    Serial.println(currentLogAddress);
#endif

    currentLogAddress = EEPROM_LOG_START_ADDRESS;      // go to the top of log section and replace data

#ifdef DEBUG
    Serial.println(F("Log roll over ...LAST ADDRESS OF log"));
#endif
  }

#ifdef DEBUG
  Serial.println(F("Reached the end of Log Address...."));
  Serial.print(F("Unsent start address :"));
  Serial.println(unsentLogStartAddress);
  Serial.print(F("Log end address :"));
  Serial.println(EEPROM_LOG_END_ADDRESS);
  Serial.print(F("Current log address :"));
  Serial.println(currentLogAddress);
#endif

  if (Flag.memoryFull == false) {

#ifdef DEBUG
    Serial.print("log adding to the currentLogAddress:");
    Serial.println(currentLogAddress);
    Serial.print("corresponding roll stored at roll_pointer:");
    Serial.println(roll_pointer);
    int temp = currentLogAddress;
#endif

    for (int i = 0; i < ROLL_BYTE_SIZE; i++) {
      writeAddress(currentLogAddress, readAddress(roll_pointer + i));
      currentLogAddress++;
    }
    writeAddress(currentLogAddress, timeStamp.Hour);
    currentLogAddress++;
    writeAddress(currentLogAddress, timeStamp.Minute);
    currentLogAddress++;
    writeAddress(currentLogAddress, timeStamp.Second);
    currentLogAddress++;
    writeAddress(currentLogAddress, timeStamp.Year);
    currentLogAddress++;
    writeAddress(currentLogAddress, timeStamp.Month);
    currentLogAddress++;
    writeAddress(currentLogAddress, timeStamp.Day);
    currentLogAddress++;

    writeAddress(currentLogAddress,   'E');
    writeAddress(currentLogAddress + 1, 'N');
    writeAddress(currentLogAddress + 2, 'D');

    logNumber++;


#ifdef DEBUG
    Serial.println("Access is logged");
    Serial.print((char)readAddress(temp++));
    Serial.print((char)readAddress(temp++));
    Serial.print((char)readAddress(temp++));
    Serial.print((char)readAddress(temp++));
    Serial.print((char)readAddress(temp++));
    Serial.print((char)readAddress(temp++));
    Serial.print((char)readAddress(temp++));
    Serial.print((char)readAddress(temp++));
    Serial.print((char)readAddress(temp++));
    Serial.print((char)readAddress(temp++));
    Serial.print("    ");
    Serial.print(readAddress(temp++));
    Serial.print(readAddress(temp++));
    Serial.print(readAddress(temp++));
    Serial.print("    ");
    Serial.print(readAddress(temp++));
    Serial.print(readAddress(temp++));
    Serial.print(readAddress(temp++));
    Serial.print("    ");
    Serial.print((char)readAddress(temp++));
    Serial.print((char)readAddress(temp++));
    Serial.println((char)readAddress(temp));
#endif
  }
}


void setTime() {

  timeStamp.Second++;
  if (timeStamp.Second >= 60) {
    timeStamp.Minute++;
    timeStamp.Second = 0;
    Flag.minuteFlag = true;
    //lcdClockSection();
    wifiSection();                    // Show green or red boundary in LCD to show wifi status
  }

  if (timeStamp.Minute == 60) {
    timeStamp.Hour++;
    timeStamp.Minute = 0;
    Flag.hourFlag = true;
  }

  if (timeStamp.Hour == 24) {
    timeStamp.Hour = 0;
    Flag.dayFlag = true;
  }
}


void commandFlag() {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : commandFlag()"));
#endif
  command.checkForCommand = true;
}


void getCommands() {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : getCommands()"));
  Serial.println(F("GETTING COMMANDS FROM SERVER"));
#endif

  String json = getRequest(command_server);

  if (json != "failed") {

    Flag.internet = true;

    if (json == "{}\n") {

#ifdef DEBUG
      Serial.println("NULL JSON RECIEVED, THE DEVICE MAY NOT BE REGISTERED.");
#endif

      command.deviceRegistered = false;
      command.endConnection = false;
      command.update_registry = false;
      command.sendlog = false;
      command.updateFirmware = false;
      command.sendRecSize = false;
      command.sendRegistry = false;
      command.regMode = false;
      command.error = false;
      command.rebootEsp = 0;
      command.messagePresent = 0;
      return;
    }

    else command.deviceRegistered = true;


    StaticJsonBuffer<1000>jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(json);

    if (!root.success()) {
#ifdef DEBUG
      Serial.println(F("josn parsing failed"));
#endif
      return;
    }

    String date;
    String time;

    command.endConnection = (bool)root["billing"][0];
    command.update_registry = (bool)root["change_available"][0];
    command.sendlog = (bool)root["uinfo_flag"][0];
    command.updateFirmware = (bool)root["update_flag"][0];
    command.sendRegistry = (bool)root["send_registry"][0];
    command.sendRecSize = (bool)root["send_recsize"][0];
    command.regMode = (bool)root["user_registration_mode"][0];
    command.rebootEsp = root["reset"][0];
    command.error = (bool)root["debug_log"][0];
    command.messagePresent = (bool)root["message"][0];

    date = root["date"][0].asString();
    time = root["time"][0].asString();
    timeStamp.Hour   = time.substring(0, 2).toInt();
    timeStamp.Minute = time.substring(3, 5).toInt();
    timeStamp.Second = time.substring(6, 8).toInt();

    timeStamp.Year   = date.substring(2, 4).toInt();
    timeStamp.Month  = date.substring(5, 7).toInt();
    timeStamp.Day    = date.substring(8, 10).toInt();

#ifdef DEBUG
    Serial.print(F("TIME :"));
    Serial.print(timeStamp.Hour);
    Serial.print(F("-"));
    Serial.print(timeStamp.Minute);
    Serial.print(F("-"));
    Serial.println(timeStamp.Second);

    Serial.print(F("DATE :"));
    Serial.print(timeStamp.Year);
    Serial.print(F("-"));
    Serial.print(timeStamp.Month);
    Serial.print(F("-"));
    Serial.println(timeStamp.Day);


    Serial.print(F("UPDATED COMMANDS FROM SERVER:"));
    Serial.print(F("update_registry :"));
    Serial.println(command.update_registry);

    Serial.print(F("End Connection :"));
    Serial.println(command.endConnection);

    Serial.print(F("deviceRegistered :"));
    Serial.println(command.deviceRegistered);

    Serial.print(F("sendlog :"));
    Serial.println(command.sendlog);

    Serial.print(F("update firmware :"));
    Serial.println(command.updateFirmware);

    Serial.print(F("send record size:"));
    Serial.println(command.sendRecSize);

    Serial.print(F("registration mode:"));
    Serial.println(command.regMode);

    Serial.print(F("Send all users:"));
    Serial.println(command.sendRegistry);


    Serial.print(F("Reset ESP:"));
    Serial.println(command.rebootEsp);

    Serial.print(F("New Message Presnet:"));
    Serial.println(command.messagePresent);

    Serial.println();
    Serial.println(F("############################################################################"));

#endif

  }
  else Flag.internet = false;
}


void send_data() {

  /*******************************************************************************
     SAVE DEBUG DATA TO EEPROM
  *******************************************************************************/
  if (command.error == true) {
    k[2] = 3;
    saveDebugToEEPROM(2);
  }
  /******************************************************************************/

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : send_data()"));
#endif

  int failedSendAttepmt = 0;
  int recurse;
  int tempStartAddress;
  int tempEndAddress;

#ifdef DEBUG
  Serial.print("UnsentlogStart:");
  Serial.println(unsentLogStartAddress);
#endif

  tempStartAddress = unsentLogStartAddress;
  tempEndAddress   = currentLogAddress;

  if (unsentLogStartAddress > currentLogAddress && unsentLogStartAddress != EEPROM_LOG_END_ADDRESS) {
    tempEndAddress = EEPROM_LOG_END_ADDRESS;
  }

  else if (unsentLogStartAddress > currentLogAddress && unsentLogStartAddress == EEPROM_LOG_END_ADDRESS) {
    unsentLogStartAddress = EEPROM_LOG_START_ADDRESS;
    tempStartAddress = unsentLogStartAddress;
  }

  else {
    tempEndAddress = currentLogAddress;
  }



#ifdef DEBUG
  Serial.print("LOG SENDING FROM ADDRESS  ");
  Serial.print(tempStartAddress);
  Serial.print(" TO  ");
  Serial.println(tempEndAddress);
#endif
  if (tempStartAddress == tempEndAddress && Flag.logOverFlow == false )return;

  lcdCardSection(BUSY, "");
  recurse = (tempEndAddress - tempStartAddress) / (LOG_BYTE_SIZE * BATCH_SIZE);

#ifdef DEBUG
  Serial.print("TOTAL PACKAGES TO BE SENT :");
  Serial.println(recurse + 1);
#endif

  for (int i = 0; i < recurse; i++) {

    String logData = convertLogToJson(tempStartAddress, tempStartAddress + BATCH_SIZE * LOG_BYTE_SIZE);
    bool postReqSuccess = postRequest(log_server, logData);

    if (postReqSuccess == true) {
      tempStartAddress = tempStartAddress + BATCH_SIZE * LOG_BYTE_SIZE;
      unsentLogStartAddress = tempStartAddress;
      failedSendAttepmt = 0;
      logNumber -= BATCH_SIZE;
    }
    else {
      i--;
      failedSendAttepmt++;
    }
    if (failedSendAttepmt == LOG_SEND_ATTEMPT) {
      break;
    }
    yield();
    delay(500);
  }

  if (failedSendAttepmt == 0 && tempStartAddress != tempEndAddress) {

#ifdef DEBUG
    Serial.println();
    Serial.println("SENDING THE REST.....");
    Serial.println();
#endif

    String logData = convertLogToJson(tempStartAddress, tempEndAddress);

    bool postReqSuccess = postRequest(log_server, logData);

    if (postReqSuccess == true) {
      tempStartAddress = tempEndAddress;
      unsentLogStartAddress = tempStartAddress;
      logNumber = 0;
    }
  }
  /*
    if (unsentLogStartAddress == EEPROM_LOG_END_ADDRESS) {
      unsentLogStartAddress = EEPROM_LOG_START_ADDRESS;
    }
  */
  saveAddressPointers(ULOG_START);
}


String convertLogToJson(int from, int to) {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : convertLogToJson()"));

  Serial.print("Start address : ");
  Serial.println(from);
  Serial.print("End address : ");
  Serial.println(to);

#endif

  StaticJsonBuffer<2900> jsonBuffer;

  JsonObject& root = jsonBuffer.createObject();
  JsonObject& nestedObject1 = root.createNestedObject("Row");

  int logs = 0;
  String uid;
  String time;
  String date;
  String dateTime;


  for (int i = from; i < to; i++) {

    yield();

    JsonObject& nestedObject2 = nestedObject1.createNestedObject(String(logs));

    String roll = "";

    for (int j = i; j < i + ROLL_BYTE_SIZE; j++) {

      char val = readAddress(j);
      if (val == 94)continue;

      else {
        roll += String(val);
        Serial.println(val);
      }
    }

    char HOUR[4];
    char MINUTE[4];
    char SECOND[4];

    char YEAR[4];
    char MONTH[4];
    char DAY[4];

    i += (ROLL_BYTE_SIZE - 1);
    sprintf(HOUR, "%02d", readAddress(++i));
    sprintf(MINUTE, "%02d", readAddress(++i));
    sprintf(SECOND, "%02d", readAddress(++i));
    sprintf(YEAR, "%02d", readAddress(++i));
    sprintf(MONTH, "%02d", readAddress(++i));
    sprintf(DAY, "%02d", readAddress(++i));

    time = String(HOUR) + ":" + String(MINUTE) + ":" + String(SECOND);
    date = "20" + String(YEAR) + "-" + String(MONTH) + "-" + String(DAY);

    dateTime = (date + " " + time);

    nestedObject2["PIN"]      = roll;
    nestedObject2["DateTime"] = dateTime;
    nestedObject2["Verified"] = "1";
    nestedObject2["Status"]   = "1";
    nestedObject2["WorkCode"] = "0";

    logs++;
  }

  nestedObject1["unit_id"] = UNIT_ID;

  String data;
  root.printTo(data);

#ifdef DEBUG
  Serial.println(data);
#endif
  return data;
}


void writeAddress(int address, byte val)
{
  int device;

  if ( address > 65535 ) {
    device = EEPROM_I2C_ADDRESS_2;
  }
  else {
    device = EEPROM_I2C_ADDRESS_1;
  }
  Wire.beginTransmission(device);
  Wire.write((int)(address >> 8));   // left-part of pointer address
  Wire.write((int)(address & 0xFF)); // and the right
  Wire.write(val);
  Wire.endTransmission();
  delay(10);
}

byte readAddress(int address)
{
  byte result; // returned value
  int device;

  if ( address > 65535 ) {
    device = EEPROM_I2C_ADDRESS_2;
  }
  else {
    device = EEPROM_I2C_ADDRESS_1;
  }
  Wire.beginTransmission(device); // these three lines set the pointer
  // position in the EEPROM
  Wire.write((int)(address >> 8));    // left-part of pointer address
  Wire.write((int)(address & 0xFF));  // and the right
  Wire.endTransmission();
  Wire.requestFrom(device, 1);    // now get the byte of data...
  result = Wire.read();
  return result;
}


void updateRegistry() {

  /*******************************************************************************
    SAVE DEBUG DATA TO EEPROM
  *******************************************************************************/
  if (command.error == true) {
    k[3] = 4;
    saveDebugToEEPROM(3);

  }
  /******************************************************************************/

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : updateRegistry()"));
#endif

  lcdCardSection(BUSY, "");             // LCD prints that device is busy sending log to server

  String json = getRequest(registry_server);

  if (json == "failed") {
    command.update_registry = true;     // if fails to connect, registry is to be updated later
    return;
  }

#ifdef DEBUG
  Serial.println(json);
#endif

  unsigned int newCardCounter = 0;
  unsigned int delCardCounter = 0;


  if (json != "failed") {

    if (json == "{}\n")return;

    else {

      /****************************************************************************************
         Curently the size of RFID tag and ROLL byte size are taken as 4 bytes and 10 bytes.
         In StaticJsonBuffer<buffer_size>, the buffer_size is choosen as 2000 because this
         buffer size matches the requirement. If for any reason the RFID tag size or Roll byte
         size is changed the buffer_size in StaticJsonBuffer<buffer_size> must be changed
         accordingly; or else the code will fail to parse the json received from the server.
       ****************************************************************************************/

      StaticJsonBuffer<2500> jsonBuffer;
      JsonObject& root = jsonBuffer.parseObject(json);

      if (!root.success()) {

#ifdef DEBUG
        Serial.println(F("data parsing failed"));
#endif
        return;
      }

      JsonArray& nestedArray   = root["add_card"];
      JsonArray& nestedArray1  = root["add_roll"];
      JsonArray& nestedArray2  = root["remove_card"];

      String card;
      String rollS;
      String delCards;

      /******************************** ADD CARD  *************************************/
      while (true) {

        card = "";
        rollS = "";
        card = root["add_card"][newCardCounter].asString();
        rollS = root["add_roll"][newCardCounter].asString();


#ifdef DEBUG

        Serial.print("TO BE ADDED, ");
        Serial.print(newCardCounter + 1);
        Serial.print("  UID :");
        Serial.print(card);
        Serial.print("  ROLL: ");
        Serial.print(rollS);
#endif


        if (card == NULL /*&& delCards == NULL*/) {

#ifdef DEBUG
          Serial.println("ALL NEW CARDS ADDED");
#endif
          break;
        }

        byte uid[RFID_BYTE_SIZE] ;
        char roll[ROLL_BYTE_SIZE] ;

        if (card != NULL) {

          int tp = 0;

          for (int j = 0; j < RFID_BYTE_SIZE * 2; j += 2) {
            uid[tp] = strtol(card.substring(j, j + 2).c_str(), NULL, 16);
            tp++;
            yield();
          }


          int roll_length = rollS.length();
          tp = 0;
          for (int j = 0; j < roll_length; j++) {
            roll[j] = *(char*)rollS.substring(j, j + 1).c_str();
            yield();
          }

          for (int j = roll_length; j < ROLL_BYTE_SIZE; j++) {
            roll[j] = 94;
            yield();
          }
          addToRegistry(uid, roll);
          newCardCounter++;
        }
      }

      /******************************** DELETE CARD  ******************************************/

      while (true) {
        delCards = "";
        delCards = root["remove_card"][delCardCounter].asString();

#ifdef DEBUG
        Serial.print("   delCardCounter   ");
        Serial.print(delCardCounter);
        Serial.print("   TO BE DELETED: ");
        Serial.println(delCards);
#endif

        if (delCards == NULL) {

#ifdef DEBUG
          Serial.println("ALL REQUESTED CARDS DELETED");
#endif
          break;
        }

        byte uidToBeDeleted[RFID_BYTE_SIZE];

        if (delCards != NULL) {

          Serial.println("INSIDE DELETE PORTION OF UPDATEREGISTRY");

          int tp = 0;

          for (int j = 0; j < RFID_BYTE_SIZE * 2; j += 2) {
            uidToBeDeleted[tp] = strtol(delCards.substring(j, j + 2).c_str(), NULL, 16);
            Serial.println(uidToBeDeleted[tp], HEX);
            tp++;
            yield();
          }
          Serial.print("Card to delete : ");
          deleteRegistry(uidToBeDeleted);
          delCardCounter++;
        }
      }
    }
  }

  saveAddressPointers(REGISTRY_ADDR);
  loadRegistry();

#ifdef DEBUG
  Serial.println("UPDATED REGISTRY:");
  printRegistry();
#endif

  getRequest(acknowledge_server);
  command.update_registry = false;
}


void deleteRegistry(byte uidToBeDeleted[]) {

  /*******************************************************************************
    SAVE DEBUG DATA TO EEPROM
  *******************************************************************************/
  if (command.error == true) {
    k[4] = 5;
    saveDebugToEEPROM(4);
  }
  /******************************************************************************/

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : deleteRegistry()"));

  Serial.print("UID TO BE DELETED : ");
  for (int k = 0; k < RFID_BYTE_SIZE; k++) {
    Serial.print(uidToBeDeleted[k], HEX);
    Serial.print(" ");
  }
  Serial.println("");
  Serial.println(F("PREVIOUS REGISTRY"));
  printRegistry();
#endif

  bool matched = false;

  for (int i = 0; i < recSize; i++) {
    for (int j = 0; j < RFID_BYTE_SIZE ; j++) {
      if ( registry[i][j] == uidToBeDeleted[j]) {
        matched = true;
      }
      else {
        matched = false;
        break;
      }
      yield();
    }


    if (matched == true) {

#ifdef DEBUG
      Serial.println("A match is found in the EEPROM");
      for (int j = 0; j < RFID_BYTE_SIZE; j++) {
        Serial.print(registry[i][j], HEX);
      }
      Serial.print(" == ");
      for (int j = 0; j < RFID_BYTE_SIZE; j++) {
        Serial.print(uidToBeDeleted[j], HEX);
      }
      Serial.println("");
#endif

      for (int z = 0; z < RFID_BYTE_SIZE; z++) {
        byte eData = readAddress(EEPROM_REGISTRY_START_ADDRESS + (recSize - 1) * RFID_BYTE_SIZE + z);
        writeAddress((EEPROM_REGISTRY_START_ADDRESS + i * RFID_BYTE_SIZE + z), eData);
        yield();
      }

      for (int z = 0; z < ROLL_BYTE_SIZE; z++) {
        byte eRoll = readAddress(EEPROM_ROLL_START_ADDRESS + (recSize - 1) * ROLL_BYTE_SIZE + z);
        writeAddress((EEPROM_ROLL_START_ADDRESS + i * ROLL_BYTE_SIZE + z), eRoll);
        yield();
      }

      registryLastAddress -= RFID_BYTE_SIZE ;

#ifdef DEBUG
      Serial.print("REGISTRY AFTER DELETE :  ");
      Serial.println(registryLastAddress);

#endif
      loadRegistry();
    }
  }
  yield();
}


void report_restart() {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : report_restart()"));
#endif


  String report;

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["Device_id"] = UNIT_ID;
  root["Reset Cause"] = ESP.getResetReason();
  root.printTo(report);

#ifdef DEBUG
  Serial.println();
#endif
  if (postRequest(report_server, report)) {
    command.restarted = false;
  }
  else {
    command.restarted = true;
  }
}


void getFileName() {

  String data = getRequest(OTAfileNameServer);

#ifdef DEBUG
  Serial.print(F("Json data Received : "));
  Serial.println(data);
#endif

  StaticJsonBuffer<200>jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(data);

  if (!root.success()) {
#ifdef DEBUG
    Serial.println(F("josn parsing failed"));
#endif
  }

  firmwareUrl += root["fileName"].asString();

#ifdef DEBUG
  Serial.print(F("File name: "));
  Serial.println(firmwareUrl);
#endif

  firmwareServer += SERVER + "/static/" + firmwareUrl;

#ifdef DEBUG
  Serial.print(F("Server name: "));
  Serial.println(firmwareServer);
#endif

}


void confirmUpdate() {

#ifdef DEBUG
  Serial.println("Confirming Update....");
#endif

  String data;
  int OK;

  if (Flag.updateSuccess == true) {
    OK = 1;
  }
  else {
    OK = 0;
  }

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["unit_id"] = UNIT_ID;
  root["firmware_file_name"] = firmwareUrl;
  root["success"] = OK;
  root.printTo(data);

#ifdef DEBUG
  Serial.print("Json data : ");
  Serial.println(data);
#endif
  bool confirmed = postRequest(confirm_update, data);
  delay(1500);
  ESP.restart();
}


void executeUpdate() {

  //Serial.end();
  SPI.end();
  Wire.endTransmission();
  delay(1000);

  t_httpUpdate_return ret = ESPhttpUpdate.update(firmwareServer);

  switch (ret) {

    case HTTP_UPDATE_FAILED:
#ifdef DEBUG
      Serial.println("HTTP_UPDATE_FAILD Error");
#endif
      Flag.updateSuccess = false;
      break;

    case HTTP_UPDATE_NO_UPDATES:
#ifdef DEBUG
      Serial.println("HTTP_UPDATE_NO_UPDATES");
#endif
      Flag.updateSuccess = false;
      break;

    case HTTP_UPDATE_OK:
      Serial.begin(115200);

#ifdef DEBUG
      Serial.println("HTTP_UPDATE_OK");
#endif
      Flag.updateSuccess = true;
      break;
  }
}


void modify_serverTime(int server_time) {
  serverTime.detach();
  serverTime.attach(server_time, commandFlag);
}


void sendRecsize() {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : sendRecsize()"));
#endif


  String totalUsers;

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["unit_id"] = UNIT_ID;
  root["number_of_rec"] = recSize;
  root["firmware_version"] = CodeVersion;
  root.printTo(totalUsers);


#ifdef DEBUG
  Serial.println(totalUsers);
#endif

  bool sent = postRequest(reportRecSizeServer, totalUsers);

  if (sent == true) {
    confirmSentRecSize();
    command.sendRecSize = false;
  }
}


void regModeExit() {
  Serial.print("END REGISTRATION MODE ");
  command.regMode = false;
}

void sendCardForRegistration(String data) {

#ifdef DEBUG
  Serial.println("Inside sendCardForRegistration(String data) function ");
#endif

  String payload;

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["unit_id"] = UNIT_ID;
  root["card_finger"] = data;
  root.printTo(payload);

#ifdef DEBUG
  Serial.print("Payload: ");
  Serial.println(payload);
#endif

  postRequest(cardRegistration_server, payload);
}

void confirmSentRecSize() {
  getRequest(acknowlegde_recSize);
}

void confirmRegistrationMode() {
  Serial.println("Confirming Registration mode ");
  getRequest(registrationConfirm);
}

void endRegMode() {
  Serial.println("Ending Registration mode ");
  getRequest(regTimeOutServer);
}


////////////////////////////////////////////////////////////////////////////
String getRequest(String url) {

  String data;

  HTTPClient http;
  http.begin(url);

  int code = http.GET();
#ifdef DEGUG
  Serial.print("http code :  ");
  Serial.println(code);
#endif
  data = http.getString();
  if (code == HTTP_CODE_OK) {
    http.end();
    return data;
  }
  else {
    return "failed";
  }
}

bool postRequest(String url, String payload) {
  /*
    #ifdef DEBUG
    Serial.println(F("############################################################################"));
    Serial.println(F("Function call : postReqiest()\n"));
    #endif
  */
  HTTPClient http;
  http.begin(url);
  http.addHeader("content-type", "text/json");
  int code = http.POST(payload);

#ifdef DEBUG
  Serial.print("http code :  ");
  Serial.println(code);
#endif

  if (code == HTTP_CODE_OK) {
    http.end();
    return true;
  }
  else {
#ifdef DEBUG
    Serial.print("Error Msg :  ");
    Serial.println(http.getString());
#endif
    return false;
  }
}

/////////////////////////////// Functions to send registry to server ////////////////////////

void sendRegistry() {

  serverTime.detach();

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : sendRegistry()\n"));

  Serial.print("NUMBER OF REGISTRY TO BE SENT : ");
  Serial.println(recSize);
#endif
  int counter = 0;
  int startIndex = 0;
  int endIndex = (REG_BATCH_SIZE - 1);
  bool successfull = false;
  int numberOfPackets = ( recSize / REG_BATCH_SIZE );
  int extraPackets    = ( recSize % REG_BATCH_SIZE );


  if (numberOfPackets > 0) {

#ifdef DEBUG
    Serial.print("NUMBER OF PACKETS : ");
    Serial.println(numberOfPackets);
    Serial.print("NUMBER OF EXTRAPACKETS : ");
    Serial.println(extraPackets);
#endif

    for (int i = 0; i < numberOfPackets ; i++) {
      String data = convertRegistryToJson(startIndex, startIndex + REG_BATCH_SIZE);
      yield();
      bool postReqSuccess = postRequest(sendUsersServer, data);

      if (postReqSuccess == true) {
#ifdef DEBUG
        Serial.println("REGISTRY SENT SUCCESSFULLY..!!!");
        Serial.print("SENT THE REGISTRY FROM ");
        Serial.print(startIndex);
        Serial.print("TO ");
        Serial.println(startIndex + REG_BATCH_SIZE);
#endif
        startIndex = startIndex + REG_BATCH_SIZE;
        counter += REG_BATCH_SIZE;
        Serial.print("REGISTRY SENT : ");
        Serial.println(counter);
      }

      else {
#ifdef DEBUG
        Serial.println("REGISTRY SENT FAILED..!!!");
#endif
        i--;
      }
    }
  }


  if (extraPackets > 0 && successfull == false) {

    startIndex = numberOfPackets * REG_BATCH_SIZE;

#ifdef DEBUG
    Serial.print("SENDING REST OF THE REGISTRY FROM ");
    Serial.print(startIndex);
    Serial.print("TO ");
    Serial.println(startIndex + extraPackets);
#endif

    String data = convertRegistryToJson(startIndex, startIndex + extraPackets);
    yield();
    Serial.print("Data encoded json : ");
    Serial.println(data);
    bool postReqSuccess = postRequest(sendUsersServer, data);
    yield();
    if (postReqSuccess == true) {
#ifdef DEBUG
      Serial.println("REGISTRY SENT SUCCESSFULLY..!!!");
#endif
      successfull = true;
      counter += extraPackets;
      Serial.print("REGISTRY SENT : ");
      Serial.println(counter);
    }
    else {
#ifdef DEBUG
      Serial.println("REGISTRY SENT FAILED..!!!");
#endif
    }
  }
  serverTime.attach(UPDATE_INTERVAL_SECOND, commandFlag);

  if (counter == recSize) {
    Serial.println("Confirming finish");
    confirmSentRegistry();
  }
}


String convertRegistryToJson(int from_index, int to_index) {

#ifdef DEBUG

  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : convertRegistryToJson()"));

  Serial.print("Start address : ");
  Serial.println(from_index);
  Serial.print("End address : ");
  Serial.println(to_index);

#endif

  char aa[4];
  String registry_string = "";

  StaticJsonBuffer<2900> jsonBuffer;

  JsonObject& root = jsonBuffer.createObject();
  root["unit_id"] = UNIT_ID;

  JsonArray& card = root.createNestedArray("card");

  for (int i = from_index; i < to_index; i++) {
    for (int z = 0; z < RFID_BYTE_SIZE ; z++) {
      sprintf(aa, "%02x", registry[i][z]);
      registry_string += String(aa);
    }
    card.add(registry_string);
    registry_string = "";
  }

  String payload;
  root.printTo(payload);

#ifdef DEBUG
  Serial.println(payload);
#endif
  return payload;
}



void confirmSentRegistry() {
#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : confirmSentRegistry()"));
#endif

  String response = getRequest(confirmSentUsers);
  if (response != "failed") {
#ifdef DEBUG
    Serial.println(F("Successfully Confirmed "));
#endif
    command.sendRegistry = false;
  }
}


/*******************************************************************************
   FUNCTIONS RELATED TO DEBUG DATA
*******************************************************************************/

void MakeDebugDataZero() {                                                                          //sajib
  int j = 0;
#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call :MakeDebugDataZero()"));
  Serial.println();
#endif

  for (int i = EEPROM_DEBUG_START_ADDRESS; i < EEPROM_DEBUG_START_ADDRESS + 20; i++) {
    writeAddress(i, j);
    yield();
  }
}

void saveDebugToEEPROM(int a) {                                                                     //sajib
#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : saveDebugToEEPROM()"));
  Serial.println();
#endif
  {
    writeAddress((EEPROM_DEBUG_START_ADDRESS + a), k[a]);
  }
}

String convertAdminToJson(int from, int to) {                                                           //sajib
#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : convertAdminToJson()"));

  Serial.print("Start address : ");
  Serial.println(from);
  Serial.print("End address : ");
  Serial.println(to);
#endif

  StaticJsonBuffer<300> jsonBuffer;

  JsonObject& root = jsonBuffer.createObject();
  root["unit_id"] = UNIT_ID;
  JsonArray& debug_data = root.createNestedArray("debug_data");

  for (int i = from; i < to; i++)
  {
    debug_data.add(readAddress(i));
    yield();
  }
  String data1;
  root.printTo(data1);

#ifdef DEBUG
  Serial.println(data1);
#endif

  return data1;
}


void admin_data() {                                                                                   //sajib

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : admin_data()"));
#endif

  String admin = convertAdminToJson(EEPROM_DEBUG_START_ADDRESS, EEPROM_DEBUG_START_ADDRESS + 20); //(from,to)
#ifdef DEBUG
  Serial.println(admin);
#endif

  if (postRequest(debugErrorServer, admin)) {
    command.error = false;
  }
}


/************************************ IS CARD AVAILABLE ****************************************/

bool isUserAvailable() {
#ifdef RDM
  if (RFID.isIdAvailable() > 0) {
    return true;
  }
  else return false;
#endif

#ifdef MIFARE
  if (rfid.PICC_IsNewCardPresent() && rfid.PICC_ReadCardSerial()) {
    rfid.PICC_HaltA();
    rfid.PCD_StopCrypto1();
    return true;
  }
  else return false;
#endif
}

void getUser() {

#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : getUser()"));
#endif

#ifdef RDM
  int tag = RFID.readId();

#ifdef DEBUG
  Serial.print("TAG : ");
  Serial.println(tag);
#endif

  for (int i = 0; i < RFID_BYTE_SIZE; i++) {
    user[i] = RFID.uidByte[i];
  }
#endif

#ifdef MIFARE
  Serial.print("User : ");
  for (int i = 0; i < RFID_BYTE_SIZE; i++) {
    user[i] = rfid.uid.uidByte[i];

    Serial.print(user[i]);
  }
  Serial.println("");
#endif
}

String unregisteredUser() {

#ifdef RDM
  int tag = RFID.readId();

#ifdef DEBUG
  Serial.print("TAG : ");
  Serial.println(tag);
#endif

  return RFID.realTagString;

#endif

#ifdef MIFARE
  String cardNo = "";
  char tempArray[2];

  for (byte i = 0; i < RFID_BYTE_SIZE; i++) {
    sprintf(tempArray, "%02x", rfid.uid.uidByte[i]);
#ifdef DEBUG
    Serial.println (tempArray);
#endif
    cardNo += String(tempArray);
  }
  return cardNo;
#endif
}

/************************ GET MESSAGE FROM SERVER **************************/

void getMessage() {

#ifdef DEBUG
  Serial.print("Getting Message from server ");
#endif

  scrollTextTime.detach();

  String json;
  String fontColor;
  String backgroundColor;

  json =  getRequest(messageServer);

#ifdef DEBUG
  Serial.println(F("Received Json data: "));
  Serial.println(json);
#endif

  if (json != "failed") {

    StaticJsonBuffer<1000>jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(json);

    if (!root.success()) {
#ifdef DEBUG
      Serial.println(F("josn parsing failed"));
#endif
      text += " ";
      return;
    }

    text = root["message"].asString();
    text += " ";
    fontColor  = root["font_color"].asString();
    backgroundColor   = root["background_color"].asString();

    textColor = strtol(fontColor.c_str(), NULL, 16);
    backColor = strtol(backgroundColor.c_str(), NULL, 16);

#ifdef DEBUG
    Serial.print("Received Message : ");
    Serial.println(text);
    Serial.print("Received textColor : ");
    Serial.println(textColor, HEX);
    Serial.print("Received backColor : ");
    Serial.println(backColor, HEX);
#endif
    confirmMessage();
    scrollTextTime.attach_ms(250, scrollingText);
    scrollingTextRect();
  }
}

void confirmMessage() {
  String data;
  data =  getRequest(confirmMsgServer);
  if (data != "failed") {
    command.messagePresent = false;
  }
}
/***************************************************************************/

///////////////////////////// Display /////////////////////////////

void stellar_logo() {

  tft.fillScreen(ST7735_BLUE);
  float multiplier = 3.1416 / 180;
  uint16_t center_x;
  uint16_t center_y;
  uint16_t rad_large;
  uint16_t rad_small;

  uint16_t x_1;
  uint16_t y_1;
  uint16_t x_2;
  uint16_t y_2;
  uint16_t x_3;
  uint16_t y_3;
  uint16_t x_4;
  uint16_t y_4;
  uint16_t x_5;
  uint16_t y_5;
  uint16_t x_6;
  uint16_t y_6;

  uint16_t xx_1;
  uint16_t yy_1;
  uint16_t xx_2;
  uint16_t yy_2;
  uint16_t xx_3;
  uint16_t yy_3;
  uint16_t xx_4;
  uint16_t yy_4;
  uint16_t xx_5;
  uint16_t yy_5;
  uint16_t xx_6;
  uint16_t yy_6;

  uint16_t circle_x_1;
  uint16_t circle_y_1;
  uint16_t circle_x_2 ;
  uint16_t circle_y_2;
  uint16_t circle_x_3;
  uint16_t circle_y_3 ;
  uint16_t circle_x_4;
  uint16_t circle_y_4;
  uint16_t circle_x_5;
  uint16_t circle_y_5;
  uint16_t circle_x_6;
  uint16_t circle_y_6;

#ifdef HORIZONTAL
  center_x = 30;
  center_y = 65;
  rad_large = 20;
  rad_small = 12;

  x_1 = center_x + (uint16_t)rad_large * sin(0 * multiplier);
  y_1 = center_y + (uint16_t)rad_large * cos(0 * multiplier);

  x_2 = center_x + (uint16_t)rad_large * sin(60 * multiplier);
  y_2 = center_y + (uint16_t)rad_large * cos(60 * multiplier);

  x_3 = center_x + (uint16_t)rad_large * sin(120 * multiplier);
  y_3 = center_y + (uint16_t)rad_large * cos(120 * multiplier);

  x_4 = center_x + (uint16_t)rad_large * sin(180 * multiplier);
  y_4 = center_y + (uint16_t)rad_large * cos(180 * multiplier);

  x_5 = center_x + (uint16_t)rad_large * sin(240 * multiplier);
  y_5 = center_y + (uint16_t)rad_large * cos(240 * multiplier);

  x_6 = center_x + (uint16_t)rad_large * sin(300 * multiplier);
  y_6 = center_y + (uint16_t)rad_large * cos(300 * multiplier);

  xx_1 = center_x + (uint16_t)rad_small * sin(30 * multiplier);
  yy_1 = center_y + (uint16_t)rad_small * cos(30 * multiplier);

  xx_2 = center_x + (uint16_t)rad_small * sin(90 * multiplier);
  yy_2 = center_y + (uint16_t)rad_small * cos(90 * multiplier);

  xx_3 = center_x + (uint16_t)rad_small * sin(150 * multiplier);
  yy_3 = center_y + (uint16_t)rad_small * cos(150 * multiplier);

  xx_4 = center_x + (uint16_t)rad_small * sin(210 * multiplier);
  yy_4 = center_y + (uint16_t)rad_small * cos(210 * multiplier);

  xx_5 = center_x + (uint16_t)rad_small * sin(270 * multiplier);
  yy_5 = center_y + (uint16_t)rad_small * cos(270 * multiplier);

  xx_6 = center_x + (uint16_t)rad_small * sin(330 * multiplier);
  yy_6 = center_y + (uint16_t)rad_small * cos(330 * multiplier);

  tft.drawLine(x_1, y_1, xx_1, yy_1, ST7735_WHITE);
  tft.drawLine(xx_1, yy_1, x_2, y_2, ST7735_WHITE);
  tft.drawLine(x_2, y_2, xx_2, yy_2, ST7735_WHITE);
  tft.drawLine(xx_2, yy_2, x_3, y_3, ST7735_WHITE);
  tft.drawLine(x_3, y_3, xx_3, yy_3, ST7735_WHITE);
  tft.drawLine(xx_3, yy_3, x_4, y_4, ST7735_WHITE);
  tft.drawLine(x_4, y_4, xx_4, yy_4, ST7735_WHITE);
  tft.drawLine(xx_4, yy_4, x_5, y_5, ST7735_WHITE);
  tft.drawLine(x_5, y_5, xx_5, yy_5, ST7735_WHITE);
  tft.drawLine(xx_5, yy_5, x_6, y_6, ST7735_WHITE);
  tft.drawLine(x_6, y_6, xx_6, yy_6, ST7735_WHITE);
  tft.drawLine(xx_6, yy_6, x_1, y_1, ST7735_WHITE);
  tft.drawLine(x_1, y_1, x_4, y_4, ST7735_WHITE);
  tft.drawLine(x_2, y_2, x_5, y_5, ST7735_WHITE);
  tft.drawLine(x_3, y_3, x_6, y_6, ST7735_WHITE);

  circle_x_1 = center_x + (uint16_t)1 * rad_large * sin(0 * multiplier);
  circle_y_1 = center_y + (uint16_t)1 * rad_large * cos(0 * multiplier);

  circle_x_2 = center_x + (uint16_t)1 * rad_large * sin(60 * multiplier);
  circle_y_2 = center_y + (uint16_t)1 * rad_large * cos(60 * multiplier);

  circle_x_3 = center_x + (uint16_t)1 * rad_large * sin(120 * multiplier);
  circle_y_3 = center_y + (uint16_t)1 * rad_large * cos(120 * multiplier);

  circle_x_4 = center_x + (uint16_t)1 * rad_large * sin(180 * multiplier);
  circle_y_4 = center_y + (uint16_t)1 * rad_large * cos(180 * multiplier);

  circle_x_5 = center_x + (uint16_t)1 * rad_large * sin(240 * multiplier);
  circle_y_5 = center_y + (uint16_t)1 * rad_large * cos(240 * multiplier);
  circle_x_6 = center_x + (uint16_t)1 * rad_large * sin(300 * multiplier);
  circle_y_6 = center_y + (uint16_t)1 * rad_large * cos(300 * multiplier);

  tft.drawCircle(circle_x_1, circle_y_1, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_2, circle_y_2, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_3, circle_y_3, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_4, circle_y_4, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_5, circle_y_5, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_6, circle_y_6, 2, ST7735_WHITE);

  tft.setFont(&FreeSansBold9pt7b);
  tft.setTextSize(0);
  tft.setCursor(60, 70);
  tft.print(F("STELLAR"));
#endif

#ifdef VERTICAL
  int verticalAdjustment = 30;
  center_x = 35; //previously + less +20
  center_y = 65;
  rad_large = 20;
  rad_small = 12;

  //#ifdef DEBUG
  //  Serial.println("GOING VERTICAL, SKYROCKETING");
  //#endif

  x_1 = center_x + (uint16_t)rad_large * sin(0 * multiplier);
  y_1 = center_y + (uint16_t)rad_large * cos(0 * multiplier);

  x_2 = center_x + (uint16_t)rad_large * sin(60 * multiplier);
  y_2 = center_y + (uint16_t)rad_large * cos(60 * multiplier);

  x_3 = center_x + (uint16_t)rad_large * sin(120 * multiplier);
  y_3 = center_y + (uint16_t)rad_large * cos(120 * multiplier);

  x_4 = center_x + (uint16_t)rad_large * sin(180 * multiplier);
  y_4 = center_y + (uint16_t)rad_large * cos(180 * multiplier);

  x_5 = center_x + (uint16_t)rad_large * sin(240 * multiplier);
  y_5 = center_y + (uint16_t)rad_large * cos(240 * multiplier);

  x_6 = center_x + (uint16_t)rad_large * sin(300 * multiplier);
  y_6 = center_y + (uint16_t)rad_large * cos(300 * multiplier);


  xx_1 = center_x + (uint16_t)rad_small * sin(30 * multiplier);
  yy_1 = center_y + (uint16_t)rad_small * cos(30 * multiplier);

  xx_2 = center_x + (uint16_t)rad_small * sin(90 * multiplier);
  yy_2 = center_y + (uint16_t)rad_small * cos(90 * multiplier);

  xx_3 = center_x + (uint16_t)rad_small * sin(150 * multiplier);
  yy_3 = center_y + (uint16_t)rad_small * cos(150 * multiplier);

  xx_4 = center_x + (uint16_t)rad_small * sin(210 * multiplier);
  yy_4 = center_y + (uint16_t)rad_small * cos(210 * multiplier);

  xx_5 = center_x + (uint16_t)rad_small * sin(270 * multiplier);
  yy_5 = center_y + (uint16_t)rad_small * cos(270 * multiplier);

  xx_6 = center_x + (uint16_t)rad_small * sin(330 * multiplier);
  yy_6 = center_y + (uint16_t)rad_small * cos(330 * multiplier);

  tft.drawLine(x_1 + verticalAdjustment, y_1, xx_1 + verticalAdjustment, yy_1, ST7735_WHITE);
  tft.drawLine(xx_1 + verticalAdjustment, yy_1, x_2 + verticalAdjustment, y_2, ST7735_WHITE);
  tft.drawLine(x_2 + verticalAdjustment, y_2, xx_2 + verticalAdjustment, yy_2, ST7735_WHITE);
  tft.drawLine(xx_2 + verticalAdjustment, yy_2, x_3 + verticalAdjustment, y_3, ST7735_WHITE);
  tft.drawLine(x_3 + verticalAdjustment, y_3, xx_3 + verticalAdjustment, yy_3, ST7735_WHITE);
  tft.drawLine(xx_3 + verticalAdjustment, yy_3, x_4 + verticalAdjustment, y_4, ST7735_WHITE);
  tft.drawLine(x_4 + verticalAdjustment, y_4, xx_4 + verticalAdjustment, yy_4, ST7735_WHITE);
  tft.drawLine(xx_4 + verticalAdjustment, yy_4, x_5 + verticalAdjustment, y_5, ST7735_WHITE);
  tft.drawLine(x_5 + verticalAdjustment, y_5, xx_5 + verticalAdjustment, yy_5, ST7735_WHITE);
  tft.drawLine(xx_5 + verticalAdjustment, yy_5, x_6 + verticalAdjustment, y_6, ST7735_WHITE);
  tft.drawLine(x_6 + verticalAdjustment, y_6, xx_6 + verticalAdjustment, yy_6, ST7735_WHITE);
  tft.drawLine(xx_6 + verticalAdjustment, yy_6, x_1 + verticalAdjustment, y_1, ST7735_WHITE);
  tft.drawLine(x_1 + verticalAdjustment, y_1, x_4 + verticalAdjustment, y_4, ST7735_WHITE);
  tft.drawLine(x_2 + verticalAdjustment, y_2, x_5 + verticalAdjustment, y_5, ST7735_WHITE);
  tft.drawLine(x_3 + verticalAdjustment, y_3, x_6 + verticalAdjustment, y_6, ST7735_WHITE);

  circle_x_1 = center_x + (uint16_t)1 * rad_large * sin(0 * multiplier);
  circle_y_1 = center_y + (uint16_t)1 * rad_large * cos(0 * multiplier);

  circle_x_2 = center_x + (uint16_t)1 * rad_large * sin(60 * multiplier);
  circle_y_2 = center_y + (uint16_t)1 * rad_large * cos(60 * multiplier);

  circle_x_3 = center_x + (uint16_t)1 * rad_large * sin(120 * multiplier);
  circle_y_3 = center_y + (uint16_t)1 * rad_large * cos(120 * multiplier);

  circle_x_4 = center_x + (uint16_t)1 * rad_large * sin(180 * multiplier);
  circle_y_4 = center_y + (uint16_t)1 * rad_large * cos(180 * multiplier);

  circle_x_5 = center_x + (uint16_t)1 * rad_large * sin(240 * multiplier);
  circle_y_5 = center_y + (uint16_t)1 * rad_large * cos(240 * multiplier);

  circle_x_6 = center_x + (uint16_t)1 * rad_large * sin(300 * multiplier);
  circle_y_6 = center_y + (uint16_t)1 * rad_large * cos(300 * multiplier);

  tft.drawCircle(circle_x_1 + verticalAdjustment, circle_y_1, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_2 + verticalAdjustment, circle_y_2, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_3 + verticalAdjustment, circle_y_3, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_4 + verticalAdjustment, circle_y_4, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_5 + verticalAdjustment, circle_y_5, 2, ST7735_WHITE);
  tft.drawCircle(circle_x_6 + verticalAdjustment, circle_y_6, 2, ST7735_WHITE);

  tft.setFont(&FreeSansBold9pt7b);
  tft.setTextSize(0);
  tft.setCursor(26, 110);
  tft.print(F("STELLAR"));
#endif

}

void welcome_page() {

  tft.fillScreen(ST7735_RED);

#ifdef HORIZONTAL
  tft.setFont(&FreeSansBold12pt7b);
  tft.setCursor(15, 70);
#endif
#ifdef VERTICAL
  tft.setFont(&FreeSansBold9pt7b);
  tft.setCursor(15, 80);
#endif
  tft.print(F("WELCOME"));
}

void wifiSetupPage() {

  if (WiFi.status() == WL_CONNECTED)return;
  tft.fillScreen(ST7735_BLACK);
  tft.setFont(&FreeSansBold9pt7b);
  tft.setTextSize(0);
  tft.setTextWrap(true);
  tft.setTextColor(ST7735_RED);
#ifdef HORIZONTAL
  tft.setCursor(25, 25);
#endif
#ifdef VERTICAL
  tft.setCursor(10, 25);
#endif
  tft.println(F("SETUP WIFI"));
  tft.setTextSize(0);
  tft.setTextColor(ST7735_MAGENTA);
  tft.setFont();
#ifdef HORIZONTAL
  tft.setCursor(10, 45);
  tft.print(F("Check Instruction Manual"));
  tft.setCursor(10, 100);
  tft.println(F("THE DEVICE WILL START "));
  tft.setCursor(35, 110);
  tft.print(F("IN "));
  tft.print(WIFI_TIMEOUT);
  tft.println(F(" SECONDS "));
#endif
#ifdef VERTICAL
  tft.setCursor(45, 45);
  tft.print(F("Check"));
  tft.setCursor(10, 55);
  tft.print(F("Instruction Manual"));
  tft.setCursor(0, 100);
  tft.println(F("THE DEVICE WILL START "));
  tft.setCursor(25, 110);
  tft.print(F("IN "));
  tft.print(WIFI_TIMEOUT);
  tft.println(F(" SECONDS "));
#endif

}

void device_not_registered_page() {

  espTime.detach();///////////// because of this time and wifi connection is updated on lcd in some interval
  tft.fillScreen(ST7735_RED);
  tft.setTextColor(ST7735_WHITE);
  tft.setFont(&FreeSansBold9pt7b);
#ifdef HORIZONTAL
  tft.setCursor(10, 20);
  tft.print(F("Register Device"));
  tft.setCursor(35, 60);
  tft.print(F("Device ID"));
  tft.setCursor(35, 80);
  tft.println(UNIT_ID);
  tft.setTextSize(0);
  tft.setCursor(5, 100);
  tft.println(F("rumytecnologies.com/rams"));
#endif
#ifdef VERTICAL
  tft.setCursor(15, 20);
  tft.print(F("REGISTER"));
  tft.setCursor(30, 40);
  tft.print(F("DEVICE"));
  tft.setCursor(18, 80);
  tft.print(F("DEVICE ID"));
  tft.setCursor(10, 100);
  tft.print(UNIT_ID);
  tft.setFont();
  tft.setTextSize(0);
  tft.setCursor(5, 120);
  //tft.print(F("rumytecnologies.com/rams"));
#endif
}

void update_page() {

#ifdef HORIZONTAL
  tft.fillScreen(ST7735_RED);
  tft.setTextColor(ST7735_WHITE);
  tft.setFont(&FreeSansBold9pt7b);
  tft.setCursor(20, 60);
  tft.println(F("UPDATING......"));
  tft.setCursor(20, 80);
  tft.println(F("PLEASE WAIT"));
#endif
#ifdef VERTICAL
  tft.fillScreen(ST7735_RED);
  tft.setTextColor(ST7735_WHITE);
  tft.setFont(&FreeSansBold9pt7b);
  tft.setCursor(20, 60);
  tft.println(F("UPDATING"));
  tft.setCursor(20, 80);
  tft.println(F("PLEASE"));
  tft.setCursor(20, 100);
  tft.println(F("WAIT..!!"));
#endif

}



void lcdCardSection(int matched, String roll) {


  lcdRect(ST7735_BLACK);
  tft.setFont(&FreeSansBold9pt7b);
  tft.setTextSize(0);


  if (matched == CARD_MATCHED) {

    tft.setTextColor(0x07ec);
    lcdRect(ST7735_BLACK);
#ifdef HORIZONTAL
    tft.setCursor(25, 60);
#endif

#ifdef VERTICAL
    tft.setCursor(10, 65);
#endif
    tft.print(F("VERIFIED ID"));


#ifdef HORIZONTAL
    tft.setCursor(((160 - (strlen(roll.c_str()) * 10) + 10) / 2), 80);
#endif

#ifdef VERTICAL
    tft.setCursor(((128 - (strlen(roll.c_str()) * 10)) / 2), 85);
#endif
    tft.print(roll);
  }

  else if (matched == CARD_NOT_MATCHED) {
    tft.setTextColor(0xf80a);
    lcdRect(ST7735_BLACK);
#ifdef HORIZONTAL
    tft.setCursor(30, 60);
    tft.print(F("Unregistered"));
    tft.setCursor(40, 80);
    tft.print(roll);
#endif
#ifdef VERTICAL
    tft.setCursor(10, 65);
    tft.print(F("Unregistered"));
    tft.setCursor(30, 85);
    tft.print(roll);
#endif
  }

  else if (matched == REST) {
    tft.setTextColor(ST7735_GREEN);
    lcdRect(ST7735_BLACK);
#ifdef HORIZONTAL
    tft.setCursor(15, 70);
    tft.println(F("  SCAN CARD"));
#endif
#ifdef VERTICAL
    tft.setCursor(40, 65);
    tft.println(F("SCAN"));
    tft.setCursor(40, 85);
    tft.println(F("CARD"));
#endif
  }

  else if (matched == BUSY) {
    tft.setTextColor(ST7735_GREEN);
    lcdRect(ST7735_BLACK);
#ifdef HORIZONTAL
    tft.setCursor(10, 60);
    tft.println(F("  DEVICE BUSY"));
    tft.setCursor(10, 80);
    tft.println(F("  PLEASE WAIT"));
#endif
#ifdef VERTICAL
    tft.setCursor(0, 65);
    tft.println(F("DEVICE BUSY"));
    tft.setCursor(0, 85);
    tft.println(F("PLEASE WAIT"));
#endif
  }
  else if (matched == MEMORY_FULL) {
#ifdef DEBUG
    Serial.println("Inside memory full");
#endif
    tft.setTextColor(ST7735_GREEN);
    lcdRect(ST7735_BLACK);
#ifdef HORIZONTAL
    tft.setCursor(30, 70);
    tft.println(F("MEMORY FULL"));
#endif
#ifdef VERTICAL
    tft.setCursor(25, 65);
    tft.println(F("MEMORY"));
    tft.setCursor(45, 85);
    tft.println(F("FULL"));
#endif
  }
  
    else if (matched == EXPIRED) {
#ifdef DEBUG
    Serial.println("Inside expired");
#endif
    tft.setTextColor(ST7735_RED);
    lcdRect(ST7735_BLACK);
#ifdef HORIZONTAL
    tft.setCursor(20, 70);
    tft.println(F("DEVICE EXPIRED"));
#endif
#ifdef VERTICAL
    tft.setCursor(25, 65);
    tft.println(F("DEVICE"));
    tft.setCursor(20, 85);
    tft.println(F("EXPIRED"));
#endif
  }
}


void wifiSection() {

#ifdef HORIZONTAL
  tft.fillRect(90, 0, 70, 40, ST7735_BLACK);
#endif
#ifdef VERTICAL
  tft.fillRect(90, 0, 38, 40, ST7735_BLACK);
#endif
  int COLOR1;
  int COLOR2;
  if (WiFi.status() == WL_CONNECTED && Flag.internet == true) {
    COLOR1 = ST7735_GREEN;
  }
  else if (WiFi.status() == WL_CONNECTED && Flag.internet == false) {
    COLOR1 = ST7735_YELLOW;
  }
  else {
    COLOR1 = ST7735_RED;
  }

  if (command.regMode == true) {
    COLOR2 = ST7735_GREEN;
  }
  else {
    COLOR2 = ST7735_BLACK;
  }

  tft.setFont(&TomThumb);
  tft.setFont();
  tft.setTextSize(1);
#ifdef HORIZONTAL
  tft.setCursor(135, 10);
#endif
#ifdef VERTICAL
  tft.setCursor(100, 10);
#endif
  tft.setTextColor(COLOR1);
  tft.print("WIFI");

  tft.setFont(&TomThumb);
  tft.setFont();
  tft.setTextSize(1);
#ifdef HORIZONTAL
  tft.setCursor(100, 10);
#endif
#ifdef VERTICAL
  tft.setCursor(95, 25);
#endif
  tft.setTextColor(COLOR2);
  tft.print("REG");
}



void lcdRect(uint16_t COLOR) {

#ifdef HORIZONTAL
  tft.fillRect(0, 20, 160, 85 , COLOR);
#endif
#ifdef VERTICAL
  tft.fillRect(0, 20, 128, 100, COLOR);
#endif

}


void lcdClockSection() {

  /*******************************************************************************
    SAVE DEBUG DATA TO EEPROM
  *******************************************************************************/
  if (command.error == true) {
    k[5] = 6;
    saveDebugToEEPROM(5);
  }
  /******************************************************************************/

  dispUnitIDTime.detach();

  tft.setFont();
  tft.setTextSize(1);
  tft.setTextColor(ST7735_CYAN);


  /////////////////////////PRINT DATE //////////////////////////////

#ifdef HORIZONTAL
  tft.fillRect(0, 0, 90, 20, ST7735_BLACK);
  tft.setCursor(5, 10);
#endif

#ifdef VERTICAL
  tft.fillRect(0, 0, 90, 20, ST7735_BLACK);
  tft.setCursor(5, 10);
#endif

  tft.print(timeStamp.Day);
  tft.print(F("."));
  tft.print(timeStamp.Month);
  tft.print(F(".20"));
  tft.print(timeStamp.Year);

  dispUnitIDTime.attach(30, displayUnitID);
}

void displayUnitID() {

  if (command.deviceRegistered == true) {

#ifdef HORIZONTAL
    tft.setFont();
    tft.setTextColor(ST7735_CYAN);
    tft.setTextSize(1);
    tft.fillRect(0, 0, 90, 20, ST7735_BLACK);
    tft.setCursor(5, 10);
    tft.print(UNIT_ID);
#endif

#ifdef VERTICAL

    tft.setFont();
    tft.setTextColor(ST7735_CYAN);
    tft.setTextSize(1);
    tft.fillRect(0, 0, 90, 20, ST7735_BLACK);
    tft.setCursor(5, 10);
    tft.print(UNIT_ID);
#endif
  }
}

void scrollingText() {

  if (text == " ") {
    text = "www.stellarbd.com ";
  }

  else {
    if (offset < text.length())
    {

      tft.setTextWrap(false);
      tft.setFont(&FreeSans9pt7b);
      tft.setTextSize(1);

      tft.setTextColor(backColor);

#ifdef HORIZONTAL
      tft.setCursor(0, 120);
#endif

#ifdef VERTICAL
      tft.setCursor(0, 145);
#endif

      tft.print(t);

      t = "";
      for (int i = 0; i < width; i++) {
        t += text.charAt((offset + i) % text.length());
      }

#ifdef HORIZONTAL
      tft.setCursor(0, 120);
#endif

#ifdef VERTICAL
      tft.setCursor(0, 145);
#endif
      tft.setTextColor(textColor);
      tft.print(t);
      offset++;
    }

    else {
      offset = 0;
    }
  }
}


void scrollingTextRect() {

#ifdef HORIZONTAL
  tft.fillRect(0, 105, 160, 30, backColor);
#endif

#ifdef VERTICAL
  tft.fillRect(0, 120, 128, 40, backColor);
#endif

}



/**************************************  EEPROM PRINT ******************************************/

void getUnsentLog() {


#ifdef DEBUG
  Serial.println(F("############################################################################"));
  Serial.println(F("Function call : getUnsentLog()"));
#endif

  Serial.print(F("Unsent LOG :"));
  Serial.println(logNumber);
  for (int i = EEPROM_LOG_START_ADDRESS; i < currentLogAddress;) {
    delay(1);
    Serial.print("Address :  ");
    Serial.print(i);
    Serial.print(F("UID: "));
    for (int uid = 0; uid < ROLL_BYTE_SIZE; uid++) {
      Serial.print((char)readAddress(i));
      i++;
    }

    Serial.print(F("  TIME: "));

    for (int tym = 0; tym < 3; tym++) {
      Serial.print(readAddress(i));
      Serial.print(" ");
      i++;
    }
    Serial.print(F("  DATE: "));

    for (int date = 0; date < 3; date++) {
      Serial.print(readAddress(i));
      Serial.print(" ");
      i++;
    }
    Serial.println();
  }
}




void printRegistry() {

  Serial.println("############################## REGISTRY ##############################################");
  Serial.println("_________________________________________________________________________________________");

  int reg = EEPROM_REGISTRY_START_ADDRESS;
  int rol = EEPROM_ROLL_START_ADDRESS;

  int roll_increment = 0;

  for (int card_increment = EEPROM_REGISTRY_START_ADDRESS; card_increment < registryLastAddress;) {
    for (int j = 0; j < RFID_BYTE_SIZE; j++) {
      Serial.print(readAddress(card_increment + j), HEX);
    }

    Serial.print("    ");

    roll_increment = (card_increment - EEPROM_REGISTRY_START_ADDRESS) / 3;

    for (int j = 0; j < ROLL_BYTE_SIZE; j++) {
      //Serial.print(" roll : ");
      //Serial.print(EEPROM_ROLL_START_ADDRESS+roll_increment*ROLL_BYTE_SIZE+j);
      //Serial.print("  ");
      if (readAddress(EEPROM_ROLL_START_ADDRESS + roll_increment * ROLL_BYTE_SIZE + j) == 94) continue;
      else Serial.print((char)readAddress(EEPROM_ROLL_START_ADDRESS + roll_increment * ROLL_BYTE_SIZE + j));
      //Serial.print(readAddress(EEPROM_ROLL_START_ADDRESS+j+roll_increment));
    }

    card_increment += RFID_BYTE_SIZE;
    //roll_increment +=ROLL_BYTE_SIZE;
    Serial.println();
    yield();
  }

  Serial.println("_________________________________________________________________________________________");

}



void printEEPROM() {

  Serial.println("############################################################################");
  Serial.println();
  Serial.println("_________________________________________________________________________________________");
  Serial.print("|REGISTRY_HIGH_BYTE_ADDRESS(        ");
  Serial.print(REGISTRY_HIGH_BYTE_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(REGISTRY_HIGH_BYTE_ADDRESS));

  Serial.print("|REGISTRY_MID_BYTE_ADDRESS(         ");
  Serial.print(REGISTRY_MID_BYTE_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(REGISTRY_MID_BYTE_ADDRESS));

  Serial.print("|REGISTRY_LOW_BYTE_ADDRESS(         ");
  Serial.print(REGISTRY_LOW_BYTE_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(REGISTRY_LOW_BYTE_ADDRESS));


  Serial.print("|UNSENT_LOG_STARTING_ADDRESS_H(     ");
  Serial.print(UNSENT_LOG_STARTING_ADDRESS_H);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(UNSENT_LOG_STARTING_ADDRESS_H));

  Serial.print("|UNSENT_LOG_STARTING_ADDRESS_M(     ");
  Serial.print(UNSENT_LOG_STARTING_ADDRESS_M);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(UNSENT_LOG_STARTING_ADDRESS_M));

  Serial.print("|UNSENT_LOG_STARTING_ADDRESS_L(     ");
  Serial.print(UNSENT_LOG_STARTING_ADDRESS_L);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(UNSENT_LOG_STARTING_ADDRESS_L));
  Serial.println("_________________________________________________________________________________________");
  Serial.print("|EEPROM_REGISTRY_START_ADDRESS(     ");
  Serial.print(EEPROM_REGISTRY_START_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(EEPROM_REGISTRY_START_ADDRESS));
  Serial.println("_________________________________________________________________________________________");
  Serial.print("|EEPROM_ROLL_START_ADDRESS(         ");
  Serial.print(EEPROM_ROLL_START_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(EEPROM_ROLL_START_ADDRESS));
  Serial.println("_________________________________________________________________________________________");
  Serial.print("|EEPROM_LOG_START_ADDRESS(          ");
  Serial.print(EEPROM_LOG_START_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.print((char)readAddress(EEPROM_LOG_START_ADDRESS));
  Serial.print((char)readAddress(EEPROM_LOG_START_ADDRESS + 1));
  Serial.println((char)readAddress(EEPROM_LOG_START_ADDRESS + 2));

  Serial.print("|EEPROM_LOG_END_ADDRESS(           ");
  Serial.print(EEPROM_LOG_END_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(EEPROM_LOG_END_ADDRESS));
  Serial.println("_________________________________________________________________________________________");

  Serial.print("EEPROM_DEBUG_START_ADDRESS(     ");
  Serial.print(EEPROM_DEBUG_START_ADDRESS);
  Serial.print(")");
  Serial.print("       ");
  Serial.println(readAddress(EEPROM_DEBUG_START_ADDRESS));
  Serial.println("_________________________________________________________________________________________");

  Serial.print("TOTAL LOG CAPACITY IN EEPROM :              ");
  Serial.println(MAX_LOG_CAPACITY_EEPROM);
  Serial.println("_________________________________________________________________________________________");
}

