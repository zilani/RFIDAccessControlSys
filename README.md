Version 1.0 corresponds to mifare cards
Version 2.0 corresponds to 125Khz cards

RFID ATTENDANCE MANAGEMENT SYSTEM


[![N|Solid](https://gitlab.com/zilani/RFIDAccessControlSys/blob/master/RUMY.png)](https://rumytechnologies.com/rams/)

RFID ATTENDANCE MANAGEMENT SYSTEM from Rumy Technology is a server based attendance management and monitoring system.This is the firmware version 1.0

Sub-routines:

#FOR SETTING UP ESP:
  - void sourceSetup()
  - void setAddressPointers()
  - void resetAddressPointers()
  - void loadRegistry()

 #BUZZER RELATED SUB-ROUTINES:
  - void BuzzRFIDdetected()
  - void BuzzRFIDisRegistered()
  - void BuzzRFIDisNotRegistered()
  - void BuzzlogIsSent()
 
 #COMMUNICATING WITH SERVER
  - void send_data()
  - void getCommands()
  - String convertLogToJson(int from , int to)

#EEPROM RELATED SUB-ROUTINES:
  - void saveLogToEEPROM();
  - void writeAddress(int address, byte val);
  - byte readAddress(int address);
  - void addToRegistry(byte a,byte b,byte c,byte d)

#Other Helper Functions:
  - void setTime();
  - void commandFlag();
  - void getUnsentLog();
  - bool isRegistered();
  - void printRegistry();

